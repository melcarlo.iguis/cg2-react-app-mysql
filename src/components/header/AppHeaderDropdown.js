import React, { useContext } from 'react'
import { Navigate, useNavigate } from 'react-router-dom'
import {
  CAvatar,
  CBadge,
  CDropdown,
  CDropdownDivider,
  CDropdownHeader,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
} from '@coreui/react'
import {
  cilBell,
  cilCreditCard,
  cilCommentSquare,
  cilEnvelopeOpen,
  cilFile,
  cilLockLocked,
  cilSettings,
  cilTask,
  cilUser,
  cibGmail,
  cilInf,
} from '@coreui/icons'
import CIcon from '@coreui/icons-react'
import Swal from 'sweetalert2'
import AccountCircleIcon from '@mui/icons-material/AccountCircle'
import AppContext from '../../AppContext'

import avatar8 from './../../assets/images/avatars/8.jpg'

const AppHeaderDropdown = () => {
  const navigate = useNavigate()
  const { user, setUser } = useContext(AppContext)
  let userEmail = localStorage.getItem('email')
  let userName = localStorage.getItem('name')

  const logout = (e) => {
    e.preventDefault()
    localStorage.clear()
    setUser({
      id: null,
      user_type: null,
    })
    Swal.fire({
      title: 'Successful Logout!',
      icon: 'success',
    })
    navigate('/login')
  }
  return (
    <CDropdown variant="nav-item">
      <CDropdownToggle placement="bottom-end" className="py-0" caret={false}>
        <AccountCircleIcon sx={{ fontSize: 40 }} />
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownHeader className="bg-light fw-semibold py-2">Account</CDropdownHeader>
        <CDropdownItem href="#"></CDropdownItem>
        <CDropdownItem href="#">
          <small>Email: </small>
          {userEmail}
        </CDropdownItem>
        <CDropdownItem href="#">
          <small>Username: </small>
          {userName}
        </CDropdownItem>
        <CDropdownHeader className="bg-light fw-semibold py-2">Settings</CDropdownHeader>
        <CDropdownItem onClick={(e) => logout(e)}>
          <CIcon icon={cilSettings} className="me-2" />
          Logout
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default AppHeaderDropdown
