import React from 'react'

// main pages
const Lookup = React.lazy(() => import('./views/dashboard/Lookup'))
const Community = React.lazy(() => import('./views/dashboard/Community'))
const Homepage = React.lazy(() => import('./views/dashboard/Homepage'))
const ShiftLog = React.lazy(() => import('./views/dashboard/ShiftLog'))
const Sos = React.lazy(() => import('./views/dashboard/Sos'))
const Search = React.lazy(() => import('./views/dashboard/Search'))
const TenantProfile = React.lazy(() => import('./views/dashboard/TenantProfile'))
const Admin = React.lazy(() => import('./views/dashboard/Admin'))

const routes = [
  // { path: '/', exact: true, name: 'Residents' },
  { path: '/', name: 'Residents', element: Homepage },
  { path: '/lookup', name: 'Lookup', element: Lookup },
  { path: '/community', name: 'Community', element: Community },
  { path: '/admin', name: 'Admin', element: Admin },
  { path: '/sos', name: 'Sos', element: Sos },
  { path: '/search', name: 'Search', element: Search },
  { path: '/shiftlog', name: 'Shift Log', element: ShiftLog },
  { path: '/resident/:id', name: 'Resident / ResidentName ', element: TenantProfile },

  // { path: '/', name: 'Tenant', element: TenantList, exact: true },
  // { path: '/tenant/all-tenant', name: 'Tenant List', element: TenantList },
]

export default routes
