import axios from 'axios'

export default axios.create({
  // baseURL: 'http://localhost:4000/',
  // baseURL: 'http://188.166.217.249:4002/',
  baseURL: 'https://cg-test-01.cg2tech.io/api/',
  // baseURL: 'http://188.166.217.249:4000/',
})
