import React from 'react'
import { Navigate, useNavigate } from 'react-router-dom'
import { AppContent, AppSidebar, AppFooter, AppHeader } from '../components/index'

const DefaultLayout = () => {
  let userIdval = localStorage.getItem('userId')

  return userIdval == null ? (
    <Navigate to="/login" />
  ) : (
    <div>
      W{/* <AppSidebar /> */}
      <div
        className="wrapper d-flex flex-column mx-0 min-vh-100 bg-light"
        style={{ heigth: '200vh' }}
      >
        <AppHeader />
        <div className="body flex-grow-1 px-3 mx-0">
          <AppContent />
        </div>
        {/* <AppFooter /> */}
      </div>
    </div>
  )
}

export default DefaultLayout
