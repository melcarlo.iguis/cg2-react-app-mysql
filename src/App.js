import React, { Component, Suspense, useState, useEffect } from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import './scss/style.scss'
// axios api
import './style/App.css'
import api from './api/api'
import { AppProvider } from './AppContext'

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)

// Containers
const DefaultLayout = React.lazy(() => import('./layout/DefaultLayout'))

// Pages
const Login = React.lazy(() => import('./views/pages/login/Login'))
const Register = React.lazy(() => import('./views/pages/register/Register'))
const Page404 = React.lazy(() => import('./views/pages/page404/Page404'))
const Page500 = React.lazy(() => import('./views/pages/page500/Page500'))
// global state and variable

function App() {
  const [tenantList, setTenantList] = useState([])
  const [incidentReportList, setIncidentReportList] = useState([])
  const [historyList, setHistoryList] = useState([])
  const [dietaryData, setDietaryData] = useState([{}])
  const [dailylogData, setDailylogData] = useState([])
  const [action, setAction] = useState('')
  const [diagnosisData, setDiagnosisData] = useState([{}])
  const [behaviorData, setBehaviorData] = useState([{}])
  const [shiftLogData, setShiftLogData] = useState([{}])
  const [woundData, setWoundData] = useState([])
  const [vitalData, setVitalData] = useState([])
  const [userList, setUserList] = useState([{}])
  const [eMarData, setEmarData] = useState([])
  const [notesData, setNotesData] = useState([])
  const [eDocumentsData, setEDocumentsData] = useState([])
  const [orderData, setOrderData] = useState([])
  const [assessmentData, setAssessmentData] = useState([])
  const [notificationData, setNotificationData] = useState([])

  const [notifTableData, setNotifTableData] = useState({})
  // useState for closing dialog automatically
  const [dialogClose, setDialogClose] = useState(false)
  const [isEmergencyOpen, setIsEmergencyOpen] = useState(false)

  const [actionValue, setActionValue] = useState(0)
  const [activeTab, setActiveTab] = useState(0)
  const [numberOfShiftLog, setNumberOfShiftLog] = useState(0)
  const [inputValue, setInputValue] = useState('')
  // usestate for privacy
  const [isPrivacyOn, setIsPrivacyOn] = useState(false)
  const [isPopupOpen, setIsPopupOpen] = useState(false)
  const [isInsurancePopupOpen, setIsInsurnacePopupOpen] = useState(false)
  const [popupChildren, setPopupChildren] = useState('')
  const [popupTitle, setPopupTitle] = useState('')
  const [user, setUser] = useState({
    id: null,
    user_type: null,
  })

  const [allergiesList, setAllergiesList] = useState([{}])
  const [insuranceInfoData, setInsuranceInfoData] = useState([])
  const [disabilitiesData, setDisabilitiesData] = useState([])
  const [routineDate, setRoutineData] = useState([])

  const [contactPeopleData, setContactPeopleData] = useState([])
  const [HealthCareDirectivesData, setHealthCareDirectivesData] = useState([])

  useEffect(() => {
    let token = localStorage.getItem('token')
    api
      .get('/users/details', {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((result) => {
        if (typeof result.data.id !== 'undefined') {
          setUser({
            id: result.data.id,
            userType: result.data.userType,
          })
        } else {
          setUser({
            id: null,
            userType: null,
          })
        }
      })
  }, [])

  return (
    <AppProvider
      value={{
        tenantList,
        setTenantList,
        dialogClose,
        setDialogClose,
        action,
        setAction,
        user,
        setUser,
        allergiesList,
        setAllergiesList,
        incidentReportList,
        setIncidentReportList,
        historyList,
        setHistoryList,
        isEmergencyOpen,
        setIsEmergencyOpen,
        diagnosisData,
        setDiagnosisData,
        dietaryData,
        setDietaryData,
        dailylogData,
        setDailylogData,
        vitalData,
        setVitalData,
        actionValue,
        setActionValue,
        behaviorData,
        setBehaviorData,
        numberOfShiftLog,
        setNumberOfShiftLog,
        shiftLogData,
        setShiftLogData,
        userList,
        setUserList,
        woundData,
        setWoundData,
        eMarData,
        setEmarData,
        notesData,
        setNotesData,
        orderData,
        setOrderData,
        assessmentData,
        setAssessmentData,
        isPrivacyOn,
        setIsPrivacyOn,
        notificationData,
        setNotificationData,
        notifTableData,
        setNotifTableData,
        inputValue,
        setInputValue,
        activeTab,
        setActiveTab,
        isPopupOpen,
        setIsPopupOpen,
        popupChildren,
        setPopupChildren,
        insuranceInfoData,
        setInsuranceInfoData,
        disabilitiesData,
        setDisabilitiesData,
        routineDate,
        setRoutineData,
        popupTitle,
        setPopupTitle,
        eDocumentsData,
        setEDocumentsData,
        isInsurancePopupOpen,
        setIsInsurnacePopupOpen,
        contactPeopleData,
        setContactPeopleData,
        HealthCareDirectivesData,
        setHealthCareDirectivesData,
      }}
    >
      <BrowserRouter>
        <Suspense fallback={loading}>
          <Routes>
            <Route exact path="/login" name="Login Page" element={<Login />} />
            <Route exact path="/register" name="Register Page" element={<Register />} />
            <Route exact path="/404" name="Page 404" element={<Page404 />} />
            <Route exact path="/500" name="Page 500" element={<Page500 />} />
            <Route path="*" name="Home" element={<DefaultLayout />} />
          </Routes>
        </Suspense>
      </BrowserRouter>
    </AppProvider>
  )
}

export default App
