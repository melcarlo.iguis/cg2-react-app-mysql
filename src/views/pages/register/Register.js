import React, { useEffect, useState } from 'react'
import { Navigate, Link, useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilLockLocked, cilUser } from '@coreui/icons'
import api from '../../../api/api'

const Register = () => {
  const navigate = useNavigate()
  const [username, setUsername] = useState('')
  const [firstName, setFirstName] = useState('')
  const [middleName, setMiddleName] = useState('')
  const [lastName, setLastName] = useState('')
  const [email, setEmail] = useState('')
  const [password1, setPassword1] = useState('')
  const [password2, setPassword2] = useState('')

  const [isPasswordMatch, setIsPasswordMatch] = useState(false)

  const checkIfPasswordMatch = () => {
    if (password1 == password2) {
      setIsPasswordMatch(true)
    } else {
      setIsPasswordMatch(false)
    }
  }

  console.log(isPasswordMatch)

  useEffect(() => {
    checkIfPasswordMatch()
  }, [password1, password2])

  const registerUser = (e) => {
    e.preventDefault()
    console.log('hello')
    const input = {
      email: email,
      username: username,
      password: password1,
      firstName: firstName,
      middleName: middleName,
      lastName: lastName,
    }

    // api call for adding new tenant
    api
      .post(`/users/create`, input)
      .then((result) => {
        console.log(result)
        Swal.fire({
          title: 'Register Successful!',
          icon: 'success',
          text: 'Welcome',
        })
        navigate('/login')
      })
      .catch((err) => {
        Swal.fire({
          title: 'Authentication failed',
          icon: 'error',
          text: 'Check Registration details',
        })
      })
  }

  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={9} lg={7} xl={6}>
            <CCard className="mx-4">
              <CCardBody className="p-4">
                <CForm onSubmit={(e) => registerUser(e)}>
                  <h1>Register</h1>
                  <p className="text-medium-emphasis">Create your account</p>
                  <CInputGroup
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                    className="mb-3"
                  >
                    <CInputGroupText>
                      <CIcon icon={cilUser} />
                    </CInputGroupText>
                    <CFormInput placeholder="Username" autoComplete="username" />
                  </CInputGroup>
                  <CInputGroup
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                    className="mb-3"
                  >
                    <CInputGroupText>
                      <CIcon icon={cilUser} />
                    </CInputGroupText>
                    <CFormInput placeholder="First Name" />
                  </CInputGroup>
                  <CInputGroup
                    value={middleName}
                    onChange={(e) => setMiddleName(e.target.value)}
                    className="mb-3"
                  >
                    <CInputGroupText>
                      <CIcon icon={cilUser} />
                    </CInputGroupText>
                    <CFormInput placeholder="Middle Name" />
                  </CInputGroup>
                  <CInputGroup
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                    className="mb-3"
                  >
                    <CInputGroupText>
                      <CIcon icon={cilUser} />
                    </CInputGroupText>
                    <CFormInput placeholder="Last Name" />
                  </CInputGroup>
                  <CInputGroup
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    className="mb-3"
                  >
                    <CInputGroupText>@</CInputGroupText>
                    <CFormInput placeholder="Email" autoComplete="email" />
                  </CInputGroup>
                  <CInputGroup
                    value={password1}
                    onChange={(e) => setPassword1(e.target.value)}
                    className="mb-3"
                  >
                    <CInputGroupText>
                      <CIcon icon={cilLockLocked} />
                    </CInputGroupText>
                    <CFormInput
                      type="password"
                      placeholder="Password"
                      autoComplete="new-password"
                    />
                  </CInputGroup>
                  <CInputGroup
                    value={password2}
                    onChange={(e) => setPassword2(e.target.value)}
                    className="mb-4"
                  >
                    <CInputGroupText>
                      <CIcon icon={cilLockLocked} />
                    </CInputGroupText>
                    <CFormInput
                      type="password"
                      placeholder="Repeat password"
                      autoComplete="new-password"
                    />
                  </CInputGroup>
                  <div className="d-grid">
                    <CButton
                      disabled={isPasswordMatch ? false : true}
                      type="submit"
                      color="success"
                    >
                      Create Account
                    </CButton>
                    <a className="mx-auto" href="/login">
                      Already have an account? Click here to login
                    </a>
                  </div>
                </CForm>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Register
