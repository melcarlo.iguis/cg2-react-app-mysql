import React, { useEffect, useState, useContext } from 'react'
import TextField from '@mui/material/TextField'
import Autocomplete from '@mui/material/Autocomplete'
import PageviewIcon from '@mui/icons-material/Pageview'
import { DataGrid } from '@mui/x-data-grid'
import { Row, Col, Form, Button } from 'react-bootstrap'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardGroup,
  CCardHeader,
  CCardImage,
  CCardLink,
  CCardSubtitle,
  CCardText,
  CCardTitle,
  CListGroup,
  CListGroupItem,
  CNav,
  CNavItem,
  CNavLink,
  CCol,
  CRow,
  CAccordion,
  CAccordionBody,
  CAccordionHeader,
  CAccordionItem,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import {
  cilBell,
  cilSpeedometer,
  cilEnvelopeOpen,
  cilList,
  cilMenu,
  cilSearch,
  cilHome,
  cilHeart,
  cilNotes,
  cilUser,
  cilPeople,
  cilWarning,
  cilPlus,
} from '@coreui/icons'
import { DocsCallout, DocsExample } from 'src/components'
import { useNavigate } from 'react-router-dom'
import ReactImg from 'src/assets/images/react.jpg'
// importing axios
import api from '../../api/api'

// global variable
import AppContext from '../../AppContext'

import { styled } from '@mui/material/styles'
import Card from '@mui/material/Card'
import CardHeader from '@mui/material/CardHeader'
import CardMedia from '@mui/material/CardMedia'
import CardContent from '@mui/material/CardContent'
import CardActions from '@mui/material/CardActions'
import Collapse from '@mui/material/Collapse'
import Avatar from '@mui/material/Avatar'
import IconButton from '@mui/material/IconButton'
import Typography from '@mui/material/Typography'
import { red } from '@mui/material/colors'
import FavoriteIcon from '@mui/icons-material/Favorite'
import FolderSharedIcon from '@mui/icons-material/FolderShared'
import ShareIcon from '@mui/icons-material/Share'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import MoreVertIcon from '@mui/icons-material/MoreVert'
import AddBoxIcon from '@mui/icons-material/AddBox'
import PendingActionsIcon from '@mui/icons-material/PendingActions'
import PersonSearchIcon from '@mui/icons-material/PersonSearch'
import PersonIcon from '@mui/icons-material/Person'
import NoteAltIcon from '@mui/icons-material/NoteAlt'
import InsertChartIcon from '@mui/icons-material/InsertChart'
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft'
import DriveFileMoveIcon from '@mui/icons-material/DriveFileMove'
import ThumbUpAltIcon from '@mui/icons-material/ThumbUpAlt'
import LocalPharmacyIcon from '@mui/icons-material/LocalPharmacy'
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn'
import InventoryIcon from '@mui/icons-material/Inventory'
import VolunteerActivismIcon from '@mui/icons-material/VolunteerActivism'
import AssessmentIcon from '@mui/icons-material/Assessment'
import ContentPasteSearchIcon from '@mui/icons-material/ContentPasteSearch'
import FindInPageIcon from '@mui/icons-material/FindInPage'
import MedicationIcon from '@mui/icons-material/Medication'
import KeyboardDoubleArrowLeftIcon from '@mui/icons-material/KeyboardDoubleArrowLeft'
import KeyboardDoubleArrowRightIcon from '@mui/icons-material/KeyboardDoubleArrowRight'
import MenuBookIcon from '@mui/icons-material/MenuBook'
import WarningIcon from '@mui/icons-material/Warning'
import FeedIcon from '@mui/icons-material/Feed'
import VaccinesIcon from '@mui/icons-material/Vaccines'

const ExpandMore = styled((props) => {
  const { expand, ...other } = props
  return <IconButton {...other} />
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}))

function ShiftLog() {
  const [expanded, setExpanded] = React.useState(false)
  const {
    tenantList,
    setTenantList,
    actionValue,
    setActionValue,
    numberOfShiftLog,
    setNumberOfShiftLog,
    shiftLogData,
    setShiftLogData,
  } = useContext(AppContext)

  const [isFetchDone, setIsFetchDone] = useState(false)
  const [dateFilter, setDateFilter] = useState('')

  const handleExpandClick = () => {
    setExpanded(!expanded)
  }

  useEffect(() => {
    setNumberOfShiftLog(parseInt(shiftLogData.length))
  }, [shiftLogData])

  useEffect(() => {
    // Set the default date filter as the date today
    let startDateInput = new Date()
    let newDate = startDateInput.toISOString().substr(0, 10)
    setDateFilter(newDate)
  }, [])

  const fetchShiftLogData = () => {
    let userId = localStorage.getItem('userId')
    let token = localStorage.getItem('token')
    console.log(userId)
    api
      .get(`shiftsummarylog/${userId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        setShiftLogData(res.data)

        // Filter the result that is equal to the date today
        // const newFilter = res.data.filter((value) => {
        //   // get the date today and format it.
        //   let currentDate = new Date()
        //   var month = currentDate.getUTCMonth() + 1
        //   var day = currentDate.getUTCDate()
        //   var year = currentDate.getUTCFullYear()
        //   let newCurrentDate = year + '-' + (month < 12 ? '0' + month : month) + '-' + day
        //   console.log(newCurrentDate)
        //   // get the date from database and format it and compare to the date today
        //   let dateToFilter = new Date(value.date)
        //   var month2 = currentDate.getUTCMonth() + 1
        //   var day2 = currentDate.getUTCDate()
        //   var year2 = currentDate.getUTCFullYear()
        //   let newDateToFilter = year2 + '-' + (month2 < 12 ? '0' + month2 : month2) + '-' + day2

        //   return dateFilter == newDateToFilter
        // })

        // setShiftLogData(newFilter)
        setIsFetchDone(true)
      })
  }

  useEffect(() => {
    fetchShiftLogData()
  }, [dateFilter])

  const cutTime = (string) => {
    let date = new Date(string)
    // const month = date.toLocaleString('en-us', { month: 'long' }); /* June */

    const month = date.getUTCMonth() + 1
    const day = date.getUTCDate()
    const year = date.getUTCFullYear()

    const hour = date.getHours() % 12 || 12
    const min = date.getMinutes()

    const newMins = min < 10 ? '0' + min : min
    var ampm = date.getHours() >= 12 ? 'PM' : 'AM'
    const newDay = day < 10 ? '0' + day : day

    // let fullDate = month + '/' + parseInt(newDay + 1) + '/' + year
    let fullDate = hour + ':' + newMins + ' ' + ampm
    // let fullDate =
    // month + '/' + parseInt(newDay + 1) + '/' + year + ' - ' + hour + ':' + newMins + ' ' + ampm

    return fullDate
  }

  const cutDate = (string) => {
    let date = new Date(string)
    // const month = date.toLocaleString('en-us', { month: 'long' }); /* June */

    const month = date.getUTCMonth() + 1
    const day = date.getUTCDate()
    const year = date.getUTCFullYear()

    const hour = date.getHours() % 12 || 12
    const min = date.getMinutes()

    const newMins = min < 10 ? '0' + min : min
    var ampm = date.getHours() >= 12 ? 'PM' : 'AM'
    const newDay = day < 10 ? '0' + day : day

    let fullDate = month + '/' + parseInt(newDay + 1) + '/' + year
    // let fullDate =
    //   month + '/' + parseInt(newDay + 1) + '/' + year + ' - ' + hour + ':' + newMins + ' ' + ampm

    return fullDate
  }

  const columns = [
    {
      field: 'date',
      headerName: 'Date',
      flex: 1,
      width: 120,
      valueGetter: (params) => {
        return cutDate(params.row.date)
      },
    },
    {
      field: 'time',
      headerName: 'Time',
      flex: 1,
      width: 120,
      valueGetter: (params) => {
        return cutTime(params.row.date)
      },
    },
    {
      field: 'activity',
      headerName: 'Activity',
      flex: 3.8,
      minWidth: 480,
    },
    {
      field: 'residentName',
      headerName: 'Residents',
      flex: 1,
      minWidth: 80,
    },
    {
      field: 'userName',
      headerName: 'Staff',
      flex: 1,
      minWidth: 80,
    },
  ]

  return (
    <>
      <CCard>
        <CCardHeader id="card-header">
          <strong id="res-title">Shift Summary Log</strong>
          <div className="date-filter-holder">
            <Form.Control
              type="date"
              placeholder=" "
              required
              value={dateFilter}
              onChange={(e) => setDateFilter(e.target.value)}
              className="formItem mt-3 form__input"
            />
          </div>
        </CCardHeader>
        <CCardBody>
          <div id="history-div">
            <Row>
              <Col md="12" className="mx-auto"></Col>
              <div id="datagrid-container">
                <Col id="datagrid2" md="12" className="mx-auto mb-3 mt-3">
                  {shiftLogData.length > 0 && isFetchDone == true ? (
                    <DataGrid
                      getRowId={(row) => row.id}
                      rows={shiftLogData}
                      columns={columns}
                      pageSize={5}
                      rowsPerPageOptions={[5]}
                    />
                  ) : (
                    <p>No Data Available</p>
                  )}
                </Col>
              </div>
            </Row>
          </div>
        </CCardBody>
      </CCard>
    </>
  )
}

export default ShiftLog
