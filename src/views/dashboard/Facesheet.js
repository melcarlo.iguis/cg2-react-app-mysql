import React, { useRef, useState, useEffect } from 'react'
import { useReactToPrint } from 'react-to-print'
import api from '../../api/api'
import { CircularProgress } from '@mui/material'

function Facesheet({ props }) {
  console.log(props)
  const [residentAllData, setResidentAllData] = React.useState([])
  const [isFetchDone, setIsFetchDone] = useState(false)
  // function for printing
  const componentRef = useRef()
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  })

  let residentSosId = localStorage.getItem('residentSosId')

  const fetchResidentData = async () => {
    let token = localStorage.getItem('token')
    const res = await api.get(`/residents/${residentSosId}/fetchAllData`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })

    setIsFetchDone(true)
    return res.data
  }

  React.useEffect(() => {
    const getAllResidentData = async () => {
      const allResidentData = await fetchResidentData()
      if (allResidentData) setResidentAllData(allResidentData)
    }

    getAllResidentData()
  }, [])

  const getFullName = (data) => {
    return data.lastName + ',' + ' ' + data.firstName + ' ' + data.middleName
  }

  const cutBirthday = (string) => {
    let date = new Date(string)
    const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
    const day = date.getUTCDate()
    const year = date.getUTCFullYear()

    let fullDate = month + ' ' + day + ', ' + year
    return fullDate
  }

  return (
    <div>
      <button className="mx-auto" id="submit-btn3" onClick={handlePrint}>
        Generate Facesheet
      </button>
      {isFetchDone ? (
        <div className="facesheet-holder" ref={componentRef}>
          <div className="container">
            <div className="content">
              <div className="img-and-name">
                <img className="col-print-4" src={props.picture} alt={props.firstName} />
                <div className="col-print-6 residentName">
                  <p>{getFullName(props)}</p>
                </div>
              </div>
              <div className="col-print-12 center">
                <div className="col-print-3 bold">Address:</div>
                <div className="col-print-3">{props.address}</div>
                <div className="col-print-3 bold">Birthday:</div>
                <div className="col-print-3">{cutBirthday(props.birthday)}</div>
                <div className="col-print-3 bold">Care Level:</div>
                <div className="col-print-3">{props.careLevel}</div>
                <div className="col-print-3 bold">Facility Name:</div>
                <div className="col-print-3">{props.communityName}</div>

                <div className="col-print-3 bold">Eye Color:</div>
                <div className="col-print-3">
                  {props.BasicInfo != null ? props.BasicInfo.eyeColor : 'N/A'}
                </div>
                <div className="col-print-3 bold">Hair Color:</div>
                <div className="col-print-3">
                  {props.BasicInfo != null ? props.BasicInfo.hairColor : 'N/A'}
                </div>
                <div className="col-print-3 bold">Identifying Marks:</div>
                <div className="col-print-3">
                  {props.BasicInfo != null && props.BasicInfo.identifyingMarks != null
                    ? props.BasicInfo.identifyingMarks
                    : 'N/A'}
                </div>
                <div className="col-print-3 bold">Marital Status:</div>
                <div className="col-print-3">
                  {props.BasicInfo != null ? props.BasicInfo.maritalStatus : 'N/A'}
                </div>
                <div className="col-print-3 bold">Place of Birth:</div>
                <div className="col-print-3">
                  {props.BasicInfo != null ? props.BasicInfo.placeofBirth : 'N/A'}
                </div>
                <div className="col-print-3 bold">Preffered Name:</div>
                <div className="col-print-3">
                  {' '}
                  {props.BasicInfo != null ? props.BasicInfo.preferredName : 'N/A'}
                </div>
                <div className="col-print-3 bold">Race:</div>
                <div className="col-print-3">
                  {props.BasicInfo != null ? props.BasicInfo.race : 'N/A'}
                </div>

                <div className="col-print-12">
                  {' '}
                  <div className="col-print-6 bold">Languages:</div>
                  <div className="col-print-6">
                    {' '}
                    {props.BasicInfo != null ? props.BasicInfo.languages.join(' , ') : 'N/A'}
                  </div>
                </div>

                <div className="col-print-12 bold contact">Contact Person</div>
                <div className="col-print-3 bold">Name:</div>
                <div className="col-print-3">
                  {props.ContactPerson != null ? getFullName(props.ContactPerson) : 'N/A'}
                </div>
                <div className="col-print-3 bold">Address:</div>
                <div className="col-print-3">
                  {props.ContactPerson != null ? props.ContactPerson.address : 'N/A'}
                </div>
                <div className="col-print-3 bold">Contact No:</div>
                <div className="col-print-3">
                  {props.ContactPerson != null ? props.ContactPerson.contactNo : 'N / A'}
                </div>
                <div className="col-print-12">
                  <div className="col-print-6">
                    <div className="col-print-12 bold contact">Routines</div>
                    <div className="col-print-12">
                      {props.Routines <= 0 ? (
                        <div className="col-print-12">No Available Data</div>
                      ) : (
                        ''
                      )}
                      {props.Routines.map((val) => (
                        <>
                          <div className="col-print-12">
                            <li>
                              {' '}
                              {val.name} -- {val.frequency}
                            </li>
                          </div>
                        </>
                      ))}
                    </div>
                  </div>
                  <div className="col-print-6 ">
                    <div className="col-print-12 bold contact">Disabilities</div>
                    {props.Disabilities <= 0 ? (
                      <div className="col-print-12">
                        <div className="col-print-12">No Available Data</div>
                      </div>
                    ) : (
                      ''
                    )}
                    {props.Disabilities.map((val) => (
                      <div className="col-print-12">
                        <li>{val.name}</li>
                      </div>
                    ))}
                  </div>
                </div>
                <div className="col-print-12">
                  <div className="col-print-12 bold contact">Advance Directives</div>
                  <div className="col-print-2">
                    <div className="col-print-12 bold">Living will:</div>
                  </div>
                  <div className="col-print-2">
                    <div className="col-print-12 bold">Guardian of Person:</div>
                  </div>
                  <div className="col-print-2">
                    <div className="col-print-12 bold">Power of Attorney:</div>
                  </div>
                  <div className="col-print-2">
                    <div className="col-print-12 bold">DNR:</div>
                    <div className="col-print-12">
                      {props.HealthCareDirectives <= 0 ? (
                        <div className="col-print-12">No Available Data</div>
                      ) : (
                        ''
                      )}
                      {props.HealthCareDirectives.map((val) => (
                        <div className="col-print-12">{val.codeStatus}</div>
                      ))}
                    </div>
                  </div>
                  <div className="col-print-2">
                    <div className="col-print-12 bold">DNR Notes:</div>
                    <div className="col-print-12">
                      {props.HealthCareDirectives <= 0 ? (
                        <div className="col-print-12">No Available Data</div>
                      ) : (
                        ''
                      )}
                      {props.HealthCareDirectives.map((val) => (
                        <div className="col-print-12">{val.notes}</div>
                      ))}
                    </div>
                  </div>
                </div>

                {/* {props.Routines.length > 0
                  ? props.Routines.map((val) => (
                      <div className="col-print-12">
                        <table>
                          <tr>
                            <th>Name</th>
                            <th>Frequency</th>
                          </tr>
                          <tr>
                            <td>{val.name}</td>
                            <td>{val.frequency}</td>
                          </tr>
                        </table>
                      </div>
                    ))
                  : 'N / A'} */}

                <div className="col-print-12">
                  {props.HealthCareDirectives <= 0 ? (
                    <div className="col-print-12">No Available Data</div>
                  ) : (
                    ''
                  )}
                  {props.HealthCareDirectives.map((val) => (
                    <div className="col-print-11 document-holder">
                      <img id="documents" src={val.advanceCareOrderPicture}></img>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <CircularProgress />
      )}
    </div>
  )
}

export default Facesheet
