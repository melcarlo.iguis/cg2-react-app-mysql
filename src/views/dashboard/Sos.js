import React, { useEffect, useState, useContext, useRef } from 'react'
import TextField from '@mui/material/TextField'
import Autocomplete from '@mui/material/Autocomplete'
import PageviewIcon from '@mui/icons-material/Pageview'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardGroup,
  CCardHeader,
  CCardImage,
  CCardLink,
  CCardSubtitle,
  CCardText,
  CCardTitle,
  CListGroup,
  CListGroupItem,
  CNav,
  CNavItem,
  CNavLink,
  CCol,
  CRow,
  CAccordion,
  CAccordionBody,
  CAccordionHeader,
  CAccordionItem,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import {
  cilBell,
  cilSpeedometer,
  cilEnvelopeOpen,
  cilList,
  cilMenu,
  cilSearch,
  cilHome,
  cilHeart,
  cilNotes,
  cilUser,
  cilPeople,
  cilWarning,
  cilPlus,
} from '@coreui/icons'
import { DocsCallout, DocsExample } from 'src/components'
import { useNavigate } from 'react-router-dom'
import ReactImg from 'src/assets/images/react.jpg'
// importing axios
import api from '../../api/api'
import { useReactToPrint } from 'react-to-print'

// global variable
import AppContext from '../../AppContext'

import { styled } from '@mui/material/styles'
import Card from '@mui/material/Card'
import CardHeader from '@mui/material/CardHeader'
import CardMedia from '@mui/material/CardMedia'
import CardContent from '@mui/material/CardContent'
import CardActions from '@mui/material/CardActions'
import Collapse from '@mui/material/Collapse'
import Avatar from '@mui/material/Avatar'
import IconButton from '@mui/material/IconButton'
import Typography from '@mui/material/Typography'
import { red } from '@mui/material/colors'
import FavoriteIcon from '@mui/icons-material/Favorite'
import FolderSharedIcon from '@mui/icons-material/FolderShared'
import ShareIcon from '@mui/icons-material/Share'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import MoreVertIcon from '@mui/icons-material/MoreVert'
import AddBoxIcon from '@mui/icons-material/AddBox'
import PendingActionsIcon from '@mui/icons-material/PendingActions'
import PersonSearchIcon from '@mui/icons-material/PersonSearch'
import PersonIcon from '@mui/icons-material/Person'
import NoteAltIcon from '@mui/icons-material/NoteAlt'
import InsertChartIcon from '@mui/icons-material/InsertChart'
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft'
import DriveFileMoveIcon from '@mui/icons-material/DriveFileMove'
import ThumbUpAltIcon from '@mui/icons-material/ThumbUpAlt'
import LocalPharmacyIcon from '@mui/icons-material/LocalPharmacy'
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn'
import InventoryIcon from '@mui/icons-material/Inventory'
import VolunteerActivismIcon from '@mui/icons-material/VolunteerActivism'
import AssessmentIcon from '@mui/icons-material/Assessment'
import ContentPasteSearchIcon from '@mui/icons-material/ContentPasteSearch'
import FindInPageIcon from '@mui/icons-material/FindInPage'
import MedicationIcon from '@mui/icons-material/Medication'
import KeyboardDoubleArrowLeftIcon from '@mui/icons-material/KeyboardDoubleArrowLeft'
import KeyboardDoubleArrowRightIcon from '@mui/icons-material/KeyboardDoubleArrowRight'
import MenuBookIcon from '@mui/icons-material/MenuBook'
import WarningIcon from '@mui/icons-material/Warning'
import FeedIcon from '@mui/icons-material/Feed'
import VaccinesIcon from '@mui/icons-material/Vaccines'
import SosPopup from './popup/SosPopup'
import Facesheet from './Facesheet'

const ExpandMore = styled((props) => {
  const { expand, ...other } = props
  return <IconButton {...other} />
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}))

function Sos() {
  const {
    numberOfShiftLog,
    setNumberOfShiftLog,
    shiftLogData,
    setShiftLogData,
    isPopupOpen,
    setIsPopupOpen,
  } = useContext(AppContext)
  const [expanded, setExpanded] = React.useState(false)
  const [wordEntered, setWordEntered] = useState('')

  const handleExpandClick = () => {
    setExpanded(!expanded)
  }
  let navigate = useNavigate()
  const [roomList, setRoomList] = useState([])
  const { tenantList, setTenantList, actionValue, setActionValue } = useContext(AppContext)
  useEffect(() => {
    fetchTenant()
  }, [])

  const fetchTenant = async () => {
    let token = localStorage.getItem('token')
    await api
      .get('/residents/fetch', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        setTenantList(res.data)
      })
  }

  const getFullName = (data) => {
    return data.lastName + ',' + ' ' + data.firstName + ' ' + data.middleName
  }

  const cutBirthday = (string) => {
    let date = new Date(string)
    const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
    const day = date.getUTCDate()
    const year = date.getUTCFullYear()

    let fullDate = month + ' ' + day + ', ' + year
    return fullDate
  }

  const handleFilter = async (e, room) => {
    console.log(room)
    if (room === null) {
      fetchTenant()
    }
    const roomFilter = room.communityName
    let token = localStorage.getItem('token')
    await api
      .get('/residents/fetch', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setTenantList(res.data)

        console.log(tenantList)

        const newFilter = res.data.filter((value) => {
          return value.communityName === roomFilter
        })

        console.log(newFilter)
        setTenantList(newFilter)
      })
  }

  // function for search input
  const handleSearchFilter = async (event) => {
    const searchWord = event.target.value
    setWordEntered(searchWord)
    console.log(searchWord)

    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    await api
      .get('/residents/fetch', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setTenantList(res.data)

        const newFilter = res.data.filter((value) => {
          let fullName = value.firstName + ' ' + value.middleName + ' ' + value.lastName

          return fullName.toLowerCase().match(searchWord.toLowerCase())
        })
        setTenantList(newFilter)
      })
  }

  const setResidentIdForSosDetails = (id) => {
    localStorage.setItem('residentSosId', id)
  }

  return (
    <>
      <CCard>
        <CCardHeader id="card-header">
          <strong id="res-title">Residents</strong>
          <div id="search-container">
            <Autocomplete
              size="small"
              options={tenantList}
              className="mt-2"
              id="controllable-states-demo"
              getOptionLabel={(option) => getFullName(option)}
              sx={{ width: 250 }}
              onSelect={(event, data) => {
                handleSearchFilter(event)
                // setRoom(room.communityName)
              }}
              renderInput={(params) => <TextField {...params} placeholder="Search" />}
            />
          </div>
        </CCardHeader>
        <CCardBody>
          <CRow xs={{ gutterY: 3 }} className={isPopupOpen ? ' disabled-pointer-event' : ''}>
            {tenantList.length === 0 ? <h2 className="mx-auto">No Data Found</h2> : null}
            {tenantList.map((val, key) => {
              return (
                <CCol xl={2} md={4} key={key}>
                  <CCard
                    id="card-holder"
                    className="btn p-0 mx-auto"
                    md={{ width: '15rem' }}
                    style={{ width: '12rem', minHeight: '18rem' }}
                  >
                    <div
                      onClick={(e) => {
                        setResidentIdForSosDetails(val.id)
                        localStorage.setItem('sosPopisOpen', 'open')
                        setIsPopupOpen(true)
                      }}
                    >
                      <SosPopup />
                    </div>
                    <div id="card-bgc">
                      <div id="tenant-image-holder">
                        <img id="tenant-image" src={val.picture} />
                      </div>
                    </div>
                    <CCardBody className="mt-5">
                      <CCardTitle>{getFullName(val)}</CCardTitle>
                      <div
                        style={{ position: 'absolute', bottom: '10px', left: '25%' }}
                        className="d-flex justify-content-center"
                      >
                        <small className="pr-2">{cutBirthday(val.birthday)}</small>
                      </div>
                    </CCardBody>
                  </CCard>
                </CCol>
              )
            })}
          </CRow>
        </CCardBody>
      </CCard>
    </>
  )
}

export default Sos
