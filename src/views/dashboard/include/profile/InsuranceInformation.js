import React, { useContext, useEffect, useState } from 'react'
import { Row, Col, Table } from 'react-bootstrap'
import AppContext from '../../../../AppContext'
import InputLabel from '@mui/material/InputLabel'
import FormControl from '@mui/material/FormControl'
import NativeSelect from '@mui/material/NativeSelect'
import { Grid, Box, CardActionArea } from '@mui/material'
import { DataGrid } from '@mui/x-data-grid'
// axios api
import api from '../../../../api/api'

import Popup from '../../popup/Popup'
import AddNewAllergyForm from '../../form/AddNewAllergyForm'
import Swal from 'sweetalert2'
import EditPopup from '../../popup/EditPopup'
import EditAllergyForm from '../../form/EditAllergyForm'

// Icons
import SearchIcon from '@mui/icons-material/Search'
import InputBase from '@material-ui/core/InputBase'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'
import AttachmentIcon from '@mui/icons-material/Attachment'
import EditIcon from '@mui/icons-material/Edit'

function InsuranceInformation() {
  // global variables
  const { allergiesList, setAllergiesList, currentTab, setCurrentTab } = useContext(AppContext)
  // state for search word
  const [wordEntered, setWordEntered] = useState('')
  const [allergyTypeInput, setAllergyTypeInput] = useState('')
  const [isFetchDone, setIsFetchDone] = useState(false)
  let token = localStorage.getItem('token')

  const formatDate = (string) => {
    if (string === undefined || string === '') {
      return ''
    } else {
      let date = new Date(string)
      const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
      const day = date.getUTCDate()
      const year = date.getUTCFullYear()

      let fullDate = month + ' ' + day + ', ' + year
      return fullDate
    }
  }

  return (
    <div id="container" className="mt-3">
      <div className="title-holder d-flex">
        <h1 className="mx-auto mt-2">Insurance Information</h1>
      </div>
    </div>
  )
}

export default InsuranceInformation
