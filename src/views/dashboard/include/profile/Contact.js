import React, { useState, useEffect, useContext } from 'react'
import Box from '@mui/material/Box'
import TextField from '@mui/material/TextField'
import { Row, Col, Container } from 'react-bootstrap'
import Divider from '@mui/material/Divider'
// axios api
import api from '../../../../api/api'
import ContactPersonPopup from '../../popup/ContactPersonPopup'
import AppContext from '../../../../AppContext'

function Contact() {
  // useState for input field
  const {
    allergiesList,
    setAllergiesList,
    currentTab,
    setCurrentTab,
    contactPeopleData,
    setContactPeopleData,
  } = useContext(AppContext)
  const [fullName, setFullName] = useState('')
  const [contactNo, setContactNo] = useState('')
  const [address, setAddress] = useState('')

  let tenantId = localStorage.getItem('tenantId')

  const fetchResidentData = async () => {
    let token = localStorage.getItem('token')
    await api
      .get(`/residents/${tenantId}/fetchAllData`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        setContactPeopleData(res.data.ContactPeople)
      })
  }

  const getFullName = (data) => {
    return data.lastName + ',' + ' ' + data.firstName + ' ' + data.middleName
  }

  // const setInitialValue = (data) => {
  //   setAddress(data.ContactPerson.address)
  //   setContactNo(data.ContactPerson.contactNo)
  //   setFullName(getFullName(data.ContactPerson))
  // }

  useEffect(() => {
    fetchResidentData()
  }, [tenantId])

  return (
    <Container fluid>
      <ContactPersonPopup />
      <Row>
        {contactPeopleData.map((data, index) => (
          <>
            <div className="mt-4">
              <h4 style={{ textTransform: 'capitalize' }}>
                {index + 1 + '.' + ' '}
                {getFullName(data)}
              </h4>
              <div className="d-flex">
                <Col md={2}>
                  <TextField
                    style={{ textTransform: 'capitalize' }}
                    label="Relationship"
                    value={data.relationship}
                    variant="standard"
                  />
                </Col>
                <Col md={2}>
                  {data.ContactPersonEmails != null && data.ContactPersonEmails != undefined ? (
                    data.ContactPersonEmails.map((item) => (
                      <TextField
                        style={{ textTransform: 'capitalize', marginBottom: '8px' }}
                        label="Email"
                        value={item.email}
                        variant="standard"
                      />
                    ))
                  ) : (
                    <TextField
                      style={{ textTransform: 'capitalize' }}
                      label="Email"
                      value={'No Email Available'}
                      variant="standard"
                    />
                  )}
                </Col>
                <Col md={2}>
                  {data.ContactPersonPhones != null && data.ContactPersonPhones != undefined ? (
                    data.ContactPersonPhones.map((item) => (
                      <TextField
                        style={{ textTransform: 'capitalize', marginBottom: '8px' }}
                        label="Contact Number"
                        value={item.phoneNumber}
                        variant="standard"
                      />
                    ))
                  ) : (
                    <TextField
                      style={{ textTransform: 'capitalize' }}
                      label="Contact Number"
                      value={'No Contact Available'}
                      variant="standard"
                    />
                  )}
                </Col>
                <Col md={2}>
                  {data.ContactPersonAddresses != null &&
                  data.ContactPersonAddresses != undefined ? (
                    data.ContactPersonAddresses.map((item) => (
                      <TextField
                        style={{ textTransform: 'capitalize', marginBottom: '8px' }}
                        label="Address"
                        value={item.address}
                        variant="standard"
                      />
                    ))
                  ) : (
                    <TextField
                      style={{ textTransform: 'capitalize' }}
                      label="Address"
                      value={'No Contact Available'}
                      variant="standard"
                    />
                  )}
                </Col>
                <Col md={4}>
                  <TextField
                    style={{ textTransform: 'capitalize', width: '100%' }}
                    label="Responsibility"
                    value={data.responsibility}
                    variant="standard"
                  />
                </Col>
              </div>
            </div>
            <Divider className="mt-4" />
          </>
        ))}
      </Row>
    </Container>
  )
}

export default Contact
