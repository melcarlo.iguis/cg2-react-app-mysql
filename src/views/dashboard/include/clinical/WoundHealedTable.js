import React, { useState, useEffect } from 'react'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import Paper from '@mui/material/Paper'
import api from '../../../../api/api'

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein }
}

const rows = [
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  createData('Eclair', 262, 16.0, 24, 6.0),
  createData('Cupcake', 305, 3.7, 67, 4.3),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
]

export default function WoundHealedTable() {
  const [healWoundData, setHealWoundData] = useState([])
  const fetchHealedWound = () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')
    api
      .get(`/wound/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res.data)
        setHealWoundData(res.data)
      })
      .catch((err) => {
        console.table(err)
      })
  }

  useEffect(() => {
    fetchHealedWound()
  }, [])
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>General Wound Location</TableCell>
            <TableCell>Specific Wound Location</TableCell>
            <TableCell>Date Healed</TableCell>
            <TableCell>Wound Image</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {healWoundData.map((row, key) => (
            <TableRow key={key} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
              <TableCell>{row.generalWoundLocation}</TableCell>
              <TableCell>{row.specificWoundLocation}</TableCell>
              <TableCell>{row.onSetDate}</TableCell>
              <TableCell>
                <a href={row.picture} target="_blank">
                  Image
                </a>{' '}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}
