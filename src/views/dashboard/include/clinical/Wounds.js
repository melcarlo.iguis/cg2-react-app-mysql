import React, { useEffect, useState } from 'react'
import { Container, Row, Col, Form } from 'react-bootstrap'
import bodyDiagram from '../../../../assets/images/body-diagram.png'
import leftHand from '../../../../assets/images/left-hand.png'
import rightHand from '../../../../assets/images/right-hand.png'
import rightFoot from '../../../../assets/images/right-foot.png'
import leftFoot from '../../../../assets/images/left-foot.png'
import head from '../../../../assets/images/head.png'
import chest from '../../../../assets/images/chest.png'
import abdomen from '../../../../assets/images/abdomen.png'
import LocalHospitalIcon from '@mui/icons-material/LocalHospital'
import Box from '@mui/material/Box'
import TextField from '@mui/material/TextField'
import { ImageMap } from '@qiuz/react-image-map'
import Autocomplete from '@mui/material/Autocomplete'
import { CButton } from '@coreui/react'
import Radio from '@mui/material/Radio'
import RadioGroup from '@mui/material/RadioGroup'
import FormControlLabel from '@mui/material/FormControlLabel'
import FormControl from '@mui/material/FormControl'
import FormLabel from '@mui/material/FormLabel'
import moment from 'moment'
import Accordion from '@mui/material/Accordion'
import AccordionDetails from '@mui/material/AccordionDetails'
import AccordionSummary from '@mui/material/AccordionSummary'
import Typography from '@mui/material/Typography'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
// accordion
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import api from '../../../../api/api'
import { FaBeer, FaMinus, FaPlus, FaArrowLeft } from 'react-icons/fa'

function Wounds() {
  const [expanded, setExpanded] = React.useState('panel1')

  const d = new Date()
  const month = '' + (d.getMonth() + 1)
  const day = '' + d.getDate()
  const year = d.getFullYear()
  const newMonth = month.length < 2 ? '0' + month : month
  const newDay = day.length < 2 ? '0' + day : day

  let fullDate = year + '-' + newMonth + '-' + newDay
  console.log(fullDate)

  const date = new Date()
  const hour = date.getHours()
  const min = date.getMinutes()
  const newMins = min < 10 ? '0' + min : min
  const newHour = hour < 10 ? '0' + hour : hour
  // var ampm = date.getHours() >= 12 ? 'PM' : 'AM'

  let fullTime = newHour + ':' + newMins

  console.log(fullTime)
  // states
  const [woundedBodySegment, setWoundedBodySegment] = React.useState({ label: '' })
  const [generalBodySegment, setGeneralBodySegment] = React.useState({ label: '' })
  const [specificWoundLoc, setSpecificWoundLoc] = React.useState({ label: '' })
  const [dateDiscover, setDateDiscover] = React.useState(fullDate)
  const [timeDiscover, setTimeDiscover] = React.useState(fullTime)
  const [onsetDate, setOnsetDate] = React.useState('')
  const [onsetTime, setOnsetTime] = React.useState('')
  const [woundPresentOn, setWoundPresentOn] = React.useState('true')
  const [woundHealing, setWoundHealing] = React.useState('true')
  const [anatomicalDirection, setAnatomicalDirection] = React.useState(false)
  const [redness, setRedness] = React.useState('')

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false)
  }
  const [image, setImage] = useState('')
  const [imageName, setImageName] = useState('')
  const [imageFile, setImageFile] = useState('')

  const handleFileInputChange = (e) => {
    const file = e.target.files[0]
    setImageName(file.name)
    previewFile(file)
    setImageFile(file)
  }

  const previewFile = (file) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onloadend = () => {
      setImage(reader.result)
    }
  }

  const removeImage = (e) => {
    setImage('')
    setImageFile('')
    setImageName('')
  }

  const arr = [
    { label: 'Head' },
    { label: 'Back' },
    { label: 'Lower Back' },
    { label: 'Chest' },
    { label: 'Arm' },
  ]

  const addWoundData = (e) => {
    e.preventDefault()

    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')
    console.log(tenantId)

    const dateDiscoverDateTime = moment(dateDiscover + ' ' + timeDiscover)
    const onsetDateTime =
      onsetDate == '' && onsetTime == '' ? 0 : moment(onsetDate + ' ' + onsetTime)

    // console.log(onsetDateTime)

    const formData = new FormData()

    formData.append('image', imageFile)
    formData.append('woundedBodySegment', woundedBodySegment.label)
    formData.append('generalWoundLocation', generalBodySegment.label)
    formData.append('specificWoundLocation', specificWoundLoc.label)
    formData.append('dateDiscover', dateDiscoverDateTime)
    formData.append('onSetDate', onsetDateTime)
    formData.append('woundPresentOn', woundPresentOn)
    formData.append('woundHealing', woundHealing)
    formData.append('anatomicalDirection', anatomicalDirection)
    formData.append('redness', redness)
    formData.append('ResidentId', tenantId)

    // api call for adding new tenant
    api
      .post('/wound/add', formData, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)

        toast.success('Added Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
        // localStorage.setItem('tenantId', result.data.id)
        // setTenantList([...tenantList, result.data])
        // setButtonIsEnable(false)

        // // code for adding contact details for tenant
        // const input = {
        //   firstName: contactFirstName,
        //   middleName: contactMiddleName,
        //   lastName: contactLastName,
        //   contactNo: contactNo,
        //   address: contactAddress,
        //   ResidentId: result.data.id,
        // }

        // api call for adding new tenant
        // // api
        // //   .post(`/contact/add`, input, {
        // //     headers: {
        // //       Authorization: `Bearer ${token}`,
        // //     },
        // //   })
        // //   .then((result) => {
        // //     console.log(result)

        // //     // adding history to database
        // //     const input2 = {
        // //       title: `${name} input a new tenant on the database`,
        // //       tenantName:
        // //         result.data.firstName + ' ' + result.data.middleName + ' ' + result.data.lastName,
        // //       tenantId: result.data._id,
        // //       userName: name,
        // //     }
        // //     api
        // //       .post(`/history/create/`, input2, {
        // //         headers: {
        // //           Authorization: `Bearer ${token}`,
        // //         },
        // //       })
        // //       .then((result) => {
        // //         // Clear Input fields
        // //         setImage('')
        // //         setFirstName('')
        // //         setMiddleName('')
        // //         setLastName('')
        // //         setAddress('')
        // //         setBirthday('')
        // //         setStatus('')
        // //         setContactFirstName('')
        // //         setContactMiddleName('')
        // //         setContactLastName('')
        // //         setContactNo('')
        // //         setContactAddress('')

        // //         // delay function

        // //         setTimeout(function () {
        // //           setDialogClose(false)
        // //         }, 2000)
        // //       })
        // //       .catch((err) => {
        // //         console.error(err)
        // //       })
        //   })
      })
      .catch((err) => {
        console.log(err)
      })
  }

  // image  map body diagram
  const [hoverName, setHoverName] = useState('')
  const [selectedBodyPart, setSelectedBodyPart] = useState('')
  const [showSelectedPart, setShowSelectedPart] = useState('false')
  const [diagramToDisplay, setDiagramToDisplay] = useState('bodyDiagram')
  const onMapClick = (area, index) => {
    if (index == 0) {
      console.log('right hand')
      setWoundedBodySegment({ label: 'Upper extremities' })
      setGeneralBodySegment({ label: 'Right Hand' })
      setSpecificWoundLoc({ label: '' })
      setDiagramToDisplay('rightHand')
    } else if (index == 1) {
      setWoundedBodySegment({ label: 'Upper extremities' })
      setGeneralBodySegment({ label: 'Left Hand' })
      setSpecificWoundLoc({ label: '' })
      setDiagramToDisplay('leftHand')
      console.log('left')
    } else if (index == 2) {
      setWoundedBodySegment({ label: 'Head' })
      setGeneralBodySegment({ label: 'Head' })
      setSpecificWoundLoc({ label: '' })
      setDiagramToDisplay('head')
      console.log('head')
    } else if (index == 3) {
      setWoundedBodySegment({ label: 'Trunk' })
      setGeneralBodySegment({ label: 'Chest' })
      setSpecificWoundLoc({ label: '' })
      setDiagramToDisplay('trunk')
    } else if (index == 4) {
      setWoundedBodySegment({ label: 'Trunk' })
      setGeneralBodySegment({ label: 'Abdomen' })
      setSpecificWoundLoc({ label: '' })
      setDiagramToDisplay('abdomen')
    } else if (index == 5) {
      setWoundedBodySegment({ label: 'Lower Extremities' })
      setGeneralBodySegment({ label: 'Right Foot' })
      setSpecificWoundLoc({ label: '' })
      setDiagramToDisplay('rightFoot')
    } else if (index == 6) {
      setWoundedBodySegment({ label: 'Lower Extremities' })
      setGeneralBodySegment({ label: 'Left Foot' })
      setSpecificWoundLoc({ label: '' })
      setDiagramToDisplay('leftFoot')
    } else if (index == 7) {
      setWoundedBodySegment({ label: 'Lower Extremities' })
      setGeneralBodySegment({ label: 'Lower Leg' })
      setSpecificWoundLoc({ label: 'Right Lower Leg' })
      // setDiagramToDisplay('chest')
    } else if (index == 8) {
      setWoundedBodySegment({ label: 'Lower Extremities' })
      setGeneralBodySegment({ label: 'Lower Leg' })
      setSpecificWoundLoc({ label: 'Left Lower Leg' })
      // setDiagramToDisplay('chest')
    } else if (index == 9) {
      setWoundedBodySegment({ label: 'Lower Extremities' })
      setGeneralBodySegment({ label: 'Upper Leg' })
      setSpecificWoundLoc({ label: 'Right Upper Leg' })
      // setDiagramToDisplay('chest')
    } else if (index == 10) {
      setWoundedBodySegment({ label: 'Lower Extremities' })
      setGeneralBodySegment({ label: 'Upper Leg' })
      setSpecificWoundLoc({ label: 'Left Upper Leg' })
      // setDiagramToDisplay('chest')
    } else if (index == 11) {
      setWoundedBodySegment({ label: 'Lower Extremities' })
      setGeneralBodySegment({ label: 'Knee' })
      setSpecificWoundLoc({ label: 'Right Knee' })
      // setDiagramToDisplay('chest')
    } else if (index == 12) {
      setWoundedBodySegment({ label: 'Lower Extremities' })
      setGeneralBodySegment({ label: 'Knee' })
      setSpecificWoundLoc({ label: 'Left Knee' })
      // setDiagramToDisplay('chest')
    } else if (index == 13) {
      setWoundedBodySegment({ label: 'Lower Extremities' })
      setGeneralBodySegment({ label: 'Hip' })
      setSpecificWoundLoc({ label: 'Hip' })
      // setDiagramToDisplay('chest')
    } else if (index == 14) {
      setWoundedBodySegment({ label: 'Upper extremities' })
      setGeneralBodySegment({ label: 'Lower Arm' })
      setSpecificWoundLoc({ label: 'Right Lower Arm' })
      // setDiagramToDisplay('chest')
    } else if (index == 15) {
      setWoundedBodySegment({ label: 'Upper extremities' })
      setGeneralBodySegment({ label: 'Lower Arm' })
      setSpecificWoundLoc({ label: 'Left Lower Arm' })
      // setDiagramToDisplay('chest')
    } else if (index == 16) {
      setWoundedBodySegment({ label: 'Upper extremities' })
      setGeneralBodySegment({ label: 'Upper Arm' })
      setSpecificWoundLoc({ label: 'Right Lower Arm' })
      // setDiagramToDisplay('chest')
    } else if (index == 17) {
      setWoundedBodySegment({ label: 'Upper extremities' })
      setGeneralBodySegment({ label: 'Upper Arm' })
      setSpecificWoundLoc({ label: 'Left Lower Arm' })
      // setDiagramToDisplay('chest')
    } else if (index == 18) {
      setWoundedBodySegment({ label: 'Upper extremities' })
      setGeneralBodySegment({ label: 'Elbow' })
      setSpecificWoundLoc({ label: 'Right Elbow' })
      // setDiagramToDisplay('chest')
    } else if (index == 19) {
      setWoundedBodySegment({ label: 'Upper extremities' })
      setGeneralBodySegment({ label: 'Elbow' })
      setSpecificWoundLoc({ label: 'Left Elbow' })
      // setDiagramToDisplay('chest')
    } else if (index == 20) {
      setWoundedBodySegment({ label: 'Upper extremities' })
      setGeneralBodySegment({ label: 'Shoulder' })
      setSpecificWoundLoc({ label: 'Right Shoulder' })
      // setDiagramToDisplay('chest')
    } else if (index == 21) {
      setWoundedBodySegment({ label: 'Upper extremities' })
      setGeneralBodySegment({ label: 'Shoulder' })
      setSpecificWoundLoc({ label: 'Left Shoulder' })
      // setDiagramToDisplay('chest')
    } else if (index == 22) {
      setWoundedBodySegment({ label: 'hasjdksajsa extremities' })
      setGeneralBodySegment({ label: 'Shoulder' })
      setSpecificWoundLoc({ label: 'Right Shoulder' })
      // setDiagramToDisplay('chest')
    }
    // const tip = `click map${index + 1}`
    // console.log(tip)
    // alert(tip)
  }

  const onMapClickHead = (area, index) => {
    if (index == 0) {
      setSpecificWoundLoc({ label: 'Nose' })
    } else if (index == 1) {
      setSpecificWoundLoc({ label: 'Left Eye' })
    } else if (index == 2) {
      setSpecificWoundLoc({ label: 'Right Eye' })
    } else if (index == 3) {
      setSpecificWoundLoc({ label: 'Forehead' })
    } else if (index == 4) {
      setSpecificWoundLoc({ label: 'Right Ear' })
    } else if (index == 5) {
      setSpecificWoundLoc({ label: 'Mouth' })
    } else if (index == 6) {
      setSpecificWoundLoc({ label: 'Left Ear' })
    } else if (index == 7) {
      setSpecificWoundLoc({ label: 'Chin' })
    } else if (index == 8) {
      setSpecificWoundLoc({ label: 'Right Jaw' })
    } else if (index == 9) {
      setSpecificWoundLoc({ label: 'Left Jaw' })
    } else if (index == 10) {
      setSpecificWoundLoc({ label: 'Neck' })
    } else if (index == 11) {
      setSpecificWoundLoc({ label: 'Right Cheek' })
    } else if (index == 12) {
      setSpecificWoundLoc({ label: 'Left Cheek' })
    }
    // const tip = `click map${index + 1}`
    // console.log(tip)
    // alert(tip)
  }

  const onMapClickChest = (area, index) => {
    if (index == 0) {
      console.log('thumb index')
      setSpecificWoundLoc({ label: 'Right Upper chest' })
    } else if (index == 1) {
      setSpecificWoundLoc({ label: 'Left Upper Chest' })
      console.log('left')
    } else if (index == 2) {
      setSpecificWoundLoc({ label: 'Right Middle Chest' })
    } else if (index == 3) {
      setSpecificWoundLoc({ label: 'Left Middle Chest' })
    } else if (index == 4) {
      setSpecificWoundLoc({ label: 'Right Lower Chest' })
    } else if (index == 5) {
      setSpecificWoundLoc({ label: 'Left Lower Chest' })
    }
    // const tip = `click map${index + 1}`
    // console.log(tip)
    // alert(tip)
  }

  const onMapClickAbdomen = (area, index) => {
    if (index == 0) {
      console.log('thumb index')
      setSpecificWoundLoc({ label: 'Umbilical Region' })
    } else if (index == 1) {
      setSpecificWoundLoc({ label: 'Right Lumbar' })
      console.log('left')
    } else if (index == 2) {
      setSpecificWoundLoc({ label: 'Left Lumbar' })
    } else if (index == 3) {
      setSpecificWoundLoc({ label: 'Left Hypochondrium' })
    } else if (index == 4) {
      setSpecificWoundLoc({ label: 'Epigastric Region' })
    } else if (index == 5) {
      setSpecificWoundLoc({ label: 'Right Hypochondrium' })
    } else if (index == 6) {
      setSpecificWoundLoc({ label: 'Right Iliac Region' })
    } else if (index == 7) {
      setSpecificWoundLoc({ label: 'Hyprogastrium' })
    } else if (index == 8) {
      setSpecificWoundLoc({ label: 'Left Iliac Region' })
    }
    // const tip = `click map${index + 1}`
    // console.log(tip)
    // alert(tip)
  }

  const onMapClickRightFoot = (area, index) => {
    if (index == 0) {
      console.log('thumb index')
      setSpecificWoundLoc({ label: 'Toe' })
    } else if (index == 1) {
      setSpecificWoundLoc({ label: 'Ball of the foot' })
      console.log('left')
    } else if (index == 2) {
      setSpecificWoundLoc({ label: 'Heel' })
    } else if (index == 3) {
      setSpecificWoundLoc({ label: 'Arch' })
    } else if (index == 4) {
      setSpecificWoundLoc({ label: 'Instep' })
    } else if (index == 5) {
      setSpecificWoundLoc({ label: 'Ankle' })
    }
    // const tip = `click map${index + 1}`
    // console.log(tip)
    // alert(tip)
  }

  const onMapClickRightHand = (area, index) => {
    if (index == 0) {
      console.log('thumb index')
      setSpecificWoundLoc({ label: 'Thumb Finger' })
    } else if (index == 1) {
      setSpecificWoundLoc({ label: 'Index Finger' })
      console.log('left')
    } else if (index == 2) {
      setSpecificWoundLoc({ label: 'Middle Finger' })
    } else if (index == 3) {
      setSpecificWoundLoc({ label: 'Ring Finger' })
    } else if (index == 4) {
      setSpecificWoundLoc({ label: 'Little Finger' })
    } else if (index == 5) {
      setSpecificWoundLoc({ label: 'Palm' })
    }
    // const tip = `click map${index + 1}`
    // console.log(tip)
    // alert(tip)
  }

  const onMapClickLeftHand = (area, index) => {
    if (index == 0) {
      console.log('thumb index')
      setSpecificWoundLoc({ label: 'Thumb Finger' })
    } else if (index == 1) {
      setSpecificWoundLoc({ label: 'Little Finger' })
      console.log('left')
    } else if (index == 2) {
      setSpecificWoundLoc({ label: 'Ring Finger' })
    } else if (index == 3) {
      setSpecificWoundLoc({ label: 'Middle Finger' })
    } else if (index == 4) {
      setSpecificWoundLoc({ label: 'Index Finger' })
    }
    // const tip = `click map${index + 1}`
    // console.log(tip)
    // alert(tip)
  }

  const abdomenAea = [
    {
      width: '20.10562414266118%',
      height: '21.20353982300885%',
      left: '37.58573388203018%',
      top: '31.68141592920354%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Umbilical Region'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '20.10562414266118%',
      height: '21.20353982300885%',
      left: '17.695473251028815%',
      top: '31.858407079646017%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Right Lumbar'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '20.10562414266118%',
      height: '21.20353982300885%',
      left: '57.61316872427984%',
      top: '31.68141592920354%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Left Lumbar'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '20.10562414266118%',
      height: '21.20353982300885%',
      left: '57.475994513031544%',
      top: '10.442477876106194%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Left Hypochondrium'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '20.10562414266118%',
      height: '21.20353982300885%',
      left: '37.44855967078188%',
      top: '10.442477876106194%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Epigastric Region'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '20.10562414266118%',
      height: '21.20353982300885%',
      left: '17.832647462277084%',
      top: '10.265486725663717%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Righ Hypochondrium'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '20.10562414266118%',
      height: '21.20353982300885%',
      left: '17.558299039780515%',
      top: '53.09734513274337%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Right Iliac Region'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '20.10562414266118%',
      height: '21.20353982300885%',
      left: '37.722908093278456%',
      top: '52.74336283185842%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Hyprogastrium'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '20.10562414266118%',
      height: '21.20353982300885%',
      left: '57.61316872427984%',
      top: '52.920353982300895%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Left Iliac Region'),
      onMouseLeave: () => setHoverName(''),
    },
  ]

  const headArea = [
    {
      width: '7.622770919067213%',
      height: '14.04638472032742%',
      left: '47.599451303155%',
      top: '35.279672578444774%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Nose'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '11.052126200274346%',
      height: '8.043656207366984%',
      left: '55.14403292181069%',
      top: '35.00682128240111%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Left Eye'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '11.052126200274346%',
      height: '8.043656207366984%',
      left: '37.03703703703703%',
      top: '35.27967257844477%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Right Eye'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '36.84087791495199%',
      height: '17.45702592087312%',
      left: '32.51028806584362%',
      top: '17.271487039563453%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Forehead'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '7.074074074074073%',
      height: '17.45702592087312%',
      left: '26.200274348422493%',
      top: '36.9167803547067%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Right Ear'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '17.00960219478738%',
      height: '7.91268758526603%',
      left: '42.74691358024691%',
      top: '51.4324693042292%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Mouth'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '8.367626886145404%',
      height: '17.598908594815825%',
      left: '67.57544581618656%',
      top: '35.334242837653484%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Left Ear'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '16.872427983539097%',
      height: '7.503410641200546%',
      left: '42.88408779149519%',
      top: '58.93587994542971%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Chin'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '9.739368998628258%',
      height: '9.822646657571624%',
      left: '33.830589849108364%',
      top: '52.933152473314394%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Right Jaw'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '9.739368998628258%',
      height: '9.822646657571624%',
      left: '59.482167352537715%',
      top: '52.38744988122709%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Left Jaw'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '44.99314128943759%',
      height: '6.548431105047749%',
      left: '22.445130315500617%',
      top: '65.89358799454301%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Neck'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '10.562414266117969%',
      height: '9.549795361527966%',
      left: '33.41906721536351%',
      top: '43.51978210928007%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Right Cheek'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '10.562414266117969%',
      height: '9.549795361527966%',
      left: '57.01303155006858%',
      top: '43.11050516521458%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Left Cheek'),
      onMouseLeave: () => setHoverName(''),
    },
  ]

  const rightHandArea = [
    {
      width: '13.580246913580247%',
      height: '22.493224932249323%',
      left: '17.232510288065843%',
      top: '27.506774809302353%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Thumb Finger'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '10.83676268861454%',
      height: '22.76422764227642%',
      left: '23.131001371742112%',
      top: '48.91598890144328%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Index Finger'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '9.327846364883401%',
      height: '22.76422764227642%',
      left: '33.28189300411522%',
      top: '53.92953903694463%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Middle Finger'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '9.327846364883401%',
      height: '22.76422764227642%',
      left: '42.335390946502045%',
      top: '54.87804852203948%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Ring Finger'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '9.327846364883401%',
      height: '22.76422764227642%',
      left: '51.93758573388202%',
      top: '53.116530906863325%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Little Finger'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '32.92181069958848%',
      height: '28.86178861788618%',
      left: '32.18449931412894%',
      top: '24.796747967479675%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Palm'),
      onMouseLeave: () => setHoverName(''),
    },
  ]

  const leftHandArea = [
    {
      width: '22.026063100137172%',
      height: '27.229885057471257%',
      left: '55.28120713305895%',
      top: '22.011494252873582%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Thumb Finger'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '11.659807956104252%',
      height: '21.120689655172413%',
      left: '32.5960219478738%',
      top: '50.431034264379534%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Little Finger'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '11.796982167352537%',
      height: '22.844827586206897%',
      left: '42.60973936899863%',
      top: '52.155174606147895%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Ring Finger'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '9.602194787379972%',
      height: '15.22988505747126%',
      left: '53.17215363511663%',
      top: '60.919540229885094%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Middle Finger'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '14.40329218106996%',
      height: '17.52873563218391%',
      left: '57.97325102880658%',
      top: '50.57471308900022%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Index Finger'),
      onMouseLeave: () => setHoverName(''),
    },
  ]

  const leftFootArea = [
    {
      width: '25.318244170096023%',
      height: '22.928961748633885%',
      left: '64.19753086419753%',
      top: '44.04371584699453%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Toe'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '23.123456790123456%',
      height: '18.010928961748636%',
      left: '56.37860082304527%',
      top: '66.44808743169398%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Ball of the foot'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '20.16460905349794%',
      height: '29.781420765027317%',
      left: '24.365569272976693%',
      top: '59.56284153005464%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('heel'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '12.482853223593956%',
      height: '25.683060109289613%',
      left: '44.530178326474655%',
      top: '61.20218579234973%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('arch'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '25.102880658436206%',
      height: '25.683060109289613%',
      left: '39.18038408779152%',
      top: '29.508196721311474%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('instep'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '16.87242798353908%',
      height: '25.683060109289613%',
      left: '22.17078189300416%',
      top: '30.601092896174865%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('ankle'),
      onMouseLeave: () => setHoverName(''),
    },
  ]

  const chestArea = [
    {
      width: '27.709190672153632%',
      height: '16.352201257861633%',
      left: '22.03360768175583%',
      top: '24.528301886792452%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('right upper chest'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '27.709190672153632%',
      height: '16.35220125786163%',
      left: '49.742798353909464%',
      top: '24.31865828092243%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('left upper chest'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '27.709190672153632%',
      height: '16.35220125786163%',
      left: '22.170781893004115%',
      top: '40.461215932914044%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('right middle chest'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '27.709190672153632%',
      height: '16.35220125786163%',
      left: '49.87997256515775%',
      top: '39.62264150943396%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('left middle chest'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '25.788751714677637%',
      height: '26.624737945492654%',
      left: '23.542524005486968%',
      top: '56.60377358490566%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('right lower chest'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '25.788751714677637%',
      height: '26.624737945492654%',
      left: '51.25171467764061%',
      top: '55.34591194968554%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('left lower chest'),
      onMouseLeave: () => setHoverName(''),
    },
  ]

  const rightFootArea = [
    {
      width: '25.65157750342936%',
      height: '21.584699453551913%',
      left: '10.099451303155007%',
      top: '44.80874316939891%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Toe'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '20.85048010973937%',
      height: '14.48087431693989%',
      left: '22.307956104252398%',
      top: '68.5792349726776%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('ball of the foot'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '17.421124828532236%',
      height: '18.852459016393443%',
      left: '56.18998628257887%',
      top: '68.5792349726776%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('heel'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '13.717421124828533%',
      height: '14.48087431693989%',
      left: '43.021262002743484%',
      top: '67.21311475409836%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('arch'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '29.903978052126202%',
      height: '32.78688524590164%',
      left: '34.24211248285322%',
      top: '27.049180327868854%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('instep'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '13.717421124828533%',
      height: '30.601092896174865%',
      left: '65.24348422496571%',
      top: '24.863387978142075%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('ankle'),
      onMouseLeave: () => setHoverName(''),
    },
  ]

  const bodyDiagramArea = [
    {
      //right hand
      width: '10.699588477366255%',
      height: '11.707988980716253%',
      left: '3.377914951989026%',
      top: '49.03581267217631%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Right Hand'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      // left hand
      width: '10.150891632373114%',
      height: '10.192837465564738%',
      left: '38.90603566529492%',
      top: '48.62259163344202%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Left Hand'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      // head
      width: '8.50480109739369%',
      height: '12.258953168044078%',
      left: '22.307956104252405%',
      top: '4.2699724517906334%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Head'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '18.51851851851852%',
      height: '14.049586776859504%',
      left: '17.36968449931413%',
      top: '23.829201101928373%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Chest'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '16.46090534979424%',
      height: '9.641873278236915%',
      left: '18.74142661179698%',
      top: '37.878788404228274%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Trunk'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '6.721536351165981%',
      height: '9.090909090909092%',
      left: '21.073388203017828%',
      top: '88.84297520661157%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Right Foot'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '7.681755829903978%',
      height: '9.090909090909092%',
      left: '27.65775034293555%',
      top: '88.70523836330281%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Left Foot'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '7.956104252400549%',
      height: '12.672176308539946%',
      left: '19.290123456790123%',
      top: '73.14049481688781%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Right Lower Leg'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '7.956104252400549%',
      height: '12.672176308539946%',
      left: '26.9718792866941%',
      top: '73.27823586371977%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Left Lower Leg'),
      onMouseLeave: () => setHoverName(''),
    },
    // {
    //   width: '6.858710562414267%',
    //   height: '3.3057851239669422%',
    //   left: '20.799039780521262%',
    //   top: '85.81267217630854%',
    //   style: { cursor: 'crosshair' },
    //   onMouseOver: () => setHoverName('right ankle'),
    //   onMouseLeave: () => setHoverName(''),
    // },
    // {
    //   width: '6.858710562414267%',
    //   height: '3.3057851239669422%',
    //   left: '26.9718792866941%',
    //   top: '85.9504132231405%',
    //   style: { cursor: 'crosshair' },
    //   onMouseOver: () => setHoverName('left ankle'),
    //   onMouseLeave: () => setHoverName(''),
    // },
    {
      width: '10.150891632373114%',
      height: '14.87603305785124%',
      left: '16.409465020576132%',
      top: '51.65289269334357%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('right upper leg'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '10.150891632373114%',
      height: '14.87603305785124%',
      left: '26.42318244170096%',
      top: '51.65289269334357%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Left upper leg'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '9.190672153635116%',
      height: '7.024793388429752%',
      left: '17.644032921810698%',
      top: '66.52892561983471%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('right knee'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '9.190672153635116%',
      height: '7.024793388429752%',
      left: '26.286008230452673%',
      top: '66.39118457300276%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('left knee'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '19.478737997256516%',
      height: '4.683195592286501%',
      left: '17.36968449931413%',
      top: '47.5206611570248%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Hip'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '8.50480109739369%',
      height: '10.055096418732782%',
      left: '8.179012345679013%',
      top: '39.11845730027548%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Right Lower Arm'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '8.50480109739369%',
      height: '10.055096418732782%',
      left: '36.84842249657064%',
      top: '39.25619834710744%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Left Lower Arm'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '5.6241426611796985%',
      height: '10.055096418732782%',
      left: '12.157064471879279%',
      top: '26.033057851239672%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('right upper Arm'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '5.6241426611796985%',
      height: '10.055096418732782%',
      left: '35.88820301783264%',
      top: '25.482093663911847%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('left upper Arm'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '7.270233196159122%',
      height: '3.443526170798898%',
      left: '10.648148148148147%',
      top: '35.67493112947658%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Right Elbow'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '7.270233196159122%',
      height: '3.71900826446281%',
      left: '35.75102880658436%',
      top: '35.39944903581267%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('Left Elbow'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '5.6241426611796985%',
      height: '5.922865013774105%',
      left: '12.157064471879286%',
      top: '20.385674931129476%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('right shoulder'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '5.6241426611796985%',
      height: '5.922865013774105%',
      left: '34.79080932784637%',
      top: '20.110192837465565%',
      style: { cursor: 'crosshair' },
      onMouseOver: () => setHoverName('left shoulder'),
      onMouseLeave: () => setHoverName(''),
    },
    {
      width: '12.208504801097392%',
      height: '2.3415977961432506%',
      left: '21.210562414266118%',
      top: '16.2534435261708%',
      style: { cursor: 'crosshair' },
    },
    {
      width: '18.106995884773664%',
      height: '5.5096418732782375%',
      left: '17.232510288065843%',
      top: '18.732782369146005%',
      style: { cursor: 'crosshair' },
    },
  ]

  ;<ImageMap
    className="usage-map"
    src={bodyDiagram}
    map={bodyDiagramArea}
    onMapClick={onMapClick}
  />
  ;<ImageMap
    className="usage-map"
    src={leftHand}
    map={leftHandArea}
    onMapClick={onMapClickLeftHand}
  />
  ;<ImageMap
    className="usage-map"
    src={rightHand}
    map={rightHandArea}
    onMapClick={onMapClickRightHand}
  />
  ;<ImageMap
    className="usage-map"
    src={rightFoot}
    map={rightFootArea}
    onMapClick={onMapClickRightFoot}
  />
  ;<ImageMap
    className="usage-map"
    src={leftFoot}
    map={leftFootArea}
    onMapClick={onMapClickRightFoot}
  />
  ;<ImageMap className="usage-map" src={head} map={headArea} onMapClick={onMapClickHead} />
  ;<ImageMap className="usage-map" src={chest} map={chestArea} onMapClick={onMapClickChest} />
  ;<ImageMap className="usage-map" src={abdomen} map={abdomenAea} onMapClick={onMapClickAbdomen} />

  const ImageMapComponentAbdomen = React.useMemo(
    () => (
      <ImageMap
        className="usage-map pt-5 mt-5"
        src={abdomen}
        map={abdomenAea}
        onMapClick={onMapClickAbdomen}
      />
    ),
    [abdomen],
  )

  const ImageMapComponentChest = React.useMemo(
    () => (
      <ImageMap
        className="usage-map pt-5 mt-5"
        src={chest}
        map={chestArea}
        onMapClick={onMapClickChest}
      />
    ),
    [chest],
  )

  const ImageMapComponentLeftFoot = React.useMemo(
    () => (
      <ImageMap
        className="usage-map pt-5 mt-5"
        src={leftFoot}
        map={leftFootArea}
        onMapClick={onMapClickRightFoot}
      />
    ),
    [rightFoot],
  )

  const ImageMapComponentRightFoot = React.useMemo(
    () => (
      <ImageMap
        className="usage-map pt-5 mt-5"
        src={rightFoot}
        map={rightFootArea}
        onMapClick={onMapClickRightFoot}
      />
    ),
    [rightFoot],
  )

  const ImageMapComponentRightHand = React.useMemo(
    () => (
      <ImageMap
        className="usage-map"
        src={rightHand}
        map={rightHandArea}
        onMapClick={onMapClickRightHand}
      />
    ),
    [rightHand],
  )

  const ImageMapComponent = React.useMemo(
    () => (
      <ImageMap
        className="usage-map"
        src={bodyDiagram}
        map={bodyDiagramArea}
        onMapClick={onMapClick}
      />
    ),
    [bodyDiagram],
  )

  const ImageMapComponentLeftHand = React.useMemo(
    () => (
      <ImageMap
        className="usage-map"
        src={leftHand}
        map={leftHandArea}
        onMapClick={onMapClickLeftHand}
      />
    ),
    [leftHand],
  )

  const ImageMapComponentHead = React.useMemo(
    () => <ImageMap className="usage-map" src={head} map={headArea} onMapClick={onMapClickHead} />,
    [head],
  )

  const handleWoundedBodySegment = (e, data) => {
    console.log(data)
    setWoundedBodySegment(data)
  }

  const handleGeneralBodySegment = (e, data) => {
    setGeneralBodySegment(data)
  }

  const handleSepecificLocation = (e, data) => {
    setSpecificWoundLoc(data)
  }

  const handleWoundPresentOn = (e) => {
    setWoundPresentOn(e.target.value)
  }

  const handleOnClick = (e, data) => {
    e.preventDefault()
    console.log('yoww')
    console.log(data)
  }

  return (
    <div>
      <Container className="position-relative">
        {hoverName != '' ? (
          <div id="body-diagram-label">
            <p className="m-0 p-0">{hoverName}</p>
          </div>
        ) : null}

        <Row>
          <Col md={6} sm={12} className="position-relative">
            {/* <div id="body-diagram-image-holder"> */}
            {diagramToDisplay != 'bodyDiagram' ? (
              <div
                id="back-btn-diagram"
                onClick={() => {
                  setDiagramToDisplay('bodyDiagram')
                  setWoundedBodySegment({ label: '' })
                  setGeneralBodySegment({ label: '' })
                  setSpecificWoundLoc({ label: '' })
                }}
              >
                <img src={bodyDiagram}></img>
              </div>
            ) : null}
            {/* <p id="body-diagram-label">{hoverName}</p> */}

            {/* <img usemap="#image-map" src={bodyDiagram} id="body-diagram-image" /> */}
            <div id="body-diagram-image">
              {diagramToDisplay == 'bodyDiagram' ? (
                <>{ImageMapComponent} </>
              ) : diagramToDisplay == 'leftHand' ? (
                <>{ImageMapComponentLeftHand}</>
              ) : diagramToDisplay == 'head' ? (
                <>{ImageMapComponentHead}</>
              ) : diagramToDisplay == 'rightHand' ? (
                <>{ImageMapComponentRightHand}</>
              ) : diagramToDisplay == 'rightFoot' ? (
                <>{ImageMapComponentRightFoot}</>
              ) : diagramToDisplay == 'leftFoot' ? (
                <>{ImageMapComponentLeftFoot}</>
              ) : diagramToDisplay == 'trunk' ? (
                <>{ImageMapComponentChest}</>
              ) : diagramToDisplay == 'abdomen' ? (
                <>{ImageMapComponentAbdomen}</>
              ) : null}
            </div>
          </Col>
          <Col md={6} sm={12}>
            <div id="image-selector-wound">
              <p id="no-image-text">No Image Selected</p>
              <div className="img-holder">
                {image ? <img id="wound-image" src={image} alt=""></img> : null}
              </div>
              <div className="image-footer d-flex">
                {image ? (
                  <>
                    <div
                      className="btn"
                      id={image ? 'remove-btn' : 'disabled-event'}
                      onClick={() => removeImage()}
                    >
                      <FaMinus
                        style={{
                          color: 'white',
                        }}
                      />
                    </div>
                    <p style={{ color: 'white' }}>{imageName}</p>
                  </>
                ) : null}
                <label id={image ? 'disabled-event' : ''} htmlFor="inputTag" className="d-flex">
                  {image ? null : (
                    <div className="btn mt-0 pt-0 px-2">
                      <FaPlus
                        style={{
                          color: 'white',
                          margin: '0',
                          fontSize: '20px',
                          paddingTop: '0',
                        }}
                      />
                    </div>
                  )}

                  <input
                    onChange={handleFileInputChange}
                    id="inputTag"
                    className="input"
                    type="file"
                  />
                </label>
              </div>
            </div>

            {/* input field starts here */}
            <Col md={12} sm={12}>
              <Row>
                <Col md={6} sm={12}>
                  <p className="mt-3">Wounded Body Segment: </p>{' '}
                </Col>

                <Col md={6} sm={12}>
                  {' '}
                  <Autocomplete
                    className="mt-3"
                    size="small"
                    value={woundedBodySegment}
                    id="country-select-demo"
                    options={arr}
                    autoHighlight
                    onChange={(event, data) => {
                      handleWoundedBodySegment(event, data)
                    }}
                    getOptionLabel={(option) => option.label}
                    renderOption={(props, option) => (
                      <Box component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...props}>
                        {option.label}
                      </Box>
                    )}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Choose.."
                        variant="standard"
                        inputProps={{
                          ...params.inputProps,
                        }}
                      />
                    )}
                  />
                </Col>
                <Col md={6} sm={12}>
                  <p className="mt-3">General Body Segment: </p>{' '}
                </Col>

                <Col md={6} sm={12}>
                  {' '}
                  <Autocomplete
                    className="mt-3"
                    size="small"
                    id="country-select-demo"
                    options={arr}
                    value={generalBodySegment}
                    autoHighlight
                    onChange={(event, data) => {
                      handleGeneralBodySegment(event, data)
                    }}
                    getOptionLabel={(option) => option.label}
                    renderOption={(props, option) => (
                      <Box component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...props}>
                        {option.label}
                      </Box>
                    )}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        variant="standard"
                        label="Choose.."
                        inputProps={{
                          ...params.inputProps,
                        }}
                      />
                    )}
                  />
                </Col>
                <Col md={6} sm={12}>
                  <p className="mt-3">Specific Wound Location: </p>{' '}
                </Col>

                <Col md={6} sm={12}>
                  {' '}
                  <Autocomplete
                    className="mt-3"
                    size="small"
                    id="country-select-demo"
                    options={arr}
                    autoHighlight
                    value={specificWoundLoc}
                    onChange={(event, data) => {
                      handleSepecificLocation(event, data)
                    }}
                    getOptionLabel={(option) => option.label}
                    renderOption={(props, option) => (
                      <Box component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...props}>
                        {option.label}
                      </Box>
                    )}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        variant="standard"
                        label="Choose.."
                        inputProps={{
                          ...params.inputProps,
                        }}
                      />
                    )}
                  />
                </Col>
              </Row>
            </Col>
          </Col>
          <Col md={12} sm={12} className="mt-3">
            <Row>
              <Col md={12} sm={12}>
                <Row>
                  <Col md={6} sm={12}>
                    <Row>
                      <Col md={4} sm={12} style={{ fontSize: '15px' }}>
                        Date Discovered
                      </Col>
                      <Col md={4} sm={12}>
                        <Form.Control
                          type="date"
                          name="date"
                          style={{
                            width: '100%',
                            textAlign: 'center',
                            height: '25px',
                            fontSize: '12px',
                          }}
                          required
                          value={dateDiscover}
                          label="Date"
                          onChange={(e) => setDateDiscover(e.target.value)}
                        />
                      </Col>
                      <Col md={4} sm={12}>
                        <Form.Control
                          type="time"
                          name="time"
                          style={{
                            width: '100%',
                            textAlign: 'center',
                            height: '25px',
                            fontSize: '12px',
                          }}
                          required
                          value={fullTime}
                          label="Time"
                          onChange={(e) => setTimeDiscover(e.target.value)}
                        />
                      </Col>
                    </Row>
                    <Row>
                      <Col className="mt-3" md={6} sm={12} style={{ fontSize: '15px' }}>
                        Was Wound presend on Admit to facility?
                      </Col>
                      <Col className="mt-3" md={6} sm={12}>
                        <FormControl>
                          <RadioGroup
                            row
                            value={woundPresentOn}
                            aria-labelledby="demo-row-radio-buttons-group-label"
                            name="row-radio-buttons-group"
                            onChange={(e) => handleWoundPresentOn(e)}
                          >
                            <FormControlLabel
                              style={{ fontSize: '15px' }}
                              value="true"
                              control={<Radio />}
                              label="yes"
                            />
                            <FormControlLabel
                              style={{ fontSize: '15px' }}
                              value="false"
                              control={<Radio />}
                              label="no"
                            />
                          </RadioGroup>
                        </FormControl>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="mt-3" md={6} sm={12} style={{ fontSize: '15px' }}>
                        Wound is healing
                      </Col>
                      <Col className="mt-3" md={6} sm={12}>
                        <FormControl>
                          <RadioGroup
                            row
                            value={woundHealing}
                            aria-labelledby="demo-row-radio-buttons-group-label"
                            name="row-radio-buttons-group"
                            onChange={(e) => setWoundHealing(e.target.value)}
                          >
                            <FormControlLabel
                              style={{ fontSize: '15px' }}
                              value="true"
                              control={<Radio />}
                              label="yes"
                            />
                            <FormControlLabel
                              style={{ fontSize: '15px' }}
                              value="false"
                              control={<Radio />}
                              label="no"
                            />
                          </RadioGroup>
                        </FormControl>
                      </Col>
                    </Row>
                  </Col>
                  <Col md={6} sm={12}>
                    <Row>
                      <Col md={4} sm={12} style={{ fontSize: '15px' }}>
                        Onset Date
                      </Col>
                      <Col md={4} sm={12}>
                        <Form.Control
                          type="date"
                          name="date"
                          value={onsetDate}
                          onChange={(e) => setOnsetDate(e.target.value)}
                          style={{
                            width: '100%',
                            textAlign: 'center',
                            height: '25px',
                            fontSize: '12px',
                          }}
                          required
                          // value={dateTime}
                          label="Date"
                          // onChange={(e) => setDateTime(e.target.value)}
                        />
                      </Col>
                      <Col md={4} sm={12}>
                        <Form.Control
                          type="time"
                          name="time"
                          value={onsetTime}
                          onChange={(e) => setOnsetTime(e.target.value)}
                          style={{
                            width: '100%',
                            textAlign: 'center',
                            height: '25px',
                            fontSize: '12px',
                          }}
                          required
                          // value={dateTime}
                          label="Date"
                          // onChange={(e) => setDateTime(e.target.value)}
                        />
                      </Col>
                      <Col md={6} sm={12} style={{ fontSize: '15px' }}>
                        <p className="mt-3">Anatomical Direction: </p>{' '}
                      </Col>

                      <Col md={6} sm={12}>
                        {' '}
                        <Autocomplete
                          className="mt-3"
                          size="small"
                          id="country-select-demo"
                          onChange={(event, data) => {
                            setAnatomicalDirection(data.label)
                          }}
                          options={arr}
                          autoHighlight
                          getOptionLabel={(option) => option.label}
                          renderOption={(props, option) => (
                            <Box
                              component="li"
                              sx={{ '& > img': { mr: 2, flexShrink: 0 } }}
                              {...props}
                            >
                              {option.label}
                            </Box>
                          )}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              variant="standard"
                              label="Choose.."
                              inputProps={{
                                ...params.inputProps,
                              }}
                            />
                          )}
                        />
                      </Col>
                      <Col md={6} sm={12} style={{ fontSize: '15px' }}>
                        <p className="mt-3">Redness: </p>{' '}
                      </Col>

                      <Col md={6} sm={12}>
                        {' '}
                        <Autocomplete
                          className="mt-3"
                          size="small"
                          id="country-select-demo"
                          options={arr}
                          autoHighlight
                          onChange={(event, data) => {
                            setRedness(data.label)
                          }}
                          getOptionLabel={(option) => option.label}
                          renderOption={(props, option) => (
                            <Box
                              component="li"
                              sx={{ '& > img': { mr: 2, flexShrink: 0 } }}
                              {...props}
                            >
                              {option.label}
                            </Box>
                          )}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              variant="standard"
                              label="Choose.."
                              inputProps={{
                                ...params.inputProps,
                              }}
                            />
                          )}
                        />
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
          <Col md="12" className="d-flex justify-content-center mt-3">
            <div className="d-grid gap-2 col-2 mx-auto mt-2">
              <CButton onClick={(e) => addWoundData(e)} id="submit-btn">
                Record
              </CButton>
            </div>
          </Col>
        </Row>
      </Container>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  )
}

export default Wounds
