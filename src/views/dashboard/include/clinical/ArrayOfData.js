export const LegalGuardianArr = [
  'Legal Gurdian',
  'Conservator',
  'Other Legal Oversight',
  'Durable Power of attorney/financial',
  'Family Responsible',
  'Resident Responsible for self',
]

export const CodeStatus = ['Full Code', 'DNR', 'POLST', 'Living Will/Advanced Directive', 'Other']

export const RoomPreference = ['Private', 'Shared', 'w/ Bathroom', 'w/ Shower', 'w/ Lift']

export const diagnosisLookup = [
  {
    label: 'diagnosisSample1',
  },
  {
    label: 'diagnosisSample2',
  },
  {
    label: 'diagnosisSample3',
  },
  {
    label: 'diagnosisSample4',
  },
  {
    label: 'diagnosisSample5',
  },
]

export const allergyLookup = ['Peanut', 'Egg', 'Perfume', 'Pollen', 'Seafoods']

export const diabetesControlLookup = ['Oral', 'Injection', 'Diet', 'Other', 'N/A']

export const communicableDiseasesLookup = [
  'Resident is free from communicable disease(s)',
  'Is not free from communicable disease(s)',
  'Tuberculosis',
  'Unknown at the time of assessment Score',
]

export const oxgenUsedLookup = [
  'Not Applicable: Resident does not require supplemental oxygen',
  'Independent: Resident requires supplemental oxygen and is independent with all aspects of oxygen utilization',
  'Minimal: Resident requires cueing/reminders to use supplemental oxygen as ordered by MD',
  'Moderate: Resident requires some hands on assistance with oxygen such as adjusting nasal cannula, changing tubing, ordering supplies',
  'Extensive: Resident requires assistance from a licensed nurse to adjust liters per minute, and/or monitoring of pulse oximetry, etc.',
]

export const oxygenEnablingDeviceLookup = [
  'Nasal cannula',
  'Non-Rebreather mask',
  'Portable oxygen tank',
  'Oxygen concentrator',
  'Smoking vest/blanket',
  'Pulse oximetry meter',
  'Incentive spirometer',
  'CPAP/BiPAP',
  'Humidifier',
  'Dehumidifier',
  'Air purifier',
  'Other - Resident has enabling device(s) used related to oxygenation (see Service Plan)',
  'None Apply',
]

export const medicationLevelofAssistanceLookup = [
  'Independent: Resident does not require assistance withmedication administration',
  'Minimal: Resident requires minimal assistance (i.e. open containers or use of mediset) understands medication and treatment routine',
  'Moderate: Resident requires occasional assistance or cueing to follow medication routine or timely medication refill',
  'Extensive: Resident requires daily assistance or cueing, must be reminded to take medication and treatments; does not know medication routine',
  'Total: Resident is not able to take medication without assistance',
  'Not Applicable',
]

export const prefferedPharmacyUtilizationLookup = [
  'Resident uses community preferred pharmacy',
  'Resident uses approved non-preferred pharmacy (describe in notes)',
  'Resident uses a non-preferred pharmacy',
  'Medications are not in the community preferred packaging',
  'Over-the-counter medications not provided by community preferred pharmacy',
]

// new
export const behaviorHistory = [
  'Physical Assault',
  'Verbal Assault',
  'Wandering',
  'Sexual Assault/Inapproriate sexual behavior',
  'Disruptiveness',
  'Property Destruction',
  'Careless Disposal of smoking material',
  'Stealing',
  'Refusal to take medication',
  'Refusal to get medical attention',
  'Refusal to bathe or wear clean clothing',
  'Non-Compliance with house rules',
  'Self Abuse',
  'Suicide Attempt',
  'Depression',
  'Alcohol or Drug Abuse',
  'Sun Down Syndrome',
]

export const orientationLookup = [
  'No Impairment: Oriented to person, place, time and situation',
  'Mild Impairment: Resident has current or history of occasional disorientation to person, place, time or situation that does not interfere with functioning in familiar surroundings. Requires some direction and reminding from others',
  'Moderate Impairment: Resident has current or history of occasional disorientation to person, place, time or situation even in familiar surroundings and requires supervision and oversight for safety',
  'Severe Impairment: Resident is frequently disoriented and may require repeated verbal prompts and/or direction',
]

export const shortTermMemoryLookup = [
  'No Impairment: Resident does not have short term memory deficit',
  'Mild Impairment: Resident has current or history of occasional difficulty remembering and using information; requires some directions and reminding from others; may have difficulty following written instructions',
  'Moderate Impairment: Current or history of frequent or difficulty remembering and using information; requires directions and reminding from others; cannot follow written instructions',
  'Severe Impairment: Resident is unable to remember or use information; may require repeated verbal prompts and/or direction',
]

export const longTermMemoryLookup = [
  'No Impairment: Resident does not have long term memory deficit',
  'Mild Impairment: Resident has current or history of occasional difficulty remembering and using information; requires some directions and reminding from others; may have difficulty following written instructions',
  'Moderate Impairment: Resident has current or history of frequent or difficulty remembering and using information; requires directions and reminding from others; cannot follow written instructions',
  'Severe Impairment: Resident is unable to remember or use information; may require repeated verbal prompts and/or direction',
]

export const visionLookup = [
  'No Impairment: Resident does not have visual impairment',
  'Mild Impairment: Resident has mild visual impairment but can see adequately with devices',
  'Moderate Impairment: Resident has moderate visual impairment',
  'Severe Impairment: Resident has severe visual impairment',
  'Legally Blind: Resident is legally blind',
]

export const communicationLookup = [
  'No Impairment: Resident is able to communicate effectively and make needs known, with or without assistive device(s)',
  'Mild Impairment: Resident has current or history of occasional difficulty communicating and receiving information; may have occasional difficulty following instructions with using the telephone, writing letters and other communication devices',
  'Moderate Impairment: Resident has current or history of frequent difficulty communicating and receiving information; has difficulty following instructions; has frequent difficulty following instructions with using the telephone, writing letters and other communication devices',
  'Severe Impairment: Resident is unable to communicate or receive information; is unable to follow instructions with using the telephone, writing letters and other communication devices',
  'Does Not Perform: Resident does not use the telephone,write letters or other communication devices',
]

export const communuicableDeviceAndMethod = [
  'Resident uses a writing board',
  'Resident uses an electronic device such as a tablet or computer',
  'Resident uses gestures',
  'Other - Resident has enabling device(s) used for communication device (see Service Plan)',
  'None Apply',
]

export const MobilityAmbulationLevelofAssistanceLookup = [
  'Independent: Resident does not require assistance with mobility/ambulation',
  'Minimal: Resident may require prompts/cues to use assistive device, does not require hands on assistance',
  'Moderate: Resident may require hands on assistance by staff member(s)',
  'Extensive: Resident requires hands on assistance by staff member(s)',
  'Total: Resident is dependent on staff member(s) for all mobility/ambulation needs or requires hands on assistance on routine basis',
]

export const MobilityAmbulationEnablingDeviceAndMethodLookup = [
  'Brace/orthotic appliance',
  'Cane (regular, quad)',
  'Crutch',
  'Prosthesis',
  'Scooter',
  'Walker (no wheels, wheeled, seat attached, merri walker)',
  'Wheelchair (electric, manual)',
  'Other - Resident has other enabling device(s) used for mobility/ambulation (see Service Plan)',
  'None Apply',
]

export const ParalysisLookup = [
  'Left Side',
  'Right Side',
  'Lower Body',
  'Upper Body',
  'General',
  'None Apply',
]

export const TransferringLevelofAssistanceLookup = [
  'Independent: Resident does not require assistance with transferring',
  'Minimal: Resident requires assistance with transfer and or positioning with verbal prompts/cues, no hands on assistance needed.',
  'Moderate: Resident requires occasional hands on assistance with transfers and or changes in position.',
  'Extensive: Resident requires frequent hands on assistance with transfers and or changes in position.',
  'Total: Resident requires routine hands on assistance with transfers and or changes in position.',
  'Not Applicable',
]

export const TransferringEnablingDeviceandMethod = [
  'Grab bar',
  'Mechanical lift',
  'Slide board',
  'Transfer belt',
  'Trapeze',
  'Other - Resident has enabling device(s) used for transfers (see Service Plan)',
  'None Apply',
]

export const ResidentIsAbleToLookup = ['Walk', 'Wheel', 'Climb Stairs', 'None Apply']

export const FallswithinTheLast90DaysLookup = [
  'No falls',
  '1-2 falls in the last 90 days',
  '3 or more falls in the last 90 days',
]

export const EliminationContinence = [
  'Ambulatory/Continent',
  'Ambulatory/Incontinent',
  'Chair-bound, requires assistance with ambulation',
]

export const BathingLevelofAssistanceLookup = [
  'Independent: Resident does not require assistance with bathing',
  'Minimal: Resident can bathe without physical assistance but may require reminding or standby assistance',
  'Moderate: Resident requires assistance with bathing, requires assistance or cueing with parts of bathing including assistance getting in/out of tub/shower',
  'Extensive: Resident requires hands on assistance with participation by the resident to complete task',
  'Total: Resident is dependent on others to provide complete bath, including shampoo',
]

export const BathingFrequencyLookup = [
  '1-2 times weekly',
  '3-4 times weekly',
  '5-6 times weekly',
  'Daily',
  'None Apply',
]

export const BathingPreferencesLookup = [
  'Prefers baths',
  'Prefers showers',
  'Other (see Service Plan)',
  'None Apply',
]

export const BathingEnablingDeviceandMethodLookup = [
  'Shower Chair',
  'Bench',
  'Grab bar',
  'Hand held shower',
  'Bath lift',
  'Other (see Service Plan)',
  'None Apply',
]

export const GroomingPersonalHygieneLevelofAssistanceLookup = [
  'Independent: Resident does not require assistance with grooming/personal hygiene',
  'Minimal: Resident may manage grooming/personal hygiene but requires verbal reminders/prompts/cues',
  'Moderate: Resident performs grooming/personal hygiene but requires physical assistance to complete task',
  'Extensive: Caregiver performs most grooming/personal hygiene but resident is able to assist',
  'Total: Resident is dependent on others to provide all grooming/personal hygiene needs',
]

export const GrowingPersonalHygieneEnablingDevicesandMethodLookup = [
  'Glasses',
  'Contact lenses',
  'Artificial eye',
  'Hearing aid(s)',
  'Full dentures',
  'Partial dentures',
  'Upper dentures',
  'Lower dentures',
  'Bridge',
  'Other - Resident has enabling device(s) used for grooming/personal hygiene (see Service Plan)',
  'None Apply',
]

export const DressingLevelofAssistanceLookup = [
  'Independent: Resident does not require assistance with dressing',
  'Minimal: Resident can dress/undress and select clothing but may need to be reminded/supervised',
  'Moderate: Resident can dress/undress and select clothing with physical assistance',
  'Extensive: Resident requires assistance with dressing, caregiver dresses/undresses and selects clothing but resident is able to assist in task',
  'Total: Resident is dependent upon others to do all dressing/undressing',
]

export const ToiletingLevelofAssistanceLookup = [
  'Independent: Resident does not require assistance with toileting',
  'Minimal: Resident requires verbal prompts/cues for toileting tasks.',
  'Moderate: Resident requires stand by assistance for toileting tasks.',
  'Extensive: Resident requires physical assistance with parts of toileting tasks.',
  'Total: Resident requires physical assistance with all tasks related to toileting. May require assistance with closed drainage system/catheters.',
]

export const ToiletingEnablingDevicesandMethods = [
  'Adult Briefs',
  'Adult Pull-up/protective underwear',
  'Bedpan',
  'Bedside commode',
  'Catheter-external/condom',
  'Catheter-indwelling',
  'Catheter Care: in/out catheter',
  'Colostomy',
  'Grab bar',
  'Ileostomy',
  'Mattress protector',
  'Mechanical lift',
  'Raised seat',
  'Slide board',
  'Transfer belt',
  'Ureterostomy',
  'Urinal',
  'Urinary pad',
  'Other - Resident has enabling device(s) used for toileting (see Service Plan)',
  'None Apply',
]

export const MealConsumptionofAssistanceLookup = [
  'Independent: Resident does not require assistance with meal consumption',
  'Minimal: Resident can feed self, chew and swallow foods, however needs reminding/cueing to maintain adequate intake',
  'Moderate: Resident requires cutting up of food, opening cartons/packages; may need encouragement to select menu items',
  'Extensive: Resident requires regular encouragement to select menu items compliant with ordered diet; may require assistance with feeding appliances',
  'Total: Resident must be fed by mouth by another person or gastrostomy',
]

export const DietNeedsLookup = [
  'Regular',
  'Puree',
  'Puree',
  'Soft Mechanical',
  'Thickened Liquids',
  'Low Sodium (Cardiac)',
  'Low Potassium (Renal)',
  'Kosher Style',
]

export const AdditionalMealConsiderationLookup = [
  'Problems with chewing',
  'Problems with swallowing',
  'Loose teeth',
  'Broken teeth',
  'Decaying teeth',
  'Recent unintended weight loss',
  'Recent unintended weight gain',
  'None Apply',
]

export const AdditionalNursingServiceOutsideServiceLookup = [
  'Physical Therapy',
  'Occupational Therapy',
  'Speech/Swallow Therapy (Trigger)',
  'Social Worker/Case Manager',
  'Hospice/Palliative Care',
  'Registered Nurse (RN) or Licensed Practical/Vocational Nurse (LP/VN)',
  'Home Health Aide',
  'Private Duty Caregiver (PDA) (Trigger)',
  'Other (specify in Service Plan)',
  'None Apply',
]

export const ResidentConditionLookup = [
  'Contractures',
  'Dialysis',
  'Injections',
  'Wound/Bed Sore',
  'Bedridden',
  'G-Tube Feeding',
  'Naso-Gastric Tube',
  'Infections',
  'Paralysis',
  'Communicable Diseases',
  'Tuberculosis',
]
