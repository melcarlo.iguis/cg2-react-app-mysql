import * as React from 'react'
import Box from '@mui/material/Box'
import Tab from '@mui/material/Tab'
import TabContext from '@mui/lab/TabContext'
import TabList from '@mui/lab/TabList'
import TabPanel from '@mui/lab/TabPanel'
import AssessmentNewForm from './AssessmentNewForm'
import AssessmentTable from './AssessmentTable'
import AssessmentTableCompleted from './AssessmentTableCompleted'

function Assessments() {
  const [value, setValue] = React.useState('1')

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }
  return (
    <div>
      {/* <div>
        <AssessmentNewForm />
      </div> */}
      <Box sx={{ width: '100%', typography: 'body1' }}>
        <TabContext value={value}>
          <Box>
            <TabList onChange={handleChange}>
              <Tab label="Scheduled" value="1" />
              <Tab label="Completed" value="2" className="mx-2" />
            </TabList>
          </Box>
          <TabPanel value="1">
            <AssessmentTable />
          </TabPanel>
          <TabPanel value="2">
            <AssessmentTableCompleted />
          </TabPanel>
        </TabContext>
      </Box>
    </div>
  )
}

export default Assessments
