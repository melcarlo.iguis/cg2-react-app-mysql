import React, { useState, useEffect, useContext } from 'react'
import { Row, Col } from 'react-bootstrap'
import Popup from '../../popup/Popup'
import AddObservationForm from '../../form/AddObservationForm'
import EditObservationForm from '../../form/EditObservationForm'
import EditPopup from '../../popup/EditPopup'
// MUI
import InputBase from '@material-ui/core/InputBase'
import { Grid, Box, CardActionArea } from '@mui/material'
// icons
import SearchIcon from '@mui/icons-material/Search'
import { DataGrid } from '@mui/x-data-grid'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'
// axios api
import api from '../../../../api/api'
import Swal from 'sweetalert2'
// global variable
import AppContext from '../../../../AppContext'

function Dailylog() {
  // state for search word
  const [wordEntered, setWordEntered] = useState('')
  const [isFetchDone, setIsFetchDone] = useState(false)

  const { dailylogData, setDailylogData, incidentReportList, setIncidentReportList } =
    useContext(AppContext)

  const fetchDailylogData = () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    api
      .get(`/dailylog/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        setDailylogData(res.data)
        setIsFetchDone(true)
      })
      .catch((err) => {
        console.error(err)
      })
  }

  useEffect(() => {
    fetchDailylogData()
  }, [])

  const cutDate = (string) => {
    let date = new Date(string)
    // const month = date.toLocaleString('en-us', { month: 'long' }); /* June */

    const month = date.getUTCMonth() + 1
    const day = date.getUTCDate()
    const year = date.getUTCFullYear()

    const hour = date.getHours() % 12 || 12
    const min = date.getMinutes()

    const newMins = min < 10 ? '0' + min : min
    var ampm = date.getHours() >= 12 ? 'PM' : 'AM'
    const newDay = day < 10 ? '0' + day : day

    let fullDate =
      month + '/' + parseInt(newDay + 1) + '/' + year + ' - ' + hour + ':' + newMins + ' ' + ampm

    return fullDate
  }

  const columns = [
    {
      field: 'date',
      headerName: 'Date and Time',
      flex: 0.5,
      width: 120,
      valueGetter: (params) => {
        return cutDate(params.row.date)
      },
    },
    {
      field: 'title',
      headerName: 'Title',
      flex: 2,
      width: 180,
    },

    {
      field: 'userName',
      headerName: 'Name of the Staff',
      flex: 0.5,
      minWidth: 200,
    },
    {
      field: 'action',
      headerName: 'Action',
      sortable: false,
      flex: 0.5,
      minWidth: 80,
      renderCell: (params) => (
        <div className="d-flex">
          <EditPopup>
            <EditObservationForm data={params.row} />
          </EditPopup>
          <div
            id="delete-btn"
            className="btn d-flex"
            onClick={(e) => deleteObservationData(e, params.row.id)}
          >
            <DeleteForeverIcon />
          </div>
        </div>
      ),
    },
  ]

  const deleteObservationData = (e, id) => {
    e.preventDefault()
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        api
          .delete(`/observation/${tenantId}/delete/${id}`, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((result) => {
            console.log(result)
            const newArr = [...dailylogData]

            newArr.forEach((item, index) => {
              if (item._id === id) {
                newArr.splice(index, 1)
              }
            })

            setDailylogData(newArr)
            Swal.fire('Deleted!', 'The Data has been deleted.', 'success')

            // adding history to database
            const input2 = {
              title: `Deleted ${tenantName}'s observation data`,
              tenantName: tenantName,
              tenantId: tenantId,
              userName: name,
            }
            api
              .post(`/history/create/`, input2, {
                headers: {
                  Authorization: `Bearer ${token}`,
                },
              })
              .then((result) => {})
              .catch((err) => {
                console.error(err)
              })
          })
          .catch((err) => {
            Swal.fire({
              title: 'fail to delete',
              icon: 'error',
            })
          })
      }
    })
  }

  return (
    <div id="container" className="mt-3">
      <div className="title-holder d-flex">
        <h1 className="mx-auto mt-2">Dailylog</h1>
      </div>
      <Row>
        <Grid container rowSpacing={4} columnSpacing={{ xs: 1, sm: 1, md: 1 }}>
          <Grid
            item
            md={4}
            sm={10}
            direction="column"
            className="ml-5 pb-3"
            style={{
              marginLeft: '25px',
            }}
          >
            {/* <Popup className="pb-5">
              <AddObservationForm />
            </Popup> */}
          </Grid>

          {/*<InputBase
            placeholder="Search…"
            value={wordEntered}
            onChange={handleFilter}
            inputProps={{ 'aria-label': 'search' }}
            endAdornment={<SearchIcon style={{ fontSize: 50 }} className="pr-3" />}
            id="searchBar2"
          />*/}
        </Grid>
        <Col md="12" className="mx-auto"></Col>
        <div id="datagrid-container" className="mx-auto">
          <Col id="datagrid2" md="12" className="mx-auto mb-3 mt-3">
            {dailylogData.length > 0 && isFetchDone == true ? (
              <DataGrid
                getRowId={(row) => row.id}
                rows={dailylogData}
                columns={columns}
                pageSize={5}
                rowsPerPageOptions={[5]}
              />
            ) : (
              <p>No Data Available</p>
            )}
          </Col>
        </div>
        <Col md="12" className="mx-auto"></Col>
      </Row>
    </div>
  )
}

export default Dailylog
