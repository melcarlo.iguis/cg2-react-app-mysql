import React, { useState, useEffect, forwardRef, useContext } from 'react'
import Button from '@mui/material/Button'
import { Row, Col, Table } from 'react-bootstrap'
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import ArrowDownward from '@material-ui/icons/ArrowDownward'
import Check from '@material-ui/icons/Check'
import ChevronLeft from '@material-ui/icons/ChevronLeft'
import ChevronRight from '@material-ui/icons/ChevronRight'
import Clear from '@material-ui/icons/Clear'
import DeleteOutline from '@material-ui/icons/DeleteOutline'
import Edit from '@material-ui/icons/Edit'
import FilterList from '@material-ui/icons/FilterList'
import FirstPage from '@material-ui/icons/FirstPage'
import LastPage from '@material-ui/icons/LastPage'
import Remove from '@material-ui/icons/Remove'
import CancelIcon from '@mui/icons-material/Cancel'
import SaveAlt from '@material-ui/icons/SaveAlt'
import Search from '@material-ui/icons/Search'
import ViewColumn from '@material-ui/icons/ViewColumn'
import api from '../../../../api/api'
import AppContext from '../../../../AppContext'
import CheckCircleIcon from '@mui/icons-material/CheckCircle'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function AssessmentTable() {
  const [doneFetching, setDoneFetching] = useState(false)
  const {
    allergiesList,
    setAllergiesList,
    currentTab,
    setCurrentTab,
    assessmentData,
    setAssessmentData,
    popupChildren,
    setPopupChildren,
    setPopupTitle,
  } = useContext(AppContext)

  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => (
      <div>
        <CancelIcon />
        <p style={{ fontSize: '10px' }}>Cancel</p>
      </div>
    )),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => (
      <div>
        <Edit {...props} ref={ref} />
        <p style={{ fontSize: '10px' }}>Change</p>
      </div>
    )),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  }

  const handleClick = (e, data) => {
    e.preventDefault()
    setPopupChildren('performAssesssment')
    setPopupTitle(assessmentTypeLookup[data.type])
    localStorage.setItem('AssessmentCode', data.AssessmentCode)
    console.log(data)
  }

  // const assessmentTypeLookup = [
  //   'Pre-admission',
  //   'General',
  //   'Vaccination',
  //   'Appointment Record',
  //   'Mental Status',
  // ]

  const assessmentTypeLookup = {
    1: 'Pre-admission',
    2: 'General',
    3: 'Vaccination',
    4: 'Appointment Record',
    5: 'Mental Status',
  }

  const columns = [
    { title: 'Type', field: 'type', lookup: assessmentTypeLookup, width: '250px' },
    // {
    //   title: 'Allergy Type',
    //   field: 'allergyType',
    //   // lookup: allergyTypeSelection,
    // },
    // { title: 'Allergen Type', field: 'allergenType' },
    {
      title: 'Review Date',
      field: 'reviewDate',
      type: 'date',
      dateSetting: { locale: 'ko-KR' },
      render: (rowData) => formatDate(rowData.reviewDate),
    },
    // {
    //   title: 'End Date',
    //   field: 'endDate',
    //   type: 'date',
    //   dateSetting: { locale: 'ko-KR' },
    //   render: (rowData) => formatDate(rowData.endDate),
    // },
    // { title: 'Reaction', field: 'reaction' },
    {
      title: 'Perform',
      editable: false,
      render: (rowData) =>
        rowData && (
          <div className="btn my-0 py-0" onClick={(e) => handleClick(e, rowData)}>
            <CheckCircleIcon className="align-center" />
            <p style={{ fontSize: '10px' }}>Perform</p>
          </div>
        ),
    },
  ]

  // code for fetching allergy
  useEffect(() => {
    fetchAssessmentDataSched()
  }, [])

  const fetchAssessmentDataSched = () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')
    api
      .get(`/assessments/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        const filteredAssessment = res.data.filter((val) => val.status == false)
        setAssessmentData(filteredAssessment)
        setDoneFetching(true)
      })
  }

  const formatDate = (string) => {
    if (string === undefined || string === '') {
      return ''
    } else {
      let date = new Date(string)
      const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
      const day = date.getUTCDate() + 1
      const year = date.getUTCFullYear()

      let fullDate = month + ' ' + day + ', ' + year
      return fullDate
    }
  }

  const addAssessmentData = (data) => {
    let tenantId = localStorage.getItem('tenantId')
    let token = localStorage.getItem('token')

    let input = {
      type: data.type,
      reviewDate: data.reviewDate,
      ResidentId: tenantId,
    }
    api
      .post(`/assessments/add`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        setAssessmentData([...assessmentData, res.data])
        toast.success('Added Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
      })
  }

  const updateAssessmentData = (data, id) => {
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')
    console.log(id)
    // api call for editing allergy data
    api
      .put(`/assessments/${id.AssessmentCode}/update`, data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        // updating the tenant's allergy data state
        assessmentData.forEach((item) => {
          if (item.AssessmentCode === id.AssessmentCode) {
            item.type = data.type
            item.reviewDate = data.reviewDate
          }
        })

        setAssessmentData([...assessmentData])

        toast.success('Updated Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
      })
      .catch((err) => {
        console.log(err)
      })
  }

  const deleteAssessment = (id) => {
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    console.log(id)

    api.put(`/assessments/${id}/delete`).then((result) => {
      console.log(result)

      const newArr = [...assessmentData]
      newArr.forEach((item, index) => {
        if (item.AssessmentCode === id) {
          newArr.splice(index, 1)
        }
      })

      setAssessmentData(newArr)

      toast.success('Deleted Successfully', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
      })
    })
  }

  return (
    <div>
      <MaterialTable
        icons={tableIcons}
        title="Scheduled Assessments"
        style={{ zIndex: 9999999 }}
        columns={columns}
        data={assessmentData}
        editable={{
          onRowAdd: (newRow) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                setAssessmentData([...assessmentData, newRow])
                addAssessmentData(newRow)
                resolve()
              }, 2000)
            }),
          onRowDelete: (selectedRow) =>
            new Promise((resolve, reject) => {
              console.log(selectedRow)
              setTimeout(() => {
                deleteAssessment(selectedRow.AssessmentCode)
                resolve()
              }, 2000)
            }),
          onRowUpdate: (updatedRow, oldRow) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                updateAssessmentData(updatedRow, oldRow)
                resolve()
              }, 2000)
            }),
        }}
        options={{
          search: false,
          actionsColumnIndex: -1,
          addRowPosition: 'first',
          headerStyle: {
            zIndex: 9999999,
          },
        }}
      />
    </div>
  )
}

export default AssessmentTable
