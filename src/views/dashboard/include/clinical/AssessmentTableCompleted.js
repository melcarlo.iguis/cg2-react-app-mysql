import React, { useState, useEffect, forwardRef, useContext } from 'react'
import Button from '@mui/material/Button'
import { Row, Col, Table } from 'react-bootstrap'
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import ArrowDownward from '@material-ui/icons/ArrowDownward'
import Check from '@material-ui/icons/Check'
import ChevronLeft from '@material-ui/icons/ChevronLeft'
import ChevronRight from '@material-ui/icons/ChevronRight'
import Clear from '@material-ui/icons/Clear'
import DeleteOutline from '@material-ui/icons/DeleteOutline'
import Edit from '@material-ui/icons/Edit'
import FilterList from '@material-ui/icons/FilterList'
import FirstPage from '@material-ui/icons/FirstPage'
import LastPage from '@material-ui/icons/LastPage'
import Remove from '@material-ui/icons/Remove'
import CancelIcon from '@mui/icons-material/Cancel'
import SaveAlt from '@material-ui/icons/SaveAlt'
import Search from '@material-ui/icons/Search'
import ViewColumn from '@material-ui/icons/ViewColumn'
import api from '../../../../api/api'
import AppContext from '../../../../AppContext'
import CheckCircleIcon from '@mui/icons-material/CheckCircle'
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye'

import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function AssessmentTableCompleted() {
  const [doneFetching, setDoneFetching] = useState(false)
  const {
    allergiesList,
    setAllergiesList,
    currentTab,
    setCurrentTab,
    assessmentData,
    setAssessmentData,
    popupChildren,
    setPopupChildren,
  } = useContext(AppContext)

  const [assessmentDataCompleted, setAssessmentDataCompleted] = useState([])

  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => (
      <div>
        <CancelIcon />
        <p style={{ fontSize: '10px' }}>Cancel</p>
      </div>
    )),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => (
      <div>
        <Edit {...props} ref={ref} />
        <p style={{ fontSize: '10px' }}>Change</p>
      </div>
    )),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  }

  const handleClick = (e, data) => {
    e.preventDefault()
    setPopupChildren('performAssesssment')
    localStorage.setItem('AssessmentCode', data.AssessmentCode)
    console.log(data)
  }

  const padLeadingZeros = (num) => {
    var s = num + ''
    while (s.length < 8) s = '0' + s
    return s
  }

  const columns = [
    {
      title: 'Assessment Code',
      field: 'AssessmentCode',
      render: (rowData) => padLeadingZeros(rowData.AssessmentCode),
    },
    { title: 'Type', field: 'type' },
    // {
    //   title: 'Allergy Type',
    //   field: 'allergyType',
    //   // lookup: allergyTypeSelection,
    // },
    // { title: 'Allergen Type', field: 'allergenType' },
    {
      title: 'Completed Date',
      field: 'dateCompleted',
      type: 'date',
      dateSetting: { locale: 'ko-KR' },
      render: (rowData) => formatDate(rowData.dateCompleted),
    },
    // {
    //   title: 'End Date',
    //   field: 'endDate',
    //   type: 'date',
    //   dateSetting: { locale: 'ko-KR' },
    //   render: (rowData) => formatDate(rowData.endDate),
    // },
    // { title: 'Reaction', field: 'reaction' },
    {
      title: 'View',
      editable: false,
      render: (rowData) =>
        rowData && (
          <div className="btn my-0 py-0" onClick={(e) => handleClick(e, rowData)}>
            <RemoveRedEyeIcon className="align-center" />
            <p style={{ fontSize: '10px' }}>View</p>
          </div>
        ),
    },
  ]

  // code for fetching allergy
  useEffect(() => {
    fetchAssessmentDataSched()
  }, [])

  const fetchAssessmentDataSched = () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')
    api
      .get(`/assessments/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        const filteredAssessment = res.data.filter((val) => val.status == true)
        setAssessmentDataCompleted(filteredAssessment)
        setDoneFetching(true)
      })
  }

  const formatDate = (string) => {
    if (string === undefined || string === '') {
      return ''
    } else {
      let date = new Date(string)
      const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
      const day = date.getUTCDate()
      const year = date.getUTCFullYear()

      let fullDate = month + ' ' + day + ', ' + year
      return fullDate
    }
  }

  return (
    <div>
      <MaterialTable
        icons={tableIcons}
        title="Scheduled Assessments"
        style={{ zIndex: 9999999 }}
        columns={columns}
        data={assessmentDataCompleted}
        options={{
          search: false,
          actionsColumnIndex: -1,
          addRowPosition: 'first',
          headerStyle: {
            zIndex: 9999999,
          },
        }}
      />
    </div>
  )
}

export default AssessmentTableCompleted
