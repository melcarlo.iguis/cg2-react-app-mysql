import React, { useContext, useState, useEffect } from 'react'
import { Row, Col, Form } from 'react-bootstrap'
import Button from '@mui/material/Button'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

import api from '../../../../api/api'

function AssessmentNewForm() {
  const [type, setType] = useState('')
  const [reviewDate, setReviewDate] = useState('')

  const resetInputState = () => {
    setType('')
    setReviewDate('')
  }

  const addNewAssessment = (e) => {
    e.preventDefault()
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    const input = {
      reviewDate: reviewDate,
      type: type,
      ResidentId: tenantId,
    }

    api
      .post('/assessments/add', input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        setAssessmentData([...assessmentData, result.data])
        resetInputState()
        toast.success('Schedule added successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
      })
  }

  return (
    <div>
      <Form id="form" onSubmit={(e) => addNewAssessment(e)}>
        <Row>
          <Col md="4" sm="8" className="mx-auto colItem">
            <Form.Control
              type="text"
              required
              value={type}
              onChange={(e) => setType(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label htmlFor="date">Type</label>
          </Col>
          <Col md="4" sm="8" className="mx-auto  colItem">
            <Form.Control
              type="date"
              required
              value={reviewDate}
              onChange={(e) => setReviewDate(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label htmlFor="time">Review Date</label>
          </Col>
          <Col md="4" className="d-flex justify-content-center mt-3">
            <Button type="submit" variant="contained" style={{ height: '35px' }}>
              Add new Schedule
            </Button>
          </Col>
          <Col md="12" className="d-flex justify-content-center mt-3">
            <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
            />
          </Col>
        </Row>
      </Form>
    </div>
  )
}

export default AssessmentNewForm
