import React, { useEffect, useState, useContext } from 'react'
import TextField from '@mui/material/TextField'
import Autocomplete from '@mui/material/Autocomplete'
import PageviewIcon from '@mui/icons-material/Pageview'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardGroup,
  CCardHeader,
  CCardImage,
  CCardLink,
  CCardSubtitle,
  CCardText,
  CCardTitle,
  CListGroup,
  CListGroupItem,
  CNav,
  CNavItem,
  CNavLink,
  CCol,
  CRow,
  CAccordion,
  CAccordionBody,
  CAccordionHeader,
  CAccordionItem,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import {
  cilBell,
  cilSpeedometer,
  cilEnvelopeOpen,
  cilList,
  cilMenu,
  cilSearch,
  cilHome,
  cilHeart,
  cilNotes,
  cilUser,
  cilPeople,
  cilWarning,
  cilPlus,
} from '@coreui/icons'
import { DocsCallout, DocsExample } from 'src/components'
import { useNavigate } from 'react-router-dom'
import ReactImg from 'src/assets/images/react.jpg'
// importing axios
import api from '../../api/api'
// MUI
import InputBase from '@material-ui/core/InputBase'
import SearchIcon from '@mui/icons-material/Search'

// global variable
import AppContext from '../../AppContext'

import { styled } from '@mui/material/styles'
import Card from '@mui/material/Card'
import CardHeader from '@mui/material/CardHeader'
import CardMedia from '@mui/material/CardMedia'
import CardContent from '@mui/material/CardContent'
import CardActions from '@mui/material/CardActions'
import Collapse from '@mui/material/Collapse'
import Avatar from '@mui/material/Avatar'
import IconButton from '@mui/material/IconButton'
import Typography from '@mui/material/Typography'
import { red } from '@mui/material/colors'
import FavoriteIcon from '@mui/icons-material/Favorite'
import FolderSharedIcon from '@mui/icons-material/FolderShared'
import ShareIcon from '@mui/icons-material/Share'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import MoreVertIcon from '@mui/icons-material/MoreVert'
import AddBoxIcon from '@mui/icons-material/AddBox'
import PendingActionsIcon from '@mui/icons-material/PendingActions'
import PersonSearchIcon from '@mui/icons-material/PersonSearch'
import PersonIcon from '@mui/icons-material/Person'
import NoteAltIcon from '@mui/icons-material/NoteAlt'
import InsertChartIcon from '@mui/icons-material/InsertChart'
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft'
import DriveFileMoveIcon from '@mui/icons-material/DriveFileMove'
import ThumbUpAltIcon from '@mui/icons-material/ThumbUpAlt'
import LocalPharmacyIcon from '@mui/icons-material/LocalPharmacy'
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn'
import InventoryIcon from '@mui/icons-material/Inventory'
import VolunteerActivismIcon from '@mui/icons-material/VolunteerActivism'
import AssessmentIcon from '@mui/icons-material/Assessment'
import ContentPasteSearchIcon from '@mui/icons-material/ContentPasteSearch'
import FindInPageIcon from '@mui/icons-material/FindInPage'
import MedicationIcon from '@mui/icons-material/Medication'
import KeyboardDoubleArrowLeftIcon from '@mui/icons-material/KeyboardDoubleArrowLeft'
import KeyboardDoubleArrowRightIcon from '@mui/icons-material/KeyboardDoubleArrowRight'
import MenuBookIcon from '@mui/icons-material/MenuBook'
import WarningIcon from '@mui/icons-material/Warning'
import FeedIcon from '@mui/icons-material/Feed'
import VaccinesIcon from '@mui/icons-material/Vaccines'

const ExpandMore = styled((props) => {
  const { expand, ...other } = props
  return <IconButton {...other} />
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}))

function Search() {
  const [expanded, setExpanded] = React.useState(false)
  // state for search word
  const [wordEntered, setWordEntered] = useState('')

  const handleExpandClick = () => {
    setExpanded(!expanded)
  }
  let navigate = useNavigate()
  const [roomList, setRoomList] = useState([])
  const { tenantList, setTenantList, actionValue, setActionValue } = useContext(AppContext)
  useEffect(() => {
    fetchTenant()
  }, [])

  const fetchTenant = async () => {
    let token = localStorage.getItem('token')
    await api
      .get('/tenants/fetch', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        setTenantList(res.data)
      })
  }

  const getFullName = (data) => {
    return data.lastName + ',' + ' ' + data.firstName + ' ' + data.middleName
  }

  const cutBirthday = (string) => {
    let date = new Date(string)
    const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
    const day = date.getUTCDate()
    const year = date.getUTCFullYear()

    let fullDate = month + ' ' + day + ', ' + year
    return fullDate
  }

  const viewTenantDetailsWithAction = (e, data, actionValue) => {
    const tenantFullName = data.firstName + ' ' + data.middleName + ' ' + data.lastName
    e.preventDefault()
    localStorage.setItem('tenantId', data._id)
    localStorage.setItem('tenantName', tenantFullName)
    localStorage.setItem('actionValue', actionValue)
    navigate(`/resident/${data._id}`)
  }

  const viewTenantDetails = (e, data) => {
    const tenantFullName = data.firstName + ' ' + data.middleName + ' ' + data.lastName
    e.preventDefault()
    localStorage.setItem('tenantId', data._id)
    localStorage.setItem('tenantName', tenantFullName)
    navigate(`/resident/${data._id}`)
  }

  const fetchRoomList = async () => {
    let token = localStorage.getItem('token')
    await api
      .get('/room/fetch', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setRoomList(res.data)
      })
  }

  useEffect(() => {
    fetchRoomList()
  }, [])

  // function for search input
  const handleSearchFilter = async (event) => {
    const searchWord = event.target.value
    setWordEntered(searchWord)

    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    await api
      .get('/tenants/fetch', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setTenantList(res.data)

        const newFilter = res.data.filter((value) => {
          let fullName = value.firstName + ' ' + value.middleName + ' ' + value.lastName

          return fullName.toLowerCase().includes(searchWord.toLowerCase())
        })
        setTenantList(newFilter)
      })
  }

  const handleFilter = async (e, room) => {
    console.log(room)
    if (room === null) {
      fetchTenant()
    }
    const roomFilter = room.roomName
    let token = localStorage.getItem('token')
    await api
      .get('/tenants/fetch', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setTenantList(res.data)

        console.log(tenantList)

        const newFilter = res.data.filter((value) => {
          return value.roomName === roomFilter
        })

        console.log(newFilter)
        setTenantList(newFilter)
      })
  }
  return (
    <>
      <CRow>
        <CCol xs={12}>
          <CCard className="mb-4">
            <CCardHeader id="card-header">
              <strong id="res-title">Residents</strong>
              <div id="search-container">
                <InputBase
                  placeholder="Search…"
                  value={wordEntered}
                  onChange={handleSearchFilter}
                  inputProps={{ 'aria-label': 'search' }}
                  endAdornment={<SearchIcon style={{ fontSize: 25 }} className="pr-3" />}
                  id="searchBar"
                />
              </div>
            </CCardHeader>
            <CCardBody>
              <CRow className="d-flex justify-content-around" xs={{ gutterY: 3 }}>
                {tenantList.map((val, key) => {
                  return (
                    <CCard id="card-holder" className="p-0" key={key} style={{ width: '18rem' }}>
                      <div id="card-bgc">
                        <div id="tenant-image-holder">
                          <img id="tenant-image" src={val.picture} />
                        </div>
                      </div>
                      <CCardBody className="mt-5">
                        <CCardTitle>{getFullName(val)}</CCardTitle>
                        <small>{cutBirthday(val.birthday)}</small>
                        <CAccordion className="my-3" id="accordion">
                          <CAccordionItem itemKey={1}>
                            <CAccordionHeader>Shortcuts</CAccordionHeader>
                            <CAccordionBody className="p-0">
                              <div className="mb-3" id="btn-holder">
                                <CButton
                                  onClick={(e) => viewTenantDetailsWithAction(e, val, 0)}
                                  id="tl-btn"
                                >
                                  <PersonIcon />
                                  Profile
                                </CButton>{' '}
                                <CButton
                                  onClick={(e) => viewTenantDetailsWithAction(e, val, 1)}
                                  id="tl-btn"
                                >
                                  <FolderSharedIcon /> Allergies
                                </CButton>{' '}
                                <CButton
                                  onClick={(e) => viewTenantDetailsWithAction(e, val, 2)}
                                  id="tl-btn"
                                >
                                  <PersonSearchIcon /> Diagnosis
                                </CButton>{' '}
                                <CButton
                                  onClick={(e) => viewTenantDetailsWithAction(e, val, 5)}
                                  id="tl-btn"
                                >
                                  <PendingActionsIcon /> Behavior
                                </CButton>{' '}
                                <CButton
                                  onClick={(e) => viewTenantDetailsWithAction(e, val, 6)}
                                  id="tl-btn"
                                >
                                  <VolunteerActivismIcon /> Care Stream
                                </CButton>{' '}
                                <CButton
                                  onClick={(e) => viewTenantDetailsWithAction(e, val, 8)}
                                  id="tl-btn"
                                >
                                  <AddBoxIcon /> eMAR
                                </CButton>{' '}
                                <CButton
                                  onClick={(e) => viewTenantDetailsWithAction(e, val, 10)}
                                  id="tl-btn"
                                >
                                  <WarningIcon /> Incident
                                </CButton>{' '}
                                <CButton
                                  onClick={(e) => viewTenantDetailsWithAction(e, val, 11)}
                                  id="tl-btn"
                                >
                                  <InventoryIcon /> Orders
                                </CButton>{' '}
                                <CButton
                                  onClick={(e) => viewTenantDetailsWithAction(e, val, 12)}
                                  id="tl-btn"
                                >
                                  <FeedIcon /> Progress Notes
                                </CButton>{' '}
                                <CButton
                                  onClick={(e) => viewTenantDetailsWithAction(e, val, 13)}
                                  id="tl-btn"
                                >
                                  <VaccinesIcon /> Vitals
                                </CButton>{' '}
                                <CButton
                                  onClick={(e) => viewTenantDetailsWithAction(e, val, 14)}
                                  id="tl-btn"
                                >
                                  <MedicationIcon /> Wounds
                                </CButton>{' '}
                              </div>
                            </CAccordionBody>
                          </CAccordionItem>
                        </CAccordion>

                        <CButton onClick={(e) => viewTenantDetails(e, val)}>View Details</CButton>
                      </CCardBody>
                    </CCard>
                  )
                })}
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  )
}

export default Search
