import React, { useContext, useState, useEffect } from 'react'
import { Row, Col, Form, Button } from 'react-bootstrap'
import AddIcon from '@mui/icons-material/Add'
import Swal from 'sweetalert2'
import { CButton } from '@coreui/react'
// global variable
import AppContext from '../../../AppContext'

import moment from 'moment'
// axios api
import api from '../../../api/api'
// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { cibOcaml } from '@coreui/icons'

function AddVitalForm() {
  const x = 0
  const { vitalData, setVitalData, content, setContent, dialogClose, setDialogClose } =
    useContext(AppContext)

  const [buttonIsEnable, setButtonIsEnable] = useState(true)
  // useState for all the input fields
  const [woundedBodySegment, setWoundedBodySegment] = useState('')
  const [woundLocation, setWoundLocation] = useState('')
  const [specificWoundLocation, setSpecificWoundLocation] = useState('')
  const [dateDiscover, setDateDiscover] = useState('')
  const [onSetDate, setOnSetDate] = useState('')
  const [woundPresentOn, setWoundPresentOn] = useState('')
  const [woundHealing, setWoundHealing] = useState('')
  const [anatomicalDirection, setAnatomicalDirection] = useState('')
  const [redness, setRedness] = useState('')

  const minAverageHumanTemperatue = 36.1
  const maxAverageHumanTemperatue = 37.2

  const AddWoundData = (e) => {
    e.preventDefault()
    // conversion

    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')
    let userId = localStorage.getItem('userId')

    const input = {
      woundedBodySegment: woundedBodySegment,
      woundLocation: woundLocation,
      specificWoundLocation: specificWoundLocation,
      dateDiscover: dateDiscover,
      onSetDate: onSetDate,
      woundPresentOn: woundPresentOn,
      woundHealing: woundHealing,
      anatomicalDirection: anatomicalDirection,
      redness: redness,
      ResidentId: tenantId,
    }

    // api call for adding new wound data
    api
      .post(`/wound/add`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)

        // setVitalData([...vitalData, res.data])
        // adding shift log here
        // const shiftlogInput = {
        //   userId: userId,
        //   tenantName: tenantName,
        //   activity: `Monitor the vitals of ${tenantName}`,
        // }
        // api
        //   .post(`/shiftlog/${userId}/create`, shiftlogInput, {
        //     headers: {
        //       Authorization: `Bearer ${token}`,
        //     },
        //   })
        //   .then((result) => {
        //     console.log(result)
        //   })
        //   .catch((err) => {
        //     console.error(err)
        //   })
        // // adding history to database
        // const input2 = {
        //   title: `Monitor the vitals of ${tenantName}`,
        //   tenantName: tenantName,
        //   tenantId: tenantId,
        //   userName: name,
        // }
        // api
        //   .post(`/history/create/`, input2, {
        //     headers: {
        //       Authorization: `Bearer ${token}`,
        //     },
        //   })
        //   .then((result) => {})
        //   .catch((err) => {
        //     console.error(err)
        //   })

        toast.success('Added Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })

        // delay function

        setTimeout(function () {
          setDialogClose(false)
        }, 2000)
      })
  }

  return (
    <div>
      <Form id="form" onSubmit={(e) => AddWoundData(e)}>
        <Row>
          <Col md="6" sm="8" className="mx-auto colItem">
            <Form.Control
              type="text"
              required
              value={woundedBodySegment}
              onChange={(e) => setWoundedBodySegment(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label htmlFor="date">Wounded body segment</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto  colItem">
            <Form.Control
              type="text"
              required
              value={woundLocation}
              onChange={(e) => setWoundLocation(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label htmlFor="time">Wound location</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto  colItem">
            <Form.Control
              type="text"
              required
              value={specificWoundLocation}
              onChange={(e) => setSpecificWoundLocation(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label>Specific wound location</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto  colItem">
            <Form.Control
              type="date"
              required
              value={dateDiscover}
              onChange={(e) => setDateDiscover(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label>Date discover</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto  colItem">
            <Form.Control
              type="date"
              required
              value={onSetDate}
              onChange={(e) => setOnSetDate(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label>Onset date</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto  colItem">
            <Form.Control
              type="text"
              required
              value={woundPresentOn}
              onChange={(e) => setWoundPresentOn(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label>Wound present on</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto  colItem">
            <Form.Control
              type="text"
              required
              value={woundHealing}
              onChange={(e) => setWoundHealing(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label>Wound healing</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto  colItem">
            <Form.Control
              type="text"
              required
              value={anatomicalDirection}
              onChange={(e) => setAnatomicalDirection(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label>Anatomical Direction</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto  colItem">
            <Form.Control
              type="text"
              required
              value={redness}
              onChange={(e) => setRedness(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label>Redness</label>
          </Col>
          <Col md="12" className="mt-2 d-flex justify-content-center">
            <div className="d-grid gap-2 col-6 mx-auto">
              <CButton type="submit" color="primary">
                Add
              </CButton>
            </div>
          </Col>
          <Col md="12" className="d-flex justify-content-center mt-3">
            <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
            />
          </Col>
        </Row>
      </Form>
    </div>
  )
}

export default AddVitalForm
