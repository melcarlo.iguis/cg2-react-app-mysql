import React, { useContext, useState, useEffect } from 'react'
import { Row, Col, Form, Button } from 'react-bootstrap'
import AddIcon from '@mui/icons-material/Add'
import Swal from 'sweetalert2'
import { CButton } from '@coreui/react'
import KeyboardVoiceIcon from '@mui/icons-material/KeyboardVoice'

// global variable
import AppContext from '../../../AppContext'

// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

// pop up
import Popup from '../popup/Popup'

import moment from 'moment'
// axios api
import api from '../../../api/api'

// speech recognition
// const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition

// const mic = new SpeechRecognition()

// mic.continous = true
// mic.interimResults = true
// mic.lang = 'en-US'

function AddObservationForm() {
  const { dailylogData, setDailylogData, content, setContent, dialogClose, setDialogClose } =
    useContext(AppContext)

  // useState for all the input fields
  const [date, setDate] = useState('')
  const [time, setTime] = useState('')
  const [note, setNote] = useState('')
  const [title, setTitle] = useState('')

  // const [isListening , setIsListening] = useState(false);
  // const [speechToText , setSpeechToText] = useState(false);

  // useEffect(()=>{
  // 	handleListen();
  // },[isListening])
  // function for speech recognition
  // const handleListen = () => {

  // 	if(isListening){
  // 		mic.start()
  // 		mic.onend = () => {
  // 			console.log('continue. . .')
  // 			mic.start()
  // 		}
  // 	}else if(isListening){
  // 		mic.stop()
  // 		mic.onend = () => {
  // 			console.log('Stopped mic on click')

  // 		}
  // 	}

  // 	mic.onstart = () =>{
  // 		console.log('Mic is on')
  // 	}

  // 	mic.onresult = event => {
  // 		const transcript = Array.from(event.results)
  // 		.map(result => result[0])
  // 		.map(result => result.transcript)
  // 		.join('')

  // 		console.log(transcript)
  // 		setNote([...note, transcript])
  // 		mic.onerror = event =>{
  // 			console.log(event.error)
  // 		}
  // 	}

  // }

  const [buttonIsEnable, setButtonIsEnable] = useState(true)

  const AddObservation = (e) => {
    e.preventDefault()
    setButtonIsEnable(false)

    // conversion
    const dateTime = moment(date + ' ' + time)

    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')
    let userId = localStorage.getItem('userId')

    const input = {
      date: dateTime,
      title: title,
      note: note,
    }

    // api call for adding new observation
    api
      .post(`/observation/${tenantId}/create`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        const input3 = {
          _id: Math.floor(Math.random() * 100),
          date: dateTime,
          title: title,
          note: note,
        }

        setDailylogData([...dailylogData, input3])
        // adding shift log here
        const shiftlogInput = {
          userId: userId,
          tenantName: tenantName,
          activity: `Added ${tenantName}'s observation data titled ${title}`,
        }
        api
          .post(`/shiftlog/${userId}/create`, shiftlogInput, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((result) => {
            console.log(result)
          })
          .catch((err) => {
            console.error(err)
          })
        // adding history to database
        const input2 = {
          title: `Added ${tenantName}'s observation data titled ${title}`,
          tenantName: tenantName,
          tenantId: tenantId,
          userName: name,
        }
        api
          .post(`/history/create/`, input2, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((result) => {})
          .catch((err) => {
            console.error(err)
          })

        toast.success('Added Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })

        // delay function
        setTimeout(function () {
          setDialogClose(false)
        }, 2000)
      })
  }

  return (
    <div>
      <Form id="form" onSubmit={(e) => AddObservation(e)}>
        <Row>
          <Col md="6" sm="8" className="mx-auto colItem">
            <Form.Control
              type="date"
              name="date"
              required
              value={date}
              onChange={(e) => setDate(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label htmlFor="date">Date</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto  colItem">
            <Form.Control
              type="time"
              name="time"
              required
              value={time}
              onChange={(e) => setTime(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label htmlFor="time">Time</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto  colItem">
            <Form.Control
              type="text"
              required
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              placeholder=" "
              className="formItem form__input"
            />
            <label>Title</label>
          </Col>
          <Col md="10" sm="10" className="mx-auto mb-3 colItem">
            <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
              <Form.Label>Note</Form.Label>
              <Form.Control
                required
                as="textarea"
                rows={3}
                value={note}
                onChange={(e) => setNote(e.target.value)}
              />
            </Form.Group>
          </Col>
          <Col md="12" className="d-flex justify-content-center">
            <div className="d-grid gap-2 col-6 mx-auto">
              <CButton type="submit" color="primary">
                Add
              </CButton>
            </div>
          </Col>
          <Col md="12" className="d-flex justify-content-center">
            <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
            />
          </Col>
        </Row>
      </Form>
      {/*<button id="actionBtn" onClick={() => setIsListening(!isListening)}>
        <KeyboardVoiceIcon />
        {isListening ? (
          <div className="d-inline px-2">Off Mic</div>
        ) : (
          <div className="d-inline px-2">On Mic</div>
        )}
      </button>*/}
    </div>
  )
}

export default AddObservationForm
