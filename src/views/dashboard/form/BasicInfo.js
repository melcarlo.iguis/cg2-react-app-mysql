import React, { useState, useEffect } from 'react'
import Box from '@mui/material/Box'
import noPhoto from '../../../assets/images/nophoto.jpg'
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate'
import TextField from '@mui/material/TextField'
import { Row, Col, Container, Form } from 'react-bootstrap'

import CheckIcon from '@mui/icons-material/Check'
import Tooltip from '@mui/material/Tooltip'
// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import ReplayIcon from '@mui/icons-material/Replay'
// axios api
import { CButton } from '@coreui/react'
import api from '../../../api/api'

function BasicInfo() {
  // useState for input field
  const [firstName, setFirstName] = useState('')
  const [middleName, setMiddleName] = useState('')
  const [lastName, setLastName] = useState('')
  const [preferredName, setPrefferedName] = useState('')
  const [resId, setResId] = useState('')
  const [uniqId, setUniqId] = useState('')
  const [roomName, setRoomName] = useState('')
  const [careLevel, setCareLevel] = useState('')
  const [contractType, setContractType] = useState('')
  const [placeofBirth, setPlaceofBirth] = useState('')
  const [maritasStatus, setMaritalStatus] = useState('')
  const [churchNameorAffiliation, setChurchNameorAffiliation] = useState('')
  const [race, setRace] = useState('')
  const [eyeColor, setEyeColor] = useState('')
  const [affinity, setAffinity] = useState('')
  const [veteranNo, setVeteranNo] = useState('')
  const [languages, setLanguages] = useState([])

  const [hairColor, setHairColor] = useState('')

  let tenantId = localStorage.getItem('tenantId')
  // states for picture
  const [imageFile, setImageFile] = useState('')
  const [image, setImage] = useState('')

  // adding basic info data
  const updateBasicInfo = (e) => {
    e.preventDefault()
    let tenantId = localStorage.getItem('tenantId')
    let token = localStorage.getItem('token')

    const formData = new FormData()
    api
      .get(`/basicInfo/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        if (result.data.length == 0) {
          if (imageFile != '') {
            formData.append('image', imageFile)
            formData.append('preferredName', preferredName)
            formData.append('contractType', contractType)
            formData.append('placeofBirth', placeofBirth)
            formData.append('maritalStatus', maritasStatus)
            formData.append('race', race)
            formData.append('eyeColor', eyeColor)
            formData.append('hairColor', hairColor)
            formData.append('affinity', affinity)
            formData.append('ResidentId', tenantId)
            formData.append('churchNameorAffiliation', churchNameorAffiliation)

            // api call for adding new tenant
            api
              .post(`/basicInfo/add`, formData, {
                headers: {
                  Authorization: `Bearer ${token}`,
                },
              })
              .then((res) => {
                console.log(res)
              })
          } else {
            toast.error('Please Upload an Image', {
              position: 'top-right',
              autoClose: 2000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: false,
              draggable: true,
              progress: undefined,
            })
          }
        } else {
          if (imageFile != '') {
            formData.append('image', imageFile)
            formData.append('preferredName', preferredName)
            formData.append('contractType', contractType)
            formData.append('placeofBirth', placeofBirth)
            formData.append('maritalStatus', maritasStatus)
            formData.append('race', race)
            formData.append('eyeColor', eyeColor)
            formData.append('hairColor', hairColor)
            formData.append('affinity', affinity)
            formData.append('churchNameorAffiliation', churchNameorAffiliation)

            console.log(formData)

            // api call for adding new tenant
            api
              .put(`/basicInfo/${tenantId}/updatewithimage`, formData, {
                headers: {
                  Authorization: `Bearer ${token}`,
                },
              })
              .then((result) => {
                console.log(result)
                // localStorage.setItem('tenantId', result.data.id)
                // setTenantList([...tenantList, result.data])
                // setButtonIsEnable(false)

                // code for adding contact details for tenant

                // api call for adding new tenant

                toast.success('Update Successfully', {
                  position: 'top-right',
                  autoClose: 2000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: false,
                  draggable: true,
                  progress: undefined,
                })
              })
              .catch((err) => {
                console.log(err)
              })
          } else {
            console.log('no image')
            formData.append('preferredName', preferredName)
            formData.append('contractType', contractType)
            formData.append('placeofBirth', placeofBirth)
            formData.append('maritalStatus', maritasStatus)
            formData.append('race', race)
            formData.append('eyeColor', eyeColor)
            formData.append('hairColor', hairColor)
            formData.append('affinity', affinity)
            formData.append('churchNameorAffiliation', churchNameorAffiliation)

            console.log(formData)

            // api call for adding new tenant
            api
              .put(`/basicInfo/${tenantId}/update`, formData, {
                headers: {
                  Authorization: `Bearer ${token}`,
                },
              })
              .then((result) => {
                console.log(result)
                // localStorage.setItem('tenantId', result.data.id)
                // setTenantList([...tenantList, result.data])
                // setButtonIsEnable(false)

                // code for adding contact details for tenant

                // api call for adding new tenant

                toast.success('Update Successfully', {
                  position: 'top-right',
                  autoClose: 2000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: false,
                  draggable: true,
                  progress: undefined,
                })
              })
              .catch((err) => {
                console.log(err)
              })
          }
        }
      })
  }

  const fetchResidentData = async () => {
    console.log('hello')
    let token = localStorage.getItem('token')
    await api
      .get(`/residents/${tenantId}/fetchAllData`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        console.log('hello')
        setInitialValue(res.data)
      })
  }

  const setInitialValue = (data) => {
    setFirstName(data.firstName)
    setMiddleName(data.middleName)
    setLastName(data.lastName)
    setResId('00000' + data.id)
    setUniqId('00000000' + data.id)
    setRoomName(data.communityName)
    setCareLevel(data.careLevel)
    if (data.BasicInfo != null) {
      setPlaceofBirth(data.BasicInfo.placeofBirth)
      setMaritalStatus(data.BasicInfo.maritalStatus)
      setPrefferedName(data.BasicInfo.preferredName)
      setImage(data.BasicInfo.idPicture)
      setRace(data.BasicInfo.race)
      setEyeColor(data.BasicInfo.eyeColor)
      setHairColor(data.BasicInfo.hairColor)
    }
    // setLanguages(data.BasicInfo.languages.join(' • '))
  }

  useEffect(() => {
    fetchResidentData()
  }, [tenantId])

  const handleFileInputChange = (e) => {
    const file = e.target.files[0]
    previewFile(file)
    setImageFile(file)
  }

  const previewFile = (file) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onloadend = () => {
      setImage(reader.result)
    }
  }

  console.log(eyeColor)

  return (
    <>
      <Container fluid>
        <form onSubmit={(e) => updateBasicInfo(e)}>
          <Row>
            <div className="image-holder">
              <div>
                <Col md="12" sm="8" className="mx-auto mb-3 mt-2 text-center">
                  {image ? (
                    <img id="tenantPic2" src={image} alt=""></img>
                  ) : (
                    <img id="tenantPic2" src={noPhoto} alt=""></img>
                  )}
                </Col>
                <Col md="12" sm="12" className="mx-auto text-center">
                  <Col md="6" sm="6" className="mx-auto text-center icon-holder">
                    <Tooltip title="Upload Picture" placement="right">
                      <label id="input-label" htmlFor="input-file">
                        <AddPhotoAlternateIcon />
                      </label>
                    </Tooltip>

                    <Form.Control
                      type="file"
                      id="input-file"
                      hidden
                      // value={fileInputState}
                      onChange={handleFileInputChange}
                    />
                  </Col>
                </Col>
              </div>
              <label className="label">ID Picture</label>
            </div>
            <Col md="4" sm="6">
              <TextField
                fullWidth
                InputProps={{ style: { fontSize: '15px', margin: '10px 0', color: 'blue' } }}
                value={resId}
                id="standard-basic"
                label="Resident ID"
                variant="standard"
              />
            </Col>
            <Col md="4" sm="6">
              <TextField
                fullWidth
                InputProps={{ style: { fontSize: '15px', margin: '10px 0', color: 'blue' } }}
                value={firstName}
                id="standard-basic"
                label="First Name"
                variant="standard"
              />
            </Col>
            <Col md="4" sm="6">
              <TextField
                fullWidth
                InputProps={{ style: { fontSize: '15px', margin: '10px 0', color: 'blue' } }}
                id="standard-basic"
                value={middleName}
                label="Middle Name"
                variant="standard"
              />
            </Col>
            <Col md="4" sm="6">
              <TextField
                fullWidth
                InputProps={{ style: { fontSize: '15px', margin: '10px 0', color: 'blue' } }}
                id="standard-basic"
                label="Last Name"
                value={lastName}
                variant="standard"
              />
            </Col>
            <Col md="4" sm="6">
              <TextField
                fullWidth
                InputProps={{ style: { fontSize: '15px', margin: '10px 0', color: 'blue' } }}
                id="standard-basic"
                value={preferredName}
                label="Preferred Name"
                variant="standard"
              />
            </Col>
            <Col md="4" sm="6">
              <TextField
                fullWidth
                InputProps={{ style: { fontSize: '15px', margin: '10px 0', color: 'blue' } }}
                id="standard-basic"
                label="Move In Date"
                variant="standard"
              />
            </Col>
            <Col md="4" sm="6">
              <TextField
                fullWidth
                InputProps={{ style: { fontSize: '15px', margin: '10px 0', color: 'blue' } }}
                id="standard-basic"
                label="Unit"
                value={roomName}
                variant="standard"
              />
            </Col>
            <Col md="4" sm="6">
              <TextField
                fullWidth
                InputProps={{ style: { fontSize: '15px', margin: '10px 0', color: 'blue' } }}
                id="standard-basic"
                label="Care Level"
                value={careLevel}
                variant="standard"
              />
            </Col>
            <Col md="4" sm="6">
              <TextField
                fullWidth
                InputProps={{ style: { fontSize: '15px', margin: '10px 0', color: 'blue' } }}
                id="standard-basic"
                label="Contract Type"
                value={contractType}
                onChange={(e) => setContractType(e.target.value)}
                variant="standard"
              />
            </Col>
            <Col md="4" sm="6">
              <TextField
                fullWidth
                InputProps={{ style: { fontSize: '15px', margin: '10px 0', color: 'blue' } }}
                id="standard-basic"
                value={placeofBirth}
                onChange={(e) => setPlaceofBirth(e.target.value)}
                label="Place of Birth"
                variant="standard"
              />
            </Col>
            <Col md="4" sm="6">
              <TextField
                fullWidth
                InputProps={{ style: { fontSize: '15px', margin: '10px 0', color: 'blue' } }}
                id="standard-basic"
                value={maritasStatus}
                onChange={(e) => setMaritalStatus(e.target.value)}
                label="Marital Status"
                variant="standard"
              />
            </Col>
            <Col md="4" sm="6">
              <TextField
                fullWidth
                InputProps={{ style: { fontSize: '15px', margin: '10px 0', color: 'blue' } }}
                id="standard-basic"
                value={race}
                onChange={(e) => setRace(e.target.value)}
                label="Race"
                variant="standard"
              />
            </Col>
            <Col md="4" sm="6">
              <TextField
                fullWidth
                value={affinity}
                onChange={(e) => setAffinity(e.target.value)}
                InputProps={{ style: { fontSize: '15px', margin: '10px 0', color: 'blue' } }}
                id="standard-basic"
                label="Affinity"
                variant="standard"
              />
            </Col>
            <Col md="4" sm="6">
              <TextField
                fullWidth
                InputProps={{ style: { fontSize: '15px', margin: '10px 0', color: 'blue' } }}
                id="standard-basic"
                label="Languages"
                value={languages}
                variant="standard"
              />
            </Col>
            <Col md="4" sm="6">
              <TextField
                fullWidth
                InputProps={{ style: { fontSize: '15px', margin: '10px 0', color: 'blue' } }}
                id="standard-basic"
                label="Eye color"
                value={eyeColor}
                onChange={(e) => setEyeColor(e.target.value)}
                variant="standard"
              />
            </Col>
            <Col md="4" sm="6">
              <TextField
                fullWidth
                InputProps={{ style: { fontSize: '15px', margin: '10px 0', color: 'blue' } }}
                id="standard-basic"
                label="Hair color"
                value={hairColor}
                onChange={(e) => setHairColor(e.target.value)}
                variant="standard"
              />
            </Col>
            {/* <Col md="4" sm="6">
          <TextField
            fullWidth
            InputProps={{ style: { fontSize: '15px', margin: '10px 0', color: 'blue' } }}
            id="standard-basic"
            label="Identifying marks"
            variant="standard"
          />
        </Col> */}
            <Col md="4" sm="6">
              <TextField
                fullWidth
                InputProps={{ style: { fontSize: '15px', margin: '10px 0', color: 'blue' } }}
                id="standard-basic"
                label="Church Name/Affiliation"
                variant="standard"
                value={churchNameorAffiliation}
                onChange={(e) => setChurchNameorAffiliation(e.target.value)}
              />
            </Col>
          </Row>
          <Tooltip title="Save" placement="bottom">
            <div id="submit-btn5">
              <label htmlFor="submit-btn">
                <CheckIcon />
              </label>
            </div>
          </Tooltip>
          <Tooltip title="Undo" placement="bottom">
            <div id="submit-btn6">
              <ReplayIcon />
            </div>
          </Tooltip>
          <CButton type="submit" id="submit-btn" hidden></CButton>
        </form>
      </Container>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </>
  )
}

export default BasicInfo
