import React, { useEffect, useState, useContext, useRef } from 'react'
import { Row, Col, Form, Button, Container } from 'react-bootstrap'
import AddIcon from '@mui/icons-material/Add'
import { styled } from '@mui/material/styles'
import Box from '@mui/material/Box'
import Paper from '@mui/material/Paper'
import Grid from '@mui/material/Grid'

// global variable
import AppContext from '../../../AppContext'
import moment from 'moment'
// axios api
import api from '../../../api/api'

// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function OnScreenNumpad(data) {
  const {
    incidentReportList,
    setIncidentReportList,
    content,
    setContent,
    vitalData,
    setVitalData,
    dialogClose,
    setDialogClose,
    inputValue,
    setInputValue,
  } = useContext(AppContext)

  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }))

  const handleOnClick = (e, value) => {
    e.preventDefault()
    if (value != 'clear') {
      setInputValue(inputValue + value)
    } else {
      const newValue = inputValue.slice(0, -1)
      setInputValue(newValue)
    }
  }

  return (
    <div id="onscreen-keyboard">
      <Grid
        className="my-2 pb-4 px-4"
        container
        rowSpacing={2}
        columnSpacing={{ xs: 1, sm: 2, md: 2 }}
      >
        <Grid item xs={4}>
          <div
            onClick={(e) => handleOnClick(e, '1')}
            className="btn onscreen-item position-relative"
          >
            <p className="onscreen-tag">1</p>
          </div>
        </Grid>
        <Grid item xs={4}>
          <div
            onClick={(e) => handleOnClick(e, '2')}
            className="btn onscreen-item position-relative"
          >
            <p className="onscreen-tag">2</p>
          </div>
        </Grid>
        <Grid item xs={4}>
          <div
            onClick={(e) => handleOnClick(e, '3')}
            className="btn onscreen-item position-relative"
          >
            <p className="onscreen-tag">3</p>
          </div>
        </Grid>
        <Grid item xs={4}>
          <div
            onClick={(e) => handleOnClick(e, '4')}
            className="btn onscreen-item position-relative"
          >
            <p className="onscreen-tag">4</p>
          </div>
        </Grid>
        <Grid item xs={4}>
          <div
            onClick={(e) => handleOnClick(e, '5')}
            className="btn onscreen-item position-relative"
          >
            <p className="onscreen-tag">5</p>
          </div>
        </Grid>
        <Grid item xs={4}>
          <div
            onClick={(e) => handleOnClick(e, '6')}
            className="btn onscreen-item position-relative"
          >
            <p className="onscreen-tag">6</p>
          </div>
        </Grid>
        <Grid item xs={4}>
          <div
            onClick={(e) => handleOnClick(e, '7')}
            className="btn onscreen-item position-relative"
          >
            <p className="onscreen-tag">7</p>
          </div>
        </Grid>
        <Grid item xs={4}>
          <div
            onClick={(e) => handleOnClick(e, '8')}
            className="btn onscreen-item position-relative"
          >
            <p className="onscreen-tag">8</p>
          </div>
        </Grid>
        <Grid item xs={4}>
          <div
            onClick={(e) => handleOnClick(e, '9')}
            className="btn onscreen-item position-relative"
          >
            <p className="onscreen-tag">9</p>
          </div>
        </Grid>
        <Grid item xs={4}>
          <div
            onClick={(e) => handleOnClick(e, '.')}
            className="btn onscreen-item position-relative"
          >
            <p className="onscreen-tag">.</p>
          </div>
        </Grid>
        <Grid item xs={4}>
          <div
            onClick={(e) => handleOnClick(e, '0')}
            className="btn onscreen-item position-relative"
          >
            <p className="onscreen-tag">0</p>
          </div>
        </Grid>
        <Grid item xs={4}>
          <div
            onClick={(e) => handleOnClick(e, 'clear')}
            className="btn onscreen-item position-relative"
          >
            <p className="onscreen-tag">clear</p>
          </div>
        </Grid>
      </Grid>
    </div>
  )
}

export default OnScreenNumpad
