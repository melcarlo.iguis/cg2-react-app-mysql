import React, { useState, useEffect, useContext } from 'react'
import { Row, Col, Form, Button } from 'react-bootstrap'
import AddIcon from '@mui/icons-material/Add'
import { CButton } from '@coreui/react'
// global variable
import AppContext from '../../../AppContext'
// axios api
import api from '../../../api/api'
// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function AddNewBehaviorForm() {
  const {
    behaviorData,
    setBehaviorData,
    allergiesList,
    setAllergiesList,
    dialogClose,
    setDialogClose,
  } = useContext(AppContext)
  // State for all the input field
  const [behavior, setBehavior] = useState('')
  const [startDate, setStartDate] = useState('')
  const [location, setLocation] = useState('')
  const [alterable, setAlterable] = useState('')
  const [riskTo, setRiskTo] = useState('')
  const [times, setTimes] = useState('')
  const [triggerBy, setTriggerBy] = useState('')
  const [review, setReview] = useState('')

  let name = localStorage.getItem('name')
  let token = localStorage.getItem('token')
  let tenantName = localStorage.getItem('tenantName')
  let tenantId = localStorage.getItem('tenantId')

  const AddNewBehavior = (e) => {
    e.preventDefault()
    let userId = localStorage.getItem('userId')
    console.log('yow')

    const input = {
      behavior: behavior,
      startDate: startDate,
      location: location,
      alterable: alterable,
      riskTo: riskTo,
      times: times,
      triggerBy: triggerBy,
      review: review,
      ResidentId: tenantId,
    }

    // api call for adding new tenant
    api
      .post(`/behavior/add`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        setBehaviorData([...behaviorData, result.data])

        toast.success('Added Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
        // adding shift log here
        // const shiftlogInput = {
        //   userId: userId,
        //   tenantName: tenantName,
        //   activity: `Added ${behavior} as a new behavior data of ${tenantName}`,
        // }
        // api
        //   .post(`/shiftlog/${userId}/create`, shiftlogInput, {
        //     headers: {
        //       Authorization: `Bearer ${token}`,
        //     },
        //   })
        //   .then((result) => {
        //     console.log(result)
        //   })
        //   .catch((err) => {
        //     console.error(err)
        //   })
        // adding history to database
        const dailyLogInput = {
          title: `${name} added ${behavior} as a new behavior data of ${tenantName}`,
          residentName: tenantName,
          userName: name,
          ResidentId: tenantId,
        }

        api
          .post(`/dailylog/add`, dailyLogInput, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((result) => {
            setDailylogData([...dailylogData, result.data])
            console.log(result)
          })
          .catch((err) => {
            console.error(err)
          })

        // delay function

        setTimeout(function () {
          setDialogClose(false)
        }, 2000)
      })
      .catch((err) => {
        console.log(err)
      })
  }

  // useEffect(() => {
  //   if (allergy !== '' && allergyType !== '' && startDate !== '') {
  //     const timeoutId = setTimeout(() => addAllergy(), 1000)
  //     return () => clearTimeout(timeoutId)
  //   }
  // }, [allergy, allergyType, startDate, allergenType])

  return (
    <div>
      <Form id="form" onSubmit={(e) => AddNewBehavior(e)}>
        <Row>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              name="sportsCode"
              required
              value={behavior}
              onChange={(e) => setBehavior(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label>Behavior</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="date"
              name="date"
              required
              value={startDate}
              onChange={(e) => setStartDate(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label htmlFor="date">Start Date</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem ">
            <Form.Control
              type="text"
              required
              value={location}
              onChange={(e) => setLocation(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label>Location</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              value={alterable}
              onChange={(e) => setAlterable(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label>Alterable</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              placeholder=" "
              required
              value={riskTo}
              onChange={(e) => setRiskTo(e.target.value)}
              className="formItem mt-3 form__input"
            />
            <label>Risk To</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              placeholder=" "
              required
              value={times}
              onChange={(e) => setTimes(e.target.value)}
              className="formItem mt-3 form__input"
            />
            <label>Times</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              placeholder=" "
              required
              value={triggerBy}
              onChange={(e) => setTriggerBy(e.target.value)}
              className="formItem mt-3 form__input"
            />
            <label>Trigger By</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              placeholder=" "
              required
              value={review}
              onChange={(e) => setReview(e.target.value)}
              className="formItem mt-3 form__input"
            />
            <label>Review</label>
          </Col>
          <Col md="12" className="d-flex justify-content-center">
            <div className="d-grid gap-2 col-6 mx-auto">
              <CButton type="submit" color="primary">
                Add
              </CButton>
            </div>
          </Col>
          <Col md="12" className="d-flex justify-content-center">
            <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
            />
          </Col>
        </Row>
      </Form>
    </div>
  )
}

export default AddNewBehaviorForm
