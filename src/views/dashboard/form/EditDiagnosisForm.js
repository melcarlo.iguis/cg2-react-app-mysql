import React, { useState, useEffect, useContext, useRef } from 'react'
import { Row, Col, Form, Button } from 'react-bootstrap'
import AddIcon from '@mui/icons-material/Add'

// global variable
import AppContext from '../../../AppContext'
// axios api
import api from '../../../api/api'
// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function EditAllergyForm(data) {
  console.log(data)
  const {
    currentTab,
    allergiesList,
    setAllergiesList,
    tenantList,
    setTenantList,
    content,
    setContent,
    dialogClose,
    setDialogClose,
    setDiagnosisData,
    diagnosisData,
  } = useContext(AppContext)
  // State for all the input field
  const [diagnosisName, setDiagnosisName] = useState('')
  const [startDate, setStartDate] = useState('')
  const [endDate, setEndDate] = useState('')

  // for Initial data from the edited data
  useEffect(() => {
    setDiagnosisName(data.data.diagnosisName)
    checkDates()
  }, [])

  const checkDates = () => {
    if (
      data.data.startDate == '' ||
      data.data.startDate == undefined ||
      data.data.startDate == null
    ) {
      setStartDate('')
    } else {
      let startDateInput = new Date(data.data.startDate)
      let newDate = startDateInput.toISOString().substr(0, 10)
      setStartDate(newDate)
    }

    if (data.data.endDate == '' || data.data.endDate == undefined || data.data.endDate == null) {
      setEndDate('')
    } else {
      let startDateInput = new Date(data.data.endDate)
      let newDate = startDateInput.toISOString().substr(0, 10)
      setEndDate(newDate)
    }
  }

  const firstUpdate = useRef(true)

  const updateDiagnosis = () => {
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    const input = {
      diagnosisName: diagnosisName,
      startDate: startDate,
      endDate: endDate,
    }

    if (firstUpdate.current) {
      firstUpdate.current = false
      return
    } else {
      // api call for adding new tenant
      api
        .put(`/diagnosis/${data.data.id}/update`, input, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((result) => {
          // updating the College student on it's state to instantly apply the changes without refreshing the page
          diagnosisData.forEach((item) => {
            if (item.id === data.data.id) {
              item.diagnosisName = diagnosisName
              item.startDate = startDate
              item.endDate = endDate
            }
          })

          setDiagnosisData([...diagnosisData])

          // adding history to database
          const input2 = {
            title: `Edited ${tenantName}'s diagnosis data on ${diagnosisName}`,
            tenantName: tenantName,
            tenantId: tenantId,
            userName: name,
          }
          api
            .post(`/history/create/`, input2, {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
            .then((result) => {})
            .catch((err) => {
              console.error(err)
            })
        })
        .catch((err) => {
          console.log(err)
        })

      toast.success('Updated Successfully', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
      })
    }
  }

  useEffect(() => {
    const timeoutId = setTimeout(() => updateDiagnosis(), 2000)
    return () => clearTimeout(timeoutId)
  }, [diagnosisName, startDate, endDate])

  return (
    <div>
      <Form id="form">
        <Row>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              required
              value={diagnosisName}
              onChange={(e) => setDiagnosisName(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label htmlFor="sportsCode">Diagnosis Name</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="date"
              placeholder=" "
              required
              value={startDate}
              onChange={(e) => setStartDate(e.target.value)}
              className="formItem mt-3 form__input"
            />
            <label>Start Date</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="date"
              placeholder=" "
              required
              value={endDate}
              onChange={(e) => setEndDate(e.target.value)}
              className="formItem mt-3 form__input"
            />
            <label>End Date</label>
          </Col>
          {/*<Col md="12" className="d-flex justify-content-center">
            <div className="d-grid gap-2 col-6 mx-auto">
              <CButton type="submit" color="primary">
                Add
              </CButton>
            </div>
          </Col>*/}
          <Col md="12" className="d-flex justify-content-center">
            <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
            />
          </Col>
        </Row>
      </Form>
    </div>
  )
}

export default EditAllergyForm
