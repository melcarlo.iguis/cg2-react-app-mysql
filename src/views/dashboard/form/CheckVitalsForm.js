import React, { useContext, useState, useEffect, useRef } from 'react'
import { Row, Col, Form, Button } from 'react-bootstrap'
import AddIcon from '@mui/icons-material/Add'
import Swal from 'sweetalert2'
import { CButton } from '@coreui/react'
// global variable
import AppContext from '../../../AppContext'
import { CircularProgress } from '@mui/material'
import TextField from '@mui/material/TextField'

import moment from 'moment'
// axios api
import api from '../../../api/api'
// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { cibOcaml } from '@coreui/icons'

import Paper from '@mui/material/Paper'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TablePagination from '@mui/material/TablePagination'
import TableRow from '@mui/material/TableRow'

import OnScreenNumpad from './OnScreenNumpad'

function CheckVitalsForm() {
  const {
    vitalData,
    setVitalData,
    content,
    setContent,
    dialogClose,
    setDialogClose,
    notificationData,
    setNotificationData,
    notifTableData,
    setNotifTableData,
    inputValue,
    setInputValue,
  } = useContext(AppContext)

  const [buttonIsEnable, setButtonIsEnable] = useState(true)
  const [isFetchDone, setIsFetchDone] = useState(false)
  // useState for all the input fields
  const [date, setDate] = useState('')
  const [time, setTime] = useState('')
  const [bodyTemp, setBodyTemp] = useState(0)
  const [pulse, setPulse] = useState('')
  const [respiration, setRespiration] = useState('')
  const [systolic, setSystolic] = useState('')
  const [diastolic, setDiastolic] = useState('')
  const [activityInput, setActivityInput] = useState('')

  const [notifInputValue, setNotifInputValue] = useState('')

  const arr1 = ['A', 'B', 'C']
  const arr2 = ['1', '2', 'C']
  // set initial value for time and date
  useEffect(() => {
    console.log('hehehe')
    const d = new Date()
    console.log(d)
    const year = d.getFullYear()
    const month = d.getMonth() + 1
    const day = d.getDate()
    const dateString = '' + year + '-' + month + '-' + day

    const dateFormated = moment(dateString).format('YYYY-MM-DD')

    setDate(dateFormated)

    let hrs = d.getHours()
    let mins = d.getMinutes()

    let today = new Date()
    const time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds()

    console.log(time)

    const newMins = mins < 10 ? '0' + mins : mins
    const newHrs = hrs < 10 ? '0' + hrs : hrs

    const postTime = newHrs + ':' + newMins
    console.log(postTime)
    setTime(postTime)
  }, [])

  const minAverageHumanTemperatue = 36.1
  const maxAverageHumanTemperatue = 37.2

  let notificationId = localStorage.getItem('notificationId')
  //   function for fetching notification data
  const fetchSpecificNotifData = async () => {
    console.log('hello')
    setIsFetchDone(false)
    let id = localStorage.getItem('notificationId')
    console.log(id)
    let token = localStorage.getItem('token')
    await api
      .get(`/notification/${id}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        setNotifTableData(res.data)
        console.log(notifTableData)
        setIsFetchDone(true)
      })
      .catch((err) => {
        console.error(err)
      })
  }

  useEffect(() => {
    fetchSpecificNotifData()
  }, [notificationId])

  console.log(notifTableData)

  const AddVital = (e) => {
    e.preventDefault()
    setInputValue('')
    localStorage.setItem('itemToModify', '')
    // conversion
    const dateTime = moment(date + ' ' + time)

    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')
    let userId = localStorage.getItem('userId')

    console.log(tenantId)
    const input = {
      date: dateTime,
      bodyTemp: bodyTemp,
      pulseRate: pulse,
      respirationRate: respiration,
      systolic: systolic,
      diastolic: diastolic,
      tenantName: tenantName,
      ResidentId: tenantId,
    }

    // api call for adding new vitals data
    api
      .post(`/vitals/add`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        // temperatureCheckIfNormal()
        setVitalData([...vitalData, res.data])
        updateNotification()
        // adding shift log here

        if (notifTableData[0].actionNeeded == 'checkBP') {
          // code for adding shiftlog summary
          const shiftSummaryLogInput = {
            activity: `check the Blood pressure of ${tenantName} with a value of ${systolic}/${diastolic}`,
            residentName: tenantName,
            userName: name,
            ResidentId: tenantId,
            UserId: userId,
          }

          api
            .post(`/shiftsummarylog/add`, shiftSummaryLogInput, {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
            .then((result) => {
              // setDailylogData([...dailylogData, result.data])
              console.log(result)
            })
            .catch((err) => {
              console.error(err)
            })
        } else if (notifTableData[0].actionNeeded == 'checkPulse') {
          const shiftSummaryLogInput = {
            activity: `check the Pulse Rate of ${tenantName} with a value of ${pulse} bpm`,
            residentName: tenantName,
            userName: name,
            ResidentId: tenantId,
            UserId: userId,
          }

          api
            .post(`/shiftsummarylog/add`, shiftSummaryLogInput, {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
            .then((result) => {
              // setDailylogData([...dailylogData, result.data])
              console.log(result)
            })
            .catch((err) => {
              console.error(err)
            })
        } else if (notifTableData[0].actionNeeded == 'checkTemperature') {
          const shiftSummaryLogInput = {
            activity: `check the Body Tempreature of ${tenantName}with a value of${bodyTemp} °C`,
            residentName: tenantName,
            userName: name,
            ResidentId: tenantId,
            UserId: userId,
          }

          api
            .post(`/shiftsummarylog/add`, shiftSummaryLogInput, {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
            .then((result) => {
              // setDailylogData([...dailylogData, result.data])
              console.log(result)
            })
            .catch((err) => {
              console.error(err)
            })
        }

        // adding history to database
        // const input2 = {
        //   title: `Monitor the vitals of ${tenantName}`,
        //   tenantName: tenantName,
        //   tenantId: tenantId,
        //   userName: name,
        // }
        // api
        //   .post(`/history/create/`, input2, {
        //     headers: {
        //       Authorization: `Bearer ${token}`,
        //     },
        //   })
        //   .then((result) => {})
        //   .catch((err) => {
        //     console.error(err)
        //   })

        toast.success('Added Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })

        // delay function

        setTimeout(function () {
          setDialogClose(false)
        }, 2000)
      })
  }

  useEffect(() => {
    if (notifTableData[0].actionNeeded == 'checkBP') {
      setNotifInputValue(systolic + '/' + diastolic)
    } else if (notifTableData[0].actionNeeded == 'checkPulse') {
      setNotifInputValue(pulse + 'BPM')
    } else if (notifTableData[0].actionNeeded == 'checkTemperature') {
      setNotifInputValue(bodyTemp + '°C')
    }
  }, [systolic, diastolic, pulse, bodyTemp])
  const updateNotification = () => {
    const input = {
      priorityLevel: 0,
      isCompleted: true,
      value: notifInputValue,
      dateAccomplish: new Date(),
    }
    // api call for marking the notification/action as done
    let id = localStorage.getItem('notificationId')
    let token = localStorage.getItem('token')
    api
      .put(`/notification/${id}/update`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        // updating the data on the state
        notificationData.forEach((item) => {
          if (item.id == id) {
            item.priorityLevel = notificationData[notificationData.length - 1].priorityLevel - 1
            item.isCompleted = true
            item.dateAccomplish = new Date().toLocaleString()
            item.value = notifInputValue
          }
        })
        setNotificationData([...notificationData])
      })
      .catch((err) => {
        console.log(err)
      })
  }

  const temperatureCheckIfNormal = () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    api
      .get(`/vitals/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        const filteredResult = result.data.map(({ bodyTemp }) => ({ bodyTemp }))
        const sum = filteredResult.reduce((accumulator, object) => {
          return accumulator + parseInt(object.bodyTemp)
        }, 0)
        console.log(filteredResult.length)
        const average = sum / filteredResult.length
        console.log(bodyTemp)
        if (bodyTemp >= minAverageHumanTemperatue && bodyTemp <= maxAverageHumanTemperatue) {
          console.log('the temperature is normal!!')
        } else {
          console.log('the temperature is not normal')
          let token = localStorage.getItem('token')
          let tenantName = localStorage.getItem('tenantName')
          let tenantId = localStorage.getItem('tenantId')
          // insert add notification here
          const input = {
            title: `${tenantName}'s temperature is not normal!`,
            tenantName: tenantName,
            ResidentId: tenantId,
          }
          api
            .post(`/notification/add`, input, {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
            .then((result) => {
              console.log(result)
              // setNotificationData([...notificationData, result.data])
            })
        }
      })
  }

  useEffect(() => {
    setInputToEdit()
  }, [isFetchDone])

  const setInputToEdit = () => {
    console.log('yow')
    if (isFetchDone) {
      if (notifTableData[0].actionNeeded == 'checkBP') {
        console.log('yee')
        localStorage.setItem('itemToModify', 'systolic')
      } else if (notifTableData[0].actionNeeded == 'checkTemperature') {
        localStorage.setItem('itemToModify', 'bodyTemp')
      } else if (notifTableData[0].actionNeeded == 'checkPulse') {
        localStorage.setItem('itemToModify', 'pulse')
      }
    }
  }

  const handleOnFocus = (valueTobeModify) => {
    let itemToModify = localStorage.getItem('itemToModify')
    if (itemToModify != valueTobeModify) {
      if (valueTobeModify == 'systolic') {
        setInputValue(systolic)
        localStorage.setItem('itemToModify', valueTobeModify)
      } else if (valueTobeModify == 'diastolic') {
        setInputValue(diastolic)
        localStorage.setItem('itemToModify', valueTobeModify)
      } else if (valueTobeModify == 'bodyTemp') {
        setInputValue(bodyTemp)
        localStorage.setItem('itemToModify', valueTobeModify)
      } else if (valueTobeModify == 'pulse') {
        setInputValue(pulse)
        localStorage.setItem('itemToModify', valueTobeModify)
      } else if (valueTobeModify == 'respiration') {
        setInputValue(respiration)
        localStorage.setItem('itemToModify', valueTobeModify)
      }
    }
  }

  const handleUpdateOnInput = () => {
    let itemToModify = localStorage.getItem('itemToModify')
    if (itemToModify == 'bp') {
      setBp(inputValue)
    } else if (itemToModify == 'bodyTemp') {
      setBodyTemp(inputValue)
    } else if (itemToModify == 'pulse') {
      setPulse(inputValue)
    } else if (itemToModify == 'respiration') {
      setRespiration(inputValue)
    } else if (itemToModify == 'systolic') {
      setSystolic(inputValue)
    } else if (itemToModify == 'diastolic') {
      setDiastolic(inputValue)
    } else {
      return
    }
  }
  useEffect(() => {
    handleUpdateOnInput()
  }, [inputValue])

  const firstUpdate = useRef(true)

  useEffect(() => {
    console.log('yow')
    if (firstUpdate.current) {
      firstUpdate.current = false
      return
    } else {
      setBodyTemp(Number(bodyTemp).toFixed(1))
    }
  }, [bodyTemp])

  if (isFetchDone == false) return <CircularProgress />
  return (
    <div>
      <Form id="form" onSubmit={(e) => AddVital(e)}>
        <Row>
          {notifTableData[0].actionNeeded == 'checkBP' && isFetchDone ? (
            <>
              <Col md="12" sm="6" className="mx-auto d-flex justify-content-center  colItem">
                <h3 className="mt-3">Systolic</h3>
                <TextField
                  type="number"
                  onFocus={() => handleOnFocus('systolic')}
                  onChange={(e) => setSystolic(e.target.value)}
                  style={{ width: '20%' }}
                  id="input-field-text"
                  value={systolic}
                  placeholder=" "
                  className="formItem mx-2  mt-3 form__input"
                />

                <h3 className="mt-3">Diastolic</h3>
                <TextField
                  type="text"
                  onFocus={() => handleOnFocus('diastolic')}
                  onChange={(e) => setDiastolic(e.target.value)}
                  style={{ width: '20%' }}
                  id="input-field-text"
                  value={diastolic}
                  placeholder=" "
                  className="mx-2 formItem mt-3 form__input"
                />
              </Col>
              <Col md="12" sm="6" className="mx-auto ">
                {/* insert onscreen keyboard here */}
                <OnScreenNumpad />
              </Col>
            </>
          ) : notifTableData[0].actionNeeded == 'checkTemperature' ? (
            <>
              <Col md="12" sm="6" className="mx-auto position-relative ">
                <Row>
                  <Col
                    md="12"
                    sm="6"
                    className="d-flex justify-content-center mx-auto position-relative"
                  >
                    <h3 className="mt-4 pl-2">Body Temperature</h3>
                    <TextField
                      type="number"
                      InputProps={{ inputProps: { min: 36, max: 40, step: 'any' } }}
                      onFocus={() => handleOnFocus('bodyTemp')}
                      onChange={(e) => setBodyTemp(e.target.value)}
                      value={bodyTemp}
                      id="input-field-text"
                      placeholder=" "
                      style={{ width: '20%' }}
                      className="formItem mt-3 form__input mx-2"
                    />
                    <h3 className="mt-4">°C</h3>
                  </Col>
                </Row>
              </Col>
              <Col md="12" sm="6" className="mx-auto ">
                {/* insert onscreen keyboard here */}
                <OnScreenNumpad />
              </Col>
            </>
          ) : notifTableData[0].actionNeeded == 'checkPulse' ? (
            <>
              <Col md="12" sm="6" className="mx-auto position-relative ">
                <Row>
                  <Col
                    md="12"
                    sm="6"
                    className="d-flex justify-content-center mx-auto position-relative"
                  >
                    <h3 className="mt-4 pl-2">Pulse Rate</h3>
                    <TextField
                      type="text"
                      onFocus={() => handleOnFocus('pulse')}
                      onChange={(e) => setPulse(e.target.value)}
                      value={pulse}
                      id="input-field-text"
                      placeholder=" "
                      style={{ width: '20%' }}
                      className=" mt-3 mx-2 "
                    />
                    <h3 className="mt-4 pl-2">BPM</h3>
                  </Col>
                </Row>
              </Col>
              <Col md="12" sm="6" className="mx-auto ">
                {/* insert onscreen keyboard here */}
                <OnScreenNumpad />
              </Col>
            </>
          ) : null}
          <Col md="12" sm="12" className="mx-auto colItem">
            <Row>
              <Col md="6" sm="6" className="mx-auto colItem">
                <div className="d-flex justify-content-center">
                  <Form.Control
                    type="date"
                    name="date"
                    style={{ width: '50%', textAlign: 'center', height: '50px', fontSize: '20px' }}
                    required
                    value={date}
                    label="Date"
                    onChange={(e) => setDate(e.target.value)}
                    className="formItem mt-3 form__input"
                  />
                </div>
              </Col>
              <Col md="6" sm="6" className="mx-auto  colItem position-relative">
                <div className="d-flex justify-content-center">
                  <Form.Control
                    type="time"
                    name="time"
                    style={{ width: '50%', textAlign: 'center', height: '50px', fontSize: '20px' }}
                    required
                    value={time}
                    onChange={(e) => setTime(e.target.value)}
                    placeholder=" "
                    className="formItem mt-3 form__input"
                  />
                </div>
              </Col>
            </Row>
          </Col>

          {notifTableData[0].isCompleted == false && isFetchDone ? (
            <>
              <Col md="12" className="d-flex justify-content-center">
                <div className="d-grid gap-2 col-2 mx-auto mt-2">
                  <CButton type="submit" id="submit-btn2">
                    Record
                  </CButton>
                </div>
              </Col>
            </>
          ) : null}

          <Col md="12" className="d-flex justify-content-center mt-3">
            <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
            />
          </Col>
        </Row>
      </Form>
    </div>
  )
}

export default CheckVitalsForm
