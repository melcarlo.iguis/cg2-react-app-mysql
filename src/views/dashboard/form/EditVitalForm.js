import React, { useEffect, useState, useContext, useRef } from 'react'
import { Row, Col, Form, Button, Container } from 'react-bootstrap'
import AddIcon from '@mui/icons-material/Add'

// global variable
import AppContext from '../../../AppContext'
import moment from 'moment'
// axios api
import api from '../../../api/api'

// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function EditVitalForm(data) {
  const {
    incidentReportList,
    setIncidentReportList,
    content,
    setContent,
    vitalData,
    setVitalData,
    dialogClose,
    setDialogClose,
  } = useContext(AppContext)

  console.log(dialogClose)

  const [buttonIsEnable, setButtonIsEnable] = useState(true)

  // useState for all the input fields
  const [date, setDate] = useState('')
  const [time, setTime] = useState('')
  const [bodyTemp, setBodyTemp] = useState('')
  const [pulse, setPulse] = useState('')
  const [respiration, setRespiration] = useState('')
  const [bp, setBp] = useState('')

  // Set all the initial value
  useEffect(() => {
    // to extract the date from datetime
    const d = new Date(data.data.date)
    console.log(d)
    const year = d.getFullYear()
    const month = d.getMonth() + 1
    const day = d.getDate()
    const dateString = '' + year + '-' + month + '-' + day

    const dateFormated = moment(dateString).format('YYYY-MM-DD')

    setDate(dateFormated)

    let hrs = d.getHours()
    let mins = d.getMinutes()

    const newMins = mins < 10 ? '0' + mins : mins

    const postTime = hrs + ':' + newMins
    setTime(postTime)

    setBodyTemp(data.data.bodyTemp)
    setPulse(data.data.pulseRate)
    setRespiration(data.data.respirationRate)
    setBp(data.data.bloodPressure)
  }, [])

  useEffect(() => {
    if (
      date !== '' &&
      time !== '' &&
      bodyTemp !== '' &&
      pulse !== '' &&
      respiration !== '' &&
      bp !== ''
    ) {
      const timeoutId = setTimeout(() => updateVitalData(), 2000)
      return () => clearTimeout(timeoutId)
    }
  }, [date, time, bodyTemp, pulse, respiration, bp])

  const firstUpdate = useRef(true)

  const updateVitalData = (e) => {
    // e.preventDefault()
    console.log('updating....')
    // setButtonIsEnable(false);

    const dateFormated = moment(date).format('YYYY-MM-DD')
    const dateTime = moment(dateFormated + ' ' + time)

    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    const input = {
      date: dateTime,
      bodyTemp: bodyTemp,
      pulseRate: pulse,
      respirationRate: respiration,
      bloodPressure: bp,
      tenantName: tenantName,
    }

    if (firstUpdate.current) {
      firstUpdate.current = false
      return
    } else {
      // api call for adding new tenant
      api
        .put(`/vitals/${data.data._id}/update`, input, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((result) => {
          console.log(result)

          // updating the College student on it's state to instantly apply the changes without refreshing the page
          vitalData.forEach((item) => {
            if (item._id === data.data._id) {
              item.date = dateTime
              item.bodyTemp = bodyTemp
              item.pulseRate = pulse
              item.respirationRate = respiration
              item.bloodPressure = bp
              item.tenantName = tenantName
            }
          })

          setVitalData([...vitalData])

          // adding history to database
          const input2 = {
            title: `Edited ${tenantName}'s Vitals reports data`,
            tenantName: tenantName,
            tenantId: tenantId,
            userName: name,
          }
          api
            .post(`/history/create/`, input2, {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
            .then((result) => {})
            .catch((err) => {
              console.error(err)
            })
        })
        .catch((err) => {
          console.log(err)
        })

      toast.success('Updated Successfully', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
      })
    }
  }

  return (
    <div>
      <Form id="form">
        <Row>
          <Col md="6" sm="8" className="mx-auto colItem">
            <Form.Control
              type="date"
              name="date"
              required
              value={date}
              onChange={(e) => setDate(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label htmlFor="date">Date</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto  colItem">
            <Form.Control
              type="time"
              name="time"
              required
              value={time}
              onChange={(e) => setTime(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label htmlFor="time">Time</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto  colItem">
            <Form.Control
              type="text"
              name="bodyTemp"
              required
              value={bodyTemp}
              onChange={(e) => setBodyTemp(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label>Body Temperature</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto  colItem">
            <Form.Control
              type="text"
              name="pulseRate"
              required
              value={pulse}
              onChange={(e) => setPulse(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label>Pulse Rate</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto  colItem">
            <Form.Control
              type="text"
              name="respirationRate"
              required
              value={respiration}
              onChange={(e) => setRespiration(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label>Respiration Rate</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto  colItem">
            <Form.Control
              type="text"
              name="bloodPressure"
              required
              value={bp}
              onChange={(e) => setBp(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label>Blood Pressure</label>
          </Col>
          <Col md="12" className="d-flex justify-content-center mt-3">
            <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
            />
          </Col>
        </Row>
      </Form>
    </div>
  )
}

export default EditVitalForm
