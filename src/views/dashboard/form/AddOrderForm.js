import React, { useState, useEffect, useContext } from 'react'
// import { Row, Col, Form, Button } from 'react-bootstrap'
import AddIcon from '@mui/icons-material/Add'
import { CButton } from '@coreui/react'
// global variable
import AppContext from '../../../AppContext'
// axios api
import api from '../../../api/api'
// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function AddOrderForm() {
  const {
    setDiagnosisData,
    diagnosisData,
    dialogClose,
    setDialogClose,
    eMarData,
    setEmarData,
    notesData,
    setNotesData,
    orderData,
    setOrderData,
  } = useContext(AppContext)
  // State for all the input field
  const [sampleField, setSampleField] = useState('')

  let name = localStorage.getItem('name')
  let token = localStorage.getItem('token')
  let tenantName = localStorage.getItem('tenantName')
  let tenantId = localStorage.getItem('tenantId')

  console.log(tenantId)

  const AddOrderData = (e) => {
    e.preventDefault()
    let tenantId = localStorage.getItem('tenantId')

    const input = {
      sampleField: sampleField,
      ResidentId: tenantId,
    }

    // api call for adding new tenant
    api
      .post(`/order/add`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        setOrderData([...orderData, result.data])

        toast.success('Added Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })

        // // adding history to database
        // const input2 = {
        //   title: `Added diagnosis about ${diagnosisName} of ${tenantName}`,
        //   tenantName: tenantName,
        //   tenantId: tenantId,
        //   userName: name,
        // }
        // api
        //   .post(`/history/create/`, input2, {
        //     headers: {
        //       Authorization: `Bearer ${token}`,
        //     },
        //   })
        //   .then((result) => {})
        //   .catch((err) => {
        //     console.error(err)
        //   })

        // delay function

        setTimeout(function () {
          setDialogClose(false)
        }, 2000)
      })
      .catch((err) => {
        console.log(err)
      })
  }

  return (
    <div>
      <Form id="form" onSubmit={(e) => AddOrderData(e)}>
        <Row>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              required
              value={sampleField}
              onChange={(e) => setSampleField(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label htmlFor="sportsCode">Sample Field</label>
          </Col>
          <Col md="12" className="d-flex justify-content-center">
            <div className="d-grid gap-2 col-6 mx-auto">
              <CButton type="submit" color="primary">
                Add
              </CButton>
            </div>
          </Col>
          <Col md="12" className="d-flex justify-content-center">
            <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
            />
          </Col>
        </Row>
      </Form>
    </div>
  )
}

export default AddOrderForm
