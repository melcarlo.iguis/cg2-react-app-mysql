import React, { useEffect, useState, useContext, useRef } from 'react'
import { Row, Col, Form, Button, Container } from 'react-bootstrap'
import AddIcon from '@mui/icons-material/Add'

// global variable
import AppContext from '../../../AppContext'

import moment from 'moment'
// axios api
import api from '../../../api/api'

// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function EditIncidentReportForm(data) {
  const {
    incidentReportList,
    setIncidentReportList,
    content,
    setContent,
    dialogClose,
    setDialogClose,
  } = useContext(AppContext)

  console.log(dialogClose)

  const [buttonIsEnable, setButtonIsEnable] = useState(true)

  // useState for all the input fields
  const [date, setDate] = useState('')
  const [time, setTime] = useState('')
  const [classification, setClassification] = useState('')
  const [location, setLocation] = useState('')
  const [incidentDesc, setIncidentDesc] = useState('')
  const [residentStatement, setResidentStatement] = useState('')
  const [witnesses, setWitnesses] = useState('')
  const [firstAid, setFirstAid] = useState('')
  const [firstAidNote, setFirstAidNote] = useState('')
  const [vitalsTaken, setVitalsTaken] = useState('')
  const [inPain, setInPain] = useState('')
  const [reqPhysician, setReqPhysician] = useState('')
  const [vitalsNote, setVitalsNote] = useState('')

  // Set all the initial value
  useEffect(() => {
    // to extract the date from datetime
    const d = new Date(data.data.date)
    console.log(d)
    const year = d.getFullYear()
    const month = d.getMonth() + 1
    const day = d.getDate()
    const dateString = '' + year + '-' + month + '-' + day

    const dateFormated = moment(dateString).format('YYYY-MM-DD')

    console.log(dateFormated)

    setDate(dateFormated)

    let hrs = d.getHours()
    let mins = d.getMinutes()

    const newMins = mins < 10 ? '0' + mins : mins

    const postTime = hrs + ':' + newMins
    setTime(postTime)

    setClassification(data.data.incidentClass)
    setLocation(data.data.location)
    setIncidentDesc(data.data.incidentDescription)
    setResidentStatement(data.data.tenantStatement)
    setWitnesses(data.data.witness)
    setFirstAid(data.data.firstAidGiven)
    setFirstAidNote(data.data.firstAidNote)
    setVitalsTaken(data.data.isVitalsTaken)
    setInPain(data.data.questionPainLvl)
    setReqPhysician(data.data.requestPhysician)
    setVitalsNote(data.data.vitalNote)
  }, [])

  useEffect(() => {
    if (
      date !== '' &&
      time !== '' &&
      classification !== '' &&
      location !== '' &&
      incidentDesc !== '' &&
      residentStatement !== '' &&
      witnesses !== '' &&
      firstAid !== '' &&
      firstAidNote !== '' &&
      vitalsTaken !== '' &&
      inPain !== '' &&
      reqPhysician !== '' &&
      vitalsNote
    ) {
      const timeoutId = setTimeout(() => updateIncidentReport(), 2000)
      return () => clearTimeout(timeoutId)
    }
  }, [
    date,
    time,
    classification,
    location,
    incidentDesc,
    residentStatement,
    witnesses,
    firstAid,
    firstAidNote,
    vitalsTaken,
    inPain,
    reqPhysician,
    vitalsNote,
  ])

  const firstUpdate = useRef(true)

  const updateIncidentReport = (e) => {
    // e.preventDefault()
    console.log('updating....')
    // setButtonIsEnable(false);

    const dateFormated = moment(date).format('YYYY-MM-DD')
    const dateTime = moment(dateFormated + ' ' + time)

    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    const input = {
      date: dateTime,
      tenantStatement: residentStatement,
      incidentClass: classification,
      location: location,
      tenantName: tenantName,
      incidentDescription: incidentDesc,
      witness: witnesses,
      firstAidGiven: firstAid,
      firstAidNote: firstAidNote,
      isVitalsTaken: vitalsTaken,
      questionPainLvl: inPain,
      requestPhysician: reqPhysician,
      vitalNote: vitalsNote,
    }

    if (firstUpdate.current) {
      firstUpdate.current = false
      return
    } else {
      // api call for adding new tenant
      api
        .put(`/incident/${data.data._id}/update`, input, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((result) => {
          console.log(result)

          // updating the College student on it's state to instantly apply the changes without refreshing the page
          incidentReportList.forEach((item) => {
            if (item._id === data.data._id) {
              item.date = dateTime
              item.tenantStatement = residentStatement
              item.incidentClass = classification
              item.location = location
              item.tenantName = tenantName
              item.incidentDescription = incidentDesc
              item.witness = witnesses
              item.firstAidGiven = firstAid
              item.firstAidNote = firstAidNote
              item.isVitalsTaken = vitalsTaken
              item.questionPainLvl = inPain
              item.requestPhysician = reqPhysician
              item.vitalNote = vitalsNote
            }
          })

          setIncidentReportList([...incidentReportList])

          // adding history to database
          const input2 = {
            title: `Edited ${tenantName}'s Incident Report data about ${incidentDesc}`,
            tenantName: tenantName,
            tenantId: tenantId,
            userName: name,
          }
          api
            .post(`/history/create/`, input2, {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
            .then((result) => {})
            .catch((err) => {
              console.error(err)
            })
        })
        .catch((err) => {
          console.log(err)
        })

      toast.success('Updated Successfully', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
      })
    }
  }

  return (
    <div>
      <Form id="form" onSubmit={(e) => updateIncidentReport(e)}>
        <Row>
          <Col md="12" id="subTitle">
            <h3 className="pt-2">General Information</h3>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem mt-3">
            <Form.Control
              type="date"
              name="date"
              required
              value={date}
              onChange={(e) => setDate(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label>Date</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem mt-3">
            <Form.Control
              type="time"
              name="time"
              value={time}
              onChange={(e) => setTime(e.target.value)}
              placeholder=" "
              required
              className="formItem mt-3 form__input"
            />
            <label>Time</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem mt-3">
            <select
              id="selectForm"
              required
              className="formItem "
              value={classification}
              onChange={(e) => setClassification(e.target.value)}
            >
              <option value="" disabled hidden selected>
                Select one
              </option>
              <option value="incident one">Incident one</option>
              <option value="incident two">Incident two</option>
              <option value="incident three">Incident three</option>
              <option value="incident four">Incident four</option>
              <option value="incident five">Incident five</option>
            </select>
            <label>Incident Classification</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem mt-3">
            <select
              id="selectForm"
              required
              className="formItem "
              value={location}
              onChange={(e) => setLocation(e.target.value)}
            >
              <option value="" disabled hidden selected>
                Select one
              </option>
              <option value="Location one">Location one</option>
              <option value="Location two">Location two</option>
              <option value="Location three">Location three</option>
              <option value="Location four">Location four</option>
              <option value="Location five">Location five</option>
            </select>
            <label>Location Where the Incident Occured</label>
          </Col>

          <Col md="10" sm="10" className="mx-auto mb-3 colItem">
            <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
              <Form.Label>Briefly Decribed What Occured</Form.Label>
              <Form.Control
                placeholder="ENTER INCIDENT DESCRIPTION HERE"
                required
                as="textarea"
                rows={3}
                value={incidentDesc}
                onChange={(e) => setIncidentDesc(e.target.value)}
              />
            </Form.Group>
          </Col>
          <Col md="10" sm="10" className="mx-auto mb-3 colItem">
            <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
              <Form.Label>Resident Statement</Form.Label>
              <Form.Control
                placeholder="ENTER RESIDENT STATEMENT HERE"
                required
                as="textarea"
                rows={3}
                value={residentStatement}
                onChange={(e) => setResidentStatement(e.target.value)}
              />
            </Form.Group>
          </Col>

          <Col md="12" id="subTitle">
            <h3 className="pt-2">Witnesses</h3>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem mt-3">
            <select
              id="selectForm"
              required
              className="formItem "
              value={witnesses}
              onChange={(e) => setWitnesses(e.target.value)}
            >
              <option value="" disabled hidden selected>
                Select one
              </option>
              <option value="No">No</option>
              <option value="Yes">Yes</option>
            </select>
            <label>Were there Witnesses?</label>
          </Col>

          <Col md="12" id="subTitle">
            <h3 className="pt-2">Response to Incident</h3>
          </Col>

          <Col md="6" sm="8" className="mx-auto mb-3 colItem mt-3">
            <select
              id="selectForm"
              required
              className="formItem "
              value={firstAid}
              onChange={(e) => setFirstAid(e.target.value)}
            >
              <option value="" disabled hidden selected>
                Select one
              </option>
              <option value="No">No</option>
              <option value="Yes">Yes</option>
            </select>
            <label>First Aid Given?</label>
          </Col>
          <Col md="10" sm="10" className="mx-auto mb-3 colItem">
            <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
              <Form.Control
                placeholder="NOTES"
                required
                as="textarea"
                rows={3}
                value={firstAidNote}
                onChange={(e) => setFirstAidNote(e.target.value)}
              />
            </Form.Group>
          </Col>

          <Col md="6" sm="8" className="mx-auto mb-3 colItem mt-3">
            <select
              id="selectForm"
              required
              className="formItem "
              value={vitalsTaken}
              onChange={(e) => setVitalsTaken(e.target.value)}
            >
              <option value="" disabled hidden selected>
                Select one
              </option>
              <option value="No">No</option>
              <option value="Yes">Yes</option>
            </select>
            <label>Vitals Taken?</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem mt-3">
            <select
              id="selectForm"
              required
              className="formItem "
              value={inPain}
              onChange={(e) => setInPain(e.target.value)}
            >
              <option value="" disabled hidden selected>
                Select one
              </option>
              <option value="No">No</option>
              <option value="Yes">Yes</option>
            </select>
            <label>Question About Pain Level?</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem mt-3">
            <select
              id="selectForm"
              required
              className="formItem "
              value={reqPhysician}
              onChange={(e) => setReqPhysician(e.target.value)}
            >
              <option value="" disabled hidden selected>
                Select one
              </option>
              <option value="No">No</option>
              <option value="Yes">Yes</option>
            </select>
            <label>Request to see Physician?</label>
          </Col>
          <Col md="10" sm="10" className="mx-auto mb-3 colItem">
            <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
              <Form.Control
                placeholder="NOTES"
                required
                as="textarea"
                rows={3}
                value={vitalsNote}
                onChange={(e) => setVitalsNote(e.target.value)}
              />
            </Form.Group>
          </Col>
          <Col md="12" className="d-flex justify-content-center">
            {/*{buttonIsEnable == true ?
			 			 <button id="actionBtn" type="submit">
				 			 <AddIcon />
			                  <div className="d-inline px-2"> FILE REPORT</div>
			 			
		                </button>
		                : <button disabled id="actionBtn" type="submit">
				 			 <AddIcon />
			                  <div className="d-inline px-2">FILE REPORT</div>
			 			
		                </button>

		                }*/}
            <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
            />
          </Col>
        </Row>
      </Form>
    </div>
  )
}

export default EditIncidentReportForm
