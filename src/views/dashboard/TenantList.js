import React, { useEffect, useState, useContext } from 'react'
import TextField from '@mui/material/TextField'
import Autocomplete from '@mui/material/Autocomplete'
import PageviewIcon from '@mui/icons-material/Pageview'
import { CContainer, CRow, CCol, CCardHeader, CCard, CCardBody } from '@coreui/react'
import { CardActionArea } from '@mui/material'
import {
  CButton,
  CCardFooter,
  CCardGroup,
  CCardImage,
  CCardLink,
  CCardSubtitle,
  CCardText,
  CCardTitle,
  CListGroup,
  CListGroupItem,
  CNav,
  CNavItem,
  CNavLink,
  CAccordion,
  CAccordionBody,
  CAccordionHeader,
  CAccordionItem,
} from '@coreui/react'
// import {
//   CButton,
//   CCard,
//   CCardBody,
//   CCardFooter,
//   CCardGroup,
//   CCardHeader,
//   CCardImage,
//   CCardLink,
//   CCardSubtitle,
//   CCardText,
//   CCardTitle,
//   CListGroup,
//   CListGroupItem,
//   CNav,
//   CNavItem,
//   CNavLink,
//   CCol,
//   CRow,
//   CAccordion,
//   CAccordionBody,
//   CAccordionHeader,
//   CAccordionItem,
// } from '@coreui/react'
import CIcon from '@coreui/icons-react'
import {
  cilBell,
  cilSpeedometer,
  cilEnvelopeOpen,
  cilList,
  cilMenu,
  cilSearch,
  cilHome,
  cilHeart,
  cilNotes,
  cilUser,
  cilPeople,
  cilWarning,
  cilPlus,
} from '@coreui/icons'
import { DocsCallout, DocsExample } from 'src/components'
import { useNavigate } from 'react-router-dom'
import { Row, Col, Container } from 'react-bootstrap'
import ReactImg from 'src/assets/images/react.jpg'
// importing axios
import api from '../../api/api'

// global variable
import AppContext from '../../AppContext'

import { styled } from '@mui/material/styles'
import Card from '@mui/material/Card'
import CardHeader from '@mui/material/CardHeader'
import CardMedia from '@mui/material/CardMedia'
import CardContent from '@mui/material/CardContent'
import CardActions from '@mui/material/CardActions'
import Collapse from '@mui/material/Collapse'
import Avatar from '@mui/material/Avatar'
import IconButton from '@mui/material/IconButton'
import Typography from '@mui/material/Typography'
import { red } from '@mui/material/colors'
import FavoriteIcon from '@mui/icons-material/Favorite'
import FolderSharedIcon from '@mui/icons-material/FolderShared'
import ShareIcon from '@mui/icons-material/Share'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import MoreVertIcon from '@mui/icons-material/MoreVert'
import AddBoxIcon from '@mui/icons-material/AddBox'
import PendingActionsIcon from '@mui/icons-material/PendingActions'
import PersonSearchIcon from '@mui/icons-material/PersonSearch'
import PersonIcon from '@mui/icons-material/Person'
import NoteAltIcon from '@mui/icons-material/NoteAlt'
import InsertChartIcon from '@mui/icons-material/InsertChart'
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft'
import DriveFileMoveIcon from '@mui/icons-material/DriveFileMove'
import ThumbUpAltIcon from '@mui/icons-material/ThumbUpAlt'
import LocalPharmacyIcon from '@mui/icons-material/LocalPharmacy'
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn'
import InventoryIcon from '@mui/icons-material/Inventory'
import VolunteerActivismIcon from '@mui/icons-material/VolunteerActivism'
import AssessmentIcon from '@mui/icons-material/Assessment'
import ContentPasteSearchIcon from '@mui/icons-material/ContentPasteSearch'
import FindInPageIcon from '@mui/icons-material/FindInPage'
import MedicationIcon from '@mui/icons-material/Medication'
import KeyboardDoubleArrowLeftIcon from '@mui/icons-material/KeyboardDoubleArrowLeft'
import KeyboardDoubleArrowRightIcon from '@mui/icons-material/KeyboardDoubleArrowRight'
import MenuBookIcon from '@mui/icons-material/MenuBook'
import WarningIcon from '@mui/icons-material/Warning'
import FeedIcon from '@mui/icons-material/Feed'
import VaccinesIcon from '@mui/icons-material/Vaccines'
import NotificationAction from './component/NotificationAction'
import PieChart from './component/PieChart'

const ExpandMore = styled((props) => {
  const { expand, ...other } = props
  return <IconButton {...other} />
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}))

function TenantList() {
  const [expanded, setExpanded] = React.useState(false)

  const handleExpandClick = () => {
    setExpanded(!expanded)
  }
  let navigate = useNavigate()
  const [roomList, setRoomList] = useState([])
  const { tenantList, setTenantList, actionValue, setActionValue } = useContext(AppContext)
  useEffect(() => {
    fetchTenant()
  }, [])

  const fetchTenant = async () => {
    let token = localStorage.getItem('token')
    await api
      .get('/residents/fetch', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        setTenantList(res.data)
      })
  }

  const getFullName = (data) => {
    return data.lastName + ',' + ' ' + data.firstName + ' ' + data.middleName
  }

  const cutBirthday = (string) => {
    let date = new Date(string)
    const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
    const day = date.getUTCDate()
    const year = date.getUTCFullYear()

    let fullDate = month + ' ' + day + ', ' + year
    return fullDate
  }

  const viewTenantDetails = (e, data) => {
    const tenantFullName = data.firstName + ' ' + data.middleName + ' ' + data.lastName
    e.preventDefault()
    localStorage.setItem('tenantId', data.id)
    localStorage.setItem('tenantName', tenantFullName)
    navigate(`/resident/${data.id}`)
  }

  const fetchRoomList = async () => {
    let token = localStorage.getItem('token')
    await api
      .get('/community/fetch', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setRoomList(res.data)
      })
  }

  useEffect(() => {
    fetchRoomList()
  }, [])

  const handleFilter = async (e, room) => {
    console.log(room)
    if (room === null) {
      fetchTenant()
    }
    const roomFilter = room.communityName
    let token = localStorage.getItem('token')
    await api
      .get('/residents/fetch', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setTenantList(res.data)

        console.log(tenantList)

        const newFilter = res.data.filter((value) => {
          return value.communityName === roomFilter
        })

        console.log(newFilter)
        setTenantList(newFilter)
      })
  }

  return (
    <>
      <Row>
        <Col lg={9} sm={12}>
          <CCard>
            <CCardHeader id="card-header">
              <strong id="res-title">Residents</strong>
              <div id="search-container">
                <Autocomplete
                  size="small"
                  onChange={(event, room) => {
                    handleFilter(event, room)
                    // setRoom(room.communityName)
                  }}
                  className="mt-2"
                  id="controllable-states-demo"
                  options={roomList}
                  getOptionLabel={(option) => `${option.communityName}`}
                  isOptionEqualToValue={(option, value) => option.id === value.id}
                  sx={{ width: 250 }}
                  renderInput={(params) => <TextField {...params} defaultValue="all" />}
                />
                <label>Community</label>
              </div>
            </CCardHeader>
            <CCardBody>
              <CRow xs={{ gutterY: 3 }}>
                {tenantList.length === 0 ? <h2 className="mx-auto">No Data Found</h2> : null}
                {tenantList.map((val, key) => {
                  return (
                    <CCol xl={3} md={4} key={key}>
                      <CCard
                        id="card-holder"
                        className="btn p-0 mx-auto"
                        md={{ width: '15rem' }}
                        style={{ width: '12rem', minHeight: '18rem' }}
                        onClick={(e) => viewTenantDetails(e, val)}
                      >
                        <div id="card-bgc">
                          <div id="tenant-image-holder">
                            <img id="tenant-image" src={val.picture} />
                          </div>
                        </div>
                        <CCardBody className="mt-5">
                          <CCardTitle>{getFullName(val)}</CCardTitle>
                          <div
                            style={{ position: 'absolute', bottom: '10px', left: '25%' }}
                            className="d-flex justify-content-center"
                          >
                            <small className="pr-2">{cutBirthday(val.birthday)}</small>
                          </div>
                        </CCardBody>
                      </CCard>
                    </CCol>
                  )
                })}
              </CRow>
            </CCardBody>
          </CCard>
        </Col>
        <Col lg={3} sm={12}>
          <div className="position-relative" style={{ height: '80vh', width: '390px' }}>
            <NotificationAction />
          </div>
        </Col>
        <Row>
          <Col lg={12}>
            <div id="container7" className="pb-3">
              <CContainer>
                <CRow xs={{ gutterY: 3 }}>
                  <CCol lg={3} md={6}>
                    <Card
                      id="card-holder"
                      className="mx-auto"
                      style={{ width: 'auto', height: '18rem' }}
                    >
                      <CardActionArea>
                        <CardContent className="position-relative">
                          <div id="card-txt-title">
                            {' '}
                            <Typography id="header-text" gutterBottom component="div"></Typography>
                          </div>
                          <Typography className="mt-5 pb-5" variant="body2" color="text.secondary">
                            <div className="pie-holder">
                              <PieChart />
                            </div>
                          </Typography>
                        </CardContent>
                      </CardActionArea>
                    </Card>
                  </CCol>
                  <CCol lg={3} md={6}>
                    <Card
                      id="card-holder"
                      className="mx-auto"
                      style={{ width: 'auto', height: '18rem' }}
                    >
                      <CardActionArea>
                        <CardContent className="position-relative">
                          <div id="card-txt-title">
                            {' '}
                            <Typography id="header-text" gutterBottom component="div">
                              Sample
                            </Typography>
                          </div>
                          <Typography className="mt-5 pb-5" variant="body2" color="text.secondary">
                            <div className="pie-holder"></div>
                          </Typography>
                        </CardContent>
                      </CardActionArea>
                    </Card>
                  </CCol>
                  <CCol lg={3} md={6}>
                    <Card
                      id="card-holder"
                      className="mx-auto"
                      style={{ width: 'auto', height: '18rem' }}
                    >
                      <CardActionArea>
                        <CardContent className="position-relative">
                          <div id="card-txt-title">
                            {' '}
                            <Typography id="header-text" gutterBottom component="div">
                              Sample
                            </Typography>
                          </div>
                          <Typography className="mt-5 pb-5" variant="body2" color="text.secondary">
                            <div className="pie-holder"></div>
                          </Typography>
                        </CardContent>
                      </CardActionArea>
                    </Card>
                  </CCol>
                  <CCol lg={3} md={6}>
                    <Card
                      id="card-holder"
                      className="mx-auto"
                      style={{ width: 'auto', height: '18rem' }}
                    >
                      <CardActionArea>
                        <CardContent className="position-relative">
                          <div id="card-txt-title">
                            {' '}
                            <Typography id="header-text" gutterBottom component="div">
                              Sample
                            </Typography>
                          </div>
                          <Typography className="mt-5 pb-5" variant="body2" color="text.secondary">
                            <div className="pie-holder"></div>
                          </Typography>
                        </CardContent>
                      </CardActionArea>
                    </Card>
                  </CCol>
                </CRow>
              </CContainer>
            </div>
          </Col>
        </Row>
      </Row>
    </>
  )
}

export default TenantList
