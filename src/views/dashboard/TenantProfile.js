import React, { useState, useEffect, useContext, useRef } from 'react'
import { Row, Col, Form, Table } from 'react-bootstrap'
import AssignmentLateIcon from '@mui/icons-material/AssignmentLate'
import FilterListIcon from '@mui/icons-material/FilterList'
import CheckCircleIcon from '@mui/icons-material/CheckCircle'
import InputAdornment from '@mui/material/InputAdornment'
import { Grid, Box, CardActionArea, CardActions } from '@mui/material'
import { styled } from '@mui/material/styles'
import { chromeTabsStylesHook } from '@mui-treasury/styles/tabs'
import {
  faBedPulse,
  faBurger,
  faClipboard,
  faFaceGrin,
  faHandDots,
  faHeartPulse,
  faPersonFalling,
  faUserDoctor,
  faBell,
  faTriangleExclamation,
  faCircleInfo,
  faPhone,
  faHospital,
  faHospitalUser,
  faCalendarCheck,
  faCalendarPlus,
  faCrutch,
  faPeopleGroup,
  faFilePdf,
  faCircleCheck,
  faListSquares,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Button from '@mui/material/Button'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardGroup,
  CCardImage,
  CCardLink,
  CCardSubtitle,
  CCardText,
  CCardTitle,
  CListGroup,
  CListGroupItem,
  CNav,
  CNavItem,
  CNavLink,
  CAccordion,
  CAccordionBody,
  CAccordionHeader,
  CAccordionItem,
} from '@coreui/react'
import Card from '@mui/material/Card'
import AddIcon from '@mui/icons-material/Add'
import PendingActionsIcon from '@mui/icons-material/PendingActions'
import AddBoxIcon from '@mui/icons-material/AddBox'
import TextSnippetIcon from '@mui/icons-material/TextSnippet'
import ChevronRightIcon from '@mui/icons-material/ChevronRight'
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight'
import FolderSharedIcon from '@mui/icons-material/FolderShared'
import CardContent from '@mui/material/CardContent'
import CardMedia from '@mui/material/CardMedia'
import { CContainer, CRow, CCol, CCardHeader } from '@coreui/react'
import SwitchUnstyled, { switchUnstyledClasses } from '@mui/base/SwitchUnstyled'
import TextField from '@mui/material/TextField'
import Autocomplete from '@mui/material/Autocomplete'
import Menu from '@mui/material/Menu'
import HomeIcon from '@mui/icons-material/Home'
import DateRangeIcon from '@mui/icons-material/DateRange'
import PersonSearchIcon from '@mui/icons-material/PersonSearch'
import RestaurantIcon from '@mui/icons-material/Restaurant'
import EditLocationAltIcon from '@mui/icons-material/EditLocationAlt'
import MenuItem from '@mui/material/MenuItem'
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state'
import NoteAltIcon from '@mui/icons-material/NoteAlt'
import InsertChartIcon from '@mui/icons-material/InsertChart'
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft'
import DriveFileMoveIcon from '@mui/icons-material/DriveFileMove'
import ThumbUpAltIcon from '@mui/icons-material/ThumbUpAlt'
import LocalPharmacyIcon from '@mui/icons-material/LocalPharmacy'
import PersonIcon from '@mui/icons-material/Person'
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn'
import InventoryIcon from '@mui/icons-material/Inventory'
import VolunteerActivismIcon from '@mui/icons-material/VolunteerActivism'
import AssessmentIcon from '@mui/icons-material/Assessment'
import ContentPasteSearchIcon from '@mui/icons-material/ContentPasteSearch'
import FindInPageIcon from '@mui/icons-material/FindInPage'
import KeyboardDoubleArrowLeftIcon from '@mui/icons-material/KeyboardDoubleArrowLeft'
import KeyboardDoubleArrowRightIcon from '@mui/icons-material/KeyboardDoubleArrowRight'
import MenuBookIcon from '@mui/icons-material/MenuBook'
import WarningIcon from '@mui/icons-material/Warning'
import FeedIcon from '@mui/icons-material/Feed'
import VaccinesIcon from '@mui/icons-material/Vaccines'
// facesheet code
import { useReactToPrint } from 'react-to-print'

// import FaceSheet  from './FaceSheet';

import InputLabel from '@mui/material/InputLabel'
import FormControl from '@mui/material/FormControl'
import NativeSelect from '@mui/material/NativeSelect'

// global variable
import AppContext from '../../AppContext'

import ListAltIcon from '@mui/icons-material/ListAlt'

// axios api
import api from '../../api/api'

// components to include
import Allergies from './table/Allergies'
import Dailylog from './table/Dailylog'
import Diagnosis from './table/Diagnosis'
import Dietary from './table/Dietary'
import Vitals from './table/Vitals'
import Behavior from './table/Behavior'
import InsuranceInfo from './table/InsuranceInfo'
import DisabilitiesTable from './table/DisabilitiesTable'

import Incident from './include/clinical/Incident'
// import Behavior from './include/clinical/Behavior'

import Vital from './include/clinical/Vital'

// components to include for profile tab
import BasicInfo from './include/profile/BasicInfo'
import Contact from './include/profile/Contact'
// tab panel
import PropTypes from 'prop-types'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'

import Popup from './popup/NotificationPopup'
import PopupAll from './popup/PopupAll'
import TabPopup from './popup/TabPopup'
import TablePopup from './popup/TablePopup'

import Typography from '@mui/material/Typography'

import { useNavigate } from 'react-router-dom'

import * as ReactBootstrap from 'react-bootstrap'
// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

// skeleton
import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'

import FormControlLabel from '@mui/material/FormControlLabel'
import MedicationIcon from '@mui/icons-material/Medication'
// content loader / skeleton
import ContentLoader from 'react-content-loader'
import Wounds from './include/clinical/Wounds'
import Emar from './include/clinical/Emar'
import Notes from './include/clinical/Notes'
import Orders from './include/clinical/Orders'
import Assessments from './include/clinical/Assessments'
import InsuranceInformation from './include/profile/InsuranceInformation'
import HealthCareDirectives from './include/profile/HealthCareDirectives'
import Routines from './include/profile/Routines'
import Disabilities from './include/profile/Disablities'
import Miscellaneous from './include/profile/Miscellaneous'
import DigitalDocuments from './include/profile/DigitalDocuments'
import RoutinesTable from './table/RoutinesTable'
import { display } from '@mui/system'
import EDocuments from './table/EDocuments'
import AddNotePopup from './popup/AddNotePopup'
import HealthCareDirectivesTable from './table/HealthCareDirectivesTable'
import IncidentReport from './table/IncidentReport'
const blue = {
  500: '#007FFF',
}

const grey = {
  400: '#BFC7CF',
  500: '#AAB4BE',
  600: '#6F7E8C',
}

const AntTabs = styled(Tabs)({
  borderRadius: '10px',
  backgroundColor: '#EEEEEE',
  '& .MuiTabs-indicator': {
    backgroundColor: '#1890ff',
  },
})

const AntTab = styled((props) => <Tab disableRipple {...props} />)(({ theme }) => ({
  textTransform: 'uppercase',
  minWidth: 0,
  height: '80%',
  borderRadius: '5px',
  margin: '5px',
  [theme.breakpoints.up('sm')]: {
    minWidth: 0,
    minHeight: 5,
  },
  fontWeight: theme.typography.fontWeightRegular,
  marginRight: theme.spacing(1),
  '&:hover': {
    color: '#40a9ff',
    opacity: 1,
  },
  '&.Mui-selected': {
    backgroundColor: '#FFFFFF',
    borderStyle: 'none',
    color: 'black',
    fontSize: '15px',
    fontWeight: '800',
  },
  '&.Mui-focusVisible': {
    backgroundColor: '#d1eaff',
  },
}))

function TenantProfile() {
  let x = localStorage.getItem('actionValue')
  let y = parseInt(x)
  const [anchorEl, setAnchorEl] = React.useState(null)
  const open = Boolean(anchorEl)
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = (val) => {
    setAnchorEl(null)
    console.log(val)
  }

  const helloWorld = () => {
    console.log('hehehe')
  }

  // code for scrolling
  const testRef = useRef(null)
  const scrollToElement = () => testRef.current.scrollIntoView()
  // tab panel
  function TabPanel(props) {
    const { children, value, index, ...other } = props

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    )
  }

  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
  }

  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    }
  }

  const [value, setValue] = React.useState(0)
  let popupIsOpen = localStorage.getItem('popupIsOpen')

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  const label = { componentsProps: { input: { 'aria-label': 'Demo switch' } } }
  let navigate = useNavigate()
  const componentRef = useRef()

  const [isReachTheEndData, setIsReachTheEndData] = React.useState(false)
  const [isReachTheStartData, setIsReachTheStartData] = React.useState(false)

  const [profileSubMenu, setProfileSubMenu] = React.useState('Basic Information')

  // usestate for loading
  const [loading, setLoading] = React.useState(false)

  const delayTimer = () => {
    const timer = setTimeout(() => {
      handlePrint()
    }, 500)
    return () => clearTimeout(timer)
  }
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  })
  const [tenantData, setTenantData] = React.useState({})
  const [currentTab, setCurrentTab] = React.useState('')
  const [age, setAge] = React.useState('')
  const {
    diagnosisData,
    setDiagnosisData,
    allergiesList,
    setAllergiesList,
    dialogClose,
    setDialogClose,
    tenantList,
    setTenantList,
    behaviorData,
    setBehaviorData,
    dailylogData,
    setDailylogData,
    dietaryData,
    setDietaryData,
    incidentReportList,
    setIncidentReportList,
    vitalData,
    setVitalData,
    isPrivacyOn,
    setIsPrivacyOn,
    notificationData,
    setNotificationData,
    activeTab,
    setActiveTab,
    isPopupOpen,
    setIsPopupOpen,
    popupChildren,
    setPopupChildren,
    insuranceInfoData,
    setInsuranceInfoData,
    disabilitiesData,
    setDisabilitiesData,
    routineDate,
    setRoutineData,
    popupTitle,
    setPopupTitle,
    notesData,
    setNotesData,
    eDocumentsData,
    setEDocumentsData,
    HealthCareDirectivesData,
    setHealthCareDirectivesData,
  } = useContext(AppContext)

  const [residentAllData, setResidentAllData] = React.useState([])
  const [statusInput, setStatusInput] = React.useState('')
  let name = localStorage.getItem('name')
  let token = localStorage.getItem('token')
  let tenantName = localStorage.getItem('tenantName')
  let tenantId = localStorage.getItem('tenantId')

  console.log(isPopupOpen)
  const firstUpdate = useRef(true)
  React.useEffect(() => {
    if (firstUpdate.current) {
      firstUpdate.current = false
      return
    } else {
      handleUpdateStatus()
    }
  }, [])
  // }, [statusInput])

  // function for fetching insurance info
  const fetchInsuranceInfo = async () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')
    const res = await api.get(`/insuranceinfo/${tenantId}/fetch`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    console.log(res)
    return res.data
  }

  const fetchRoutine = async () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')
    const res = await api.get(`/routine/${tenantId}/fetch`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    console.log(res)
    return res.data
  }

  // function for fetching insurance info
  const fetchDisabilities = async () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')
    const res = await api.get(`/disabilities/${tenantId}/fetch`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    console.log(res)
    return res.data
  }

  useEffect(() => {
    const getData = async () => {
      const allData = await fetchInsuranceInfo()
      if (allData) setInsuranceInfoData(allData)
    }

    const getDisabilitiesData = async () => {
      const allData = await fetchDisabilities()
      if (allData) setDisabilitiesData(allData)
    }

    const getRoutineData = async () => {
      const allData = await fetchRoutine()
      if (allData) setRoutineData(allData)
    }

    getDisabilitiesData()
    getData()
    getRoutineData()
  }, [tenantId])

  // function for updating tenant's status
  const handleUpdateStatus = () => {
    console.log('changing status. .. .')
    api
      .put(
        `residents/${tenantId}/changeStatus`,
        { status: statusInput },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then((result) => {
        // tenantData.forEach(item => {
        //      if(item._id === tenantId){
        //      item.status = statusInput
        //      }
        //    })

        // setting the new data to the state
        setTenantData({ ...tenantData, status: statusInput })

        toast.success('Updated Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
        // adding history to database
        const input2 = {
          title: `Edited ${tenantName}'s status to ${statusInput}`,
          tenantName: tenantName,
          tenantId: tenantId,
          userName: name,
        }
        api
          .post(`/history/create/`, input2, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((result) => {
            console.log(result)
          })
          .catch((err) => {
            console.error(err)
          })

        // delay function

        setTimeout(function () {
          setDialogClose(false)
        }, 2000)
      })
      .catch((err) => {
        console.error(err)
      })
  }

  const [days, setDays] = React.useState(0)

  const fetchNotification = async () => {
    await api
      .get(`/notification/${tenantId}/fetchAll`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        const sortedNotification = res.data.sort((a, b) => b.priorityLevel - a.priorityLevel)

        // code to compute days between two dates
        // let date1 = new Date(res.data[0].date)
        // let date2 = new Date()

        // let diffInTime = date2.getTime() - date1.getTime()

        // let daysBetween = diffInTime / (1000 * 3600 * 24)
        // setDays(daysBetween)

        setNotificationData(sortedNotification)
        // setHistoryList(res.data)
      })
  }

  // }, [tenantId])

  let fullName = tenantData.firstName + ' ' + tenantData.middleName + ' ' + tenantData.lastName

  React.useEffect(() => {
    let isMounted = true
    setCurrentTab('profile')
    return () => {
      isMounted = false
    }
    // localStorage.setItem('tenantId' , tenantData._id);
    // localStorage.setItem('tenantName' , fullName);
  }, [])
  // code for fetching allergy

  const cutBirthday = (string) => {
    let date = new Date(string)
    const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
    const day = date.getUTCDate()
    const year = date.getUTCFullYear()

    let fullDate = month + ' ' + day + ', ' + year
    return fullDate
  }

  const getTheAge = (birthday) => {
    let date = new Date(birthday)
    let currentYear = new Date().getFullYear()
    let year = date.getUTCFullYear()

    const age = currentYear - year

    return age
  }

  // function for fetchning the name of the tenant and setting it to the localstorage
  const setTenantName = (id) => {
    api
      .get(`/residents/${id}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        let fullName = res.data.firstName + ' ' + res.data.middleName + ' ' + res.data.lastName

        localStorage.setItem('tenantName', fullName)
      })
  }

  React.useEffect(() => {
    fetchBehaviorData()
  }, [tenantId])

  const getFullName = (data) => {
    return data.lastName + ',' + ' ' + data.firstName + ' ' + data.middleName
  }

  const fetchBehaviorData = () => {
    let token = localStorage.getItem('token')
    api
      .get(`/behavior/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        const newFilter = res.data.filter((val) => {
          return val.isActive === true
        })
        setBehaviorData(newFilter)
      })
  }

  const fetchAllTenant = async () => {
    let token = localStorage.getItem('token')
    const res = await api.get('/residents/fetch', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })

    return res.data
  }

  // fetching of data from APO

  const fetchTenant = async () => {
    let tenantId = localStorage.getItem('tenantId')
    let token = localStorage.getItem('token')
    const res = await api.get(`/residents/${tenantId}/fetch`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    return res.data
  }

  const fetchDiagnosisData = async () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')
    const res = await api.get(`/diagnosis/${tenantId}/fetch`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    return res.data
  }

  const fetchAllergy = async () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')
    const res = await api.get(`/allergy/${tenantId}/fetch`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    console.log(res.data)
    return res.data
  }

  const fetchResidentData = async () => {
    let token = localStorage.getItem('token')
    const res = await api.get(`/residents/${tenantId}/fetchAllData`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })

    console.log(res.data)
    return res.data
  }

  React.useEffect(() => {
    // to call the function to fetch data
    const getTenantData = async () => {
      const tenantDetails = await fetchTenant()
      if (tenantDetails) setTenantData(tenantDetails)
    }
    getTenantData()

    // to call the function to fetch data
    const getDiagnosisData = async () => {
      const allDiagnosis = await fetchDiagnosisData()
      if (allDiagnosis) setDiagnosisData(allDiagnosis)
    }
    getDiagnosisData()

    // to call the function to fetch data
    const getAllergyList = async () => {
      const allAlergy = await fetchAllergy()
      if (allAlergy) setAllergiesList(allAlergy)
    }

    getAllergyList()

    // to call the function to fetch data
    const getAllResidentData = async () => {
      const allResidentData = await fetchResidentData()
      if (allResidentData) {
        setResidentAllData(allResidentData)
        setNotesData(allResidentData.ProgressNotes)
        setHealthCareDirectivesData(allResidentData.HealthCareDirectives)
        setEDocumentsData(allResidentData.EDocuments)
      }
    }

    getAllResidentData()
    const getAllTenant = async () => {
      const allTenant = await fetchAllTenant()
      if (allTenant) setTenantList(allTenant)
    }
    getAllTenant()
    fetchNotification()
  }, [tenantId])

  console.log(residentAllData)
  const changeCurrentTab = (profileVal, subMenu) => {
    setCurrentTab('profile')
    // setProfileTab(profileVal)
    setProfileSubMenu(subMenu)
  }

  let tenantCurrentIndex = localStorage.getItem('tenantCurrentIndex')
  React.useEffect(() => {
    const search = (obj) => obj.id == tenantId
    const index = tenantList.findIndex(search)
    localStorage.setItem('tenantCurrentIndex', index)
    if (index == tenantList.length - 1) {
      setIsReachTheEndData(true)
    } else {
      setIsReachTheEndData(false)
    }

    if (index == 0) {
      setIsReachTheStartData(true)
    } else {
      setIsReachTheStartData(false)
    }
  }, [tenantCurrentIndex])
  // function for next and previous button
  const nextButtonFunction = () => {
    const search = (obj) => obj.id == tenantId
    setLoading(false)
    const index = tenantList.findIndex(search)
    localStorage.setItem('tenantCurrentIndex', index + 1)
    const newIndex = tenantList[index + 1].id
    setTenantName(newIndex)
    // set new id in the localstorage
    localStorage.setItem('tenantId', newIndex)
    navigate(`/resident/${newIndex}`)
    fetchTenant(newIndex)
    fetchAllergy()
  }

  const prevButtonFunction = () => {
    const search = (obj) => obj.id == tenantId
    setLoading(false)
    const index = tenantList.findIndex(search)

    localStorage.setItem('tenantCurrentIndex', index - 1)
    const newIndex = tenantList[index - 1].id
    setTenantName(newIndex)
    // set new id in the localstorage
    localStorage.setItem('tenantId', newIndex)
    navigate(`/resident/${newIndex}`)
    fetchTenant(newIndex)
    fetchAllergy()
  }

  const formatDate = (string) => {
    if (string === undefined || string == 'No Available Data') {
      return 'No Available Data'
    } else {
      let date = new Date(string)
      const month = date.toLocaleString('en-us', { month: 'long' })
      const day = date.getUTCDate()
      const year = date.getUTCFullYear()

      let fullDate = month + ' ' + day + ', ' + year
      return fullDate
    }
  }

  const fetchDailylogData = () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    api
      .get(`/dailylog/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setDailylogData(res.data)
      })
  }

  console.count('Render:')
  console.log(isPopupOpen)

  React.useEffect(() => {
    console.log('hello world')
    fetchDailylogData()
  }, [tenantId])

  const fetchDietaryData = async () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    await api
      .get(`/dietary/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res.data)
        setDietaryData(res.data)
      })
  }

  React.useEffect(() => {
    fetchDietaryData()
  }, [tenantId])

  const fetchIncidentReport = () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    api
      .get(`/incident/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setIncidentReportList(res.data)
      })
  }

  React.useEffect(() => {
    fetchIncidentReport()
  }, [tenantId])

  console.log(vitalData)

  const changeActiveTab = (activeTab) => {
    setActiveTab(activeTab)
    // scrollToElement()
  }

  const resetActiveTab = () => {
    setActiveTab(0)
  }

  const scrollDown = () => {
    window.scrollTo({
      top: 1000,
      behavior: 'smooth',
    })
  }

  console.log(tenantData)

  // function for setting the value of notification id to be fetch
  const setNotificationId = (id) => {
    localStorage.setItem('notificationId', id)
  }

  // const retireveAllHighschool = async () =>{
  //   const res = await api.get('/highschool/fetch');
  //   return res.data
  // }

  // useEffect(()=>{
  //   const getAllHighSChool = async () => {
  //     const allHighschool = await retireveAllHighschool();
  //     if(allHighschool) setHighschool(allHighschool)

  //   }

  //  getAllHighSChool();
  // }, [])
  const fetchVitalData = () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    api
      .get(`/vitals/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setVitalData(res.data)
      })
  }

  React.useEffect(() => {
    fetchVitalData()
  }, [tenantId])

  const filteredNotifData = async (type) => {
    if (type.label == '') {
      fetchNotification()
    } else {
      await api
        .get(`/notification/${tenantId}/fetchAll`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          console.log(res)
          const newFilter = res.data.filter((val) => {
            return val.type == type.label
          })
          const sortedNotification = newFilter.sort((a, b) => b.priorityLevel - a.priorityLevel)

          setNotificationData(sortedNotification)
          // setHistoryList(res.data)
        })
    }
  }

  let notifType = [{ label: 'Vitals' }, { label: 'Medication' }]

  // function to count the pending assessment
  const countPendingAssessment = (data) => {
    const newFilter = data.filter((val) => {
      return val.status === false
    })

    return newFilter
  }

  const countCompletedAssessment = (data) => {
    const newFilter = data.filter((val) => {
      return val.status === true
    })

    return newFilter
  }

  const getLatestPending = (data) => {
    if (data.length == 0) {
      return 'No Available Data'
    } else {
      const newFilter = data.filter((val) => {
        return val.status === false
      })

      if (newFilter.length > 0) {
        return newFilter[newFilter.length - 1].type
      } else {
        return 'No Available Data'
      }
    }
  }

  const getReviewData = (data) => {
    if (data.length == 0) {
      return 'No Available Data'
    } else {
      const newFilter = data.filter((val) => {
        return val.status === false
      })

      if (newFilter.length > 0) {
        return newFilter[newFilter.length - 1].reviewDate
      } else {
        return 'No Available Data'
      }
    }
  }

  const countPendingCarePlan = (data) => {
    const newFilter = data.filter((val) => {
      return val.status === true
    })

    return newFilter
  }

  const countingCompleteCareplan = (data) => {
    const newFilter = data.filter((val) => {
      return val.status === false
    })

    return newFilter
  }

  const getCareplanLatestData = (data) => {
    if (data.length == 0) {
      return 'No Available Data'
    } else {
      const newFilter = data.filter((val) => {
        return val.status === true
      })

      if (newFilter.length > 0) {
        return (
          newFilter[newFilter.length - 1].medName +
          ' ' +
          newFilter[newFilter.length - 1].medStrength
        )
      } else {
        return 'No Available Data'
      }
    }
  }

  console.log(residentAllData)

  return (
    <div>
      <TabPopup />
      <div id="add-note-holder">
        <div className="logo">
          <AddNotePopup />
        </div>
      </div>
      {isPrivacyOn == false ? (
        <div>
          <div
            onClick={() => nextButtonFunction()}
            id={isReachTheEndData ? 'hide-element' : 'next-btn'}
          >
            <KeyboardDoubleArrowRightIcon id="next-icon" />
          </div>
          <div
            onClick={() => prevButtonFunction()}
            id={isReachTheStartData ? 'hide-element' : 'prev-btn'}
          >
            <KeyboardDoubleArrowLeftIcon id="prev-icon" />
          </div>
        </div>
      ) : null}

      {isPrivacyOn == false ? (
        <>
          <Grid md={12} container rowSpacing={2} columnSpacing={1}>
            <Grid
              item
              md={9}
              sm={12}
              xs={12}
              direction="column"
              style={{
                display: 'flex',
                marginBottom: '0px',
                paddingTop: '0px',
                justifyContent: 'center',
                alignItems: 'center',
                // height: '100%',
              }}
            >
              <Grid md={12} container rowSpacing={2} columnSpacing={{ xs: 2, sm: 2, md: 2 }}>
                <Grid
                  item
                  md={12}
                  sm={12}
                  xs={12}
                  className="mt-4"
                  direction="column"
                  id="container5"
                >
                  <div id="container5-content" className="position-relative">
                    <Grid container rowSpacing={2}>
                      <Grid item md={3} sm={12} xs={12} direction="column">
                        <div id="card-bgc2">
                          <div id="tenant-image-holder2">
                            <img id="tenant-image" src={tenantData.picture} />
                          </div>
                        </div>
                      </Grid>
                      <Grid id="tenant-profile-data" item md={5} sm={12} xs={12} direction="column">
                        <div>
                          <div>
                            <h4
                              className="name"
                              style={{
                                padding: '0px',
                                margin: '0px',
                              }}
                            >
                              {tenantName}
                            </h4>
                          </div>
                          <div>
                            <p className="body-text room">{tenantData.communityName}</p>
                          </div>
                          <div>
                            <p className="body-text carelevel">
                              Care Level : {tenantData.careLevel}
                            </p>
                          </div>
                        </div>
                        <p style={{ fontWeight: '700' }}>Allergies</p>
                        <div id="list-item-holder" className="mx-auto">
                          {allergiesList.length === 0 ? (
                            <strong className="d-inline-block pl-2"> No Allergy Data yet</strong>
                          ) : null}
                          <ul className="d-flex">
                            {allergiesList.map((item, key) => (
                              <p className="body-text"> • {item.allergy}</p>
                            ))}
                          </ul>
                        </div>{' '}
                        <div id="list-item-holder" className=" mx-auto">
                          {diagnosisData.length === 0 ? null : (
                            <p style={{ fontWeight: '700' }}>Diagnosis</p>
                          )}
                          <ul className="d-block">
                            {diagnosisData.map((item, key) => (
                              <p className="body-text"> • {item.diagnosisName}</p>
                            ))}
                          </ul>
                        </div>
                      </Grid>
                      <Grid
                        className="mt-2 mb-3"
                        item
                        md={3}
                        sm={12}
                        xs={12}
                        direction="column"
                        style={{
                          marginLeft: '15px',
                        }}
                      >
                        <div className="btn mt-2 position-relative">
                          Status : {tenantData.status}
                          <PopupAll title="Change Status">
                            <div id="status-holder" style={{ width: '250px', padding: '0px' }}>
                              {' '}
                              Status: {tenantData.status}
                            </div>
                            <Box id="drop-Drown2" sx={{ minWidth: 150 }}>
                              <FormControl>
                                <InputLabel
                                  variant="standard"
                                  htmlFor="uncontrolled-native"
                                ></InputLabel>
                                <NativeSelect
                                  value={statusInput}
                                  onChange={(e) => setStatusInput(e.target.value)}
                                  style={{ fontSize: '18px', width: '100%' }}
                                >
                                  <option value="Cancel Move in">Cancel Move in</option>
                                  <option value="Move Out">Move Out</option>
                                  <option value="On Notice">On Notice</option>
                                  <option value="Start LOA">Start LOA</option>
                                  <option value="Put On Waitlist">Put On Waitlist</option>
                                  <option value="Unit/Care Level Change">
                                    Unit/Care Level Change
                                  </option>
                                  <option value="Schedule Unit Transfer">
                                    Schedule Unit Transfer
                                  </option>
                                </NativeSelect>
                              </FormControl>
                            </Box>
                          </PopupAll>
                          {/* <Box id="drop-Drown2" sx={{ minWidth: 150 }}>
                            <FormControl>
                              <InputLabel variant="standard" htmlFor="uncontrolled-native">
                                <div>
                                  <strong style={{ fontSize: '13px', width: '100%' }}>
                                    Status : {tenantData.status}
                                    <PopupAll>yowww</PopupAll>
                                  </strong>
                                </div>
                              </InputLabel>
                              <NativeSelect
                                value={statusInput}
                                onChange={(e) => setStatusInput(e.target.value)}
                                style={{ fontSize: '12px', width: '100%' }}
                              >
                                <option value="Cancel Move in">Cancel Move in</option>
                                <option value="Move Out">Move Out</option>
                                <option value="On Notice">On Notice</option>
                                <option value="Start LOA">Start LOA</option>
                                <option value="Put On Waitlist">Put On Waitlist</option>
                                <option value="Unit/Care Level Change">
                                  Unit/Care Level Change
                                </option>
                                <option value="Schedule Unit Transfer">
                                  Schedule Unit Transfer
                                </option>
                              </NativeSelect>
                            </FormControl>
                          </Box> */}
                        </div>
                      </Grid>
                    </Grid>
                  </div>
                </Grid>
              </Grid>
            </Grid>
            <Grid
              id="conainter2-holder"
              item
              md={3}
              sm={12}
              xs={12}
              direction="column"
              style={{
                paddingTop: '0px',
                width: '100%',
                position: 'relative',
              }}
            >
              {' '}
              <div
                className={
                  isPopupOpen
                    ? 'notification-holer-tenant-profile disabled-pointer-event'
                    : 'notification-holer-tenant-profile'
                }
              >
                <div id="container2">
                  <div>
                    <strong
                      style={{
                        fontSize: '20px',
                        marginLeft: '20px',
                      }}
                    >
                      Notification/Action
                    </strong>
                    {/* Insert notification data here */}
                    <div>
                      {/* <Autocomplete
                        disablePortal
                        size="small"
                        className="mx-auto"
                        options={notifType}
                        sx={{ width: 150, marginLeft: '20px' }}
                        onChange={(e, value) => {
                          filteredNotifData(value)
                        }}
                        renderInput={(params) => <TextField variant="standard" {...params} />}
                      /> */}

                      {notificationData.length == 0 ? (
                        <strong className="d-inline-block pl-2">
                          {' '}
                          No Notification Data is Available
                        </strong>
                      ) : null}
                      {notificationData.map((item, key) => (
                        <div
                          key={key}
                          id="notification-holder"
                          className={item.isCompleted == false ? '' : 'disabled-btn'}
                          onClick={(e) => setNotificationId(item.id)}
                        >
                          <Popup data={item} />

                          {item.isCompleted == false ? (
                            <p className="text-right my-2">
                              {' '}
                              <FontAwesomeIcon
                                icon={faTriangleExclamation}
                                style={{ fontSize: '18px', color: '#f9b115' }}
                              />{' '}
                              This needs an attention.
                            </p>
                          ) : (
                            <p className="text-right my-2">
                              {' '}
                              <FontAwesomeIcon
                                icon={faCircleCheck}
                                style={{ fontSize: '18px', color: '#07BC0C' }}
                              />{' '}
                              Done.
                            </p>
                          )}
                          <p className="body-text d-inline-block"> {item.title}</p>
                          {item.isCompleted == false ? (
                            <p className="body-text d-inline-block pb-2">
                              {' '}
                              {formatDate(item.date)}
                            </p>
                          ) : (
                            <>
                              <div className="body-text d-inline-block">value: {item.value}</div>
                              <p className="body-text d-inline-block pb-2">
                                {' '}
                                Date Acoomplish:
                                {formatDate(item.dateAccomplish)}
                              </p>
                            </>
                          )}
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            </Grid>
          </Grid>

          <div id="container" className="position-relative pb-5 mt-5">
            <Box sx={{ width: '100%' }}>
              <Box className="position-relative mx-auto pt-3" sx={{ width: '95%' }}>
                <AntTabs
                  variant="scrollable"
                  scrollButtons="false"
                  aria-label="scrollable auto tabs example"
                  value={value}
                  onChange={handleChange}
                  // aria-label="basic tabs example"
                >
                  <AntTab style={{ fontSize: '10px' }} label="Profile" {...a11yProps(0)} />
                  <AntTab
                    style={{ fontSize: '10px' }}
                    label="Clinical"
                    {...a11yProps(1)}
                    onClick={(e) => resetActiveTab()}
                  />
                  {/* <AntTab
                    style={{ fontSize: '10px' }}
                    label="Reports"
                    {...a11yProps(2)}
                    onClick={(e) => resetActiveTab()}
                  /> */}
                  <AntTab
                    style={{ fontSize: '10px' }}
                    label="e-Documents"
                    {...a11yProps(3)}
                    onClick={(e) => resetActiveTab()}
                  />
                </AntTabs>
              </Box>
              <Box
                className="mx-auto"
                sx={{ borderBottom: 1, borderColor: 'divider', width: '95%' }}
              ></Box>
              <TabPanel value={value} index={1}>
                {/* cards code start here */}
                <CContainer>
                  <CRow xs={{ gutterY: 3 }}>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="position-relative"
                        style={{
                          width: 'auto',
                          height: '16rem',
                          zIndex: '1',
                        }}
                      >
                        <div id="stats-holder">
                          {allergiesList.length > 0 ? (
                            <>
                              <div className="active-allergy">
                                <p>{allergiesList.length}</p>
                              </div>
                            </>
                          ) : (
                            <div className="nodata warning-ico">
                              <AssignmentLateIcon style={{ fontSize: '30px' }} />
                            </div>
                          )}
                        </div>
                        <CardActionArea>
                          <PopupAll title="Allergies">
                            <Allergies activeTabVal="1" />
                          </PopupAll>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Allergies
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {allergiesList.length > 0 ? (
                                <>
                                  {' '}
                                  <p> {allergiesList[allergiesList.length - 1].allergy}</p>
                                  <small style={{ fontStyle: 'italic' }}>
                                    Latest allergy data of the resident
                                  </small>
                                  <p>
                                    Started Date:{' '}
                                    {formatDate(allergiesList[allergiesList.length - 1].startDate)}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto position-relative"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <div id="stats-holder">
                          {behaviorData.length > 0 ? (
                            <>
                              <div className="active-allergy">
                                <p>{behaviorData.length}</p>
                              </div>
                            </>
                          ) : (
                            <div className="nodata warning-ico">
                              <AssignmentLateIcon style={{ fontSize: '30px' }} />
                            </div>
                          )}
                        </div>
                        <CardActionArea onClick={() => changeActiveTab(2)}>
                          <PopupAll title="Behavior">
                            <Behavior />
                          </PopupAll>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Behavior
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {behaviorData.length > 0 ? (
                                <>
                                  {' '}
                                  <p> {behaviorData[behaviorData.length - 1].behavior}</p>
                                  <small style={{ fontStyle: 'italic' }}>
                                    Latest behavior data of the resident
                                  </small>
                                  <p>
                                    Started Date:{' '}
                                    {formatDate(behaviorData[behaviorData.length - 1].startDate)}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    {/* <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto position-relative"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <div id="stats-holder">
                          {dailylogData.length > 0 ? (
                            <>
                              <div className="active-allergy">
                                <p>{dailylogData.length}</p>
                              </div>
                            </>
                          ) : (
                            <div className="nodata warning-ico">
                              <AssignmentLateIcon style={{ fontSize: '30px' }} />
                            </div>
                          )}
                        </div>
                        <CardActionArea onClick={() => changeActiveTab(3)}>
                          <PopupAll title="Daily log">
                            <Dailylog />
                          </PopupAll>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Daily log
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {dailylogData.length > 0 ? (
                                <>
                                  {' '}
                                  <p> {dailylogData[dailylogData.length - 1].title}</p>
                                  <p>{dailylogData[dailylogData.length - 1].note}</p>
                                  <p>
                                    Created by: {dailylogData[dailylogData.length - 1].userName}
                                  </p>
                                  <small style={{ fontStyle: 'italic' }}>
                                    Latest log data of the resident
                                  </small>
                                  <p>
                                    Issued Date:{' '}
                                    {formatDate(dailylogData[dailylogData.length - 1].date)}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol> */}
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto position-relative"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <div id="stats-holder">
                          {diagnosisData.length > 0 ? (
                            <>
                              <div className="active-allergy">
                                <p>{diagnosisData.length}</p>
                              </div>
                            </>
                          ) : (
                            <div className="nodata warning-ico">
                              <AssignmentLateIcon style={{ fontSize: '30px' }} />
                            </div>
                          )}
                        </div>
                        <CardActionArea onClick={() => changeActiveTab(4)}>
                          <PopupAll title="Diagnosis">
                            <Diagnosis />
                          </PopupAll>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Diagnosis
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {diagnosisData.length > 0 ? (
                                <>
                                  {' '}
                                  <p> {diagnosisData[diagnosisData.length - 1].diagnosisName}</p>
                                  <small style={{ fontStyle: 'italic' }}>
                                    Latest diagnosis data of the resident
                                  </small>
                                  <p>
                                    Started Date:{' '}
                                    {formatDate(diagnosisData[diagnosisData.length - 1].startDate)}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto position-relative"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <div id="stats-holder">
                          {dietaryData.length > 0 ? (
                            <>
                              <div className="active-allergy">
                                <p>{dietaryData.length}</p>
                              </div>
                            </>
                          ) : (
                            <div className="nodata warning-ico">
                              <AssignmentLateIcon style={{ fontSize: '30px' }} />
                            </div>
                          )}
                        </div>
                        <CardActionArea onClick={() => changeActiveTab(7)}>
                          <PopupAll title="Dietary">
                            <Dietary />
                          </PopupAll>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Dietary
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {dietaryData.length > 0 ? (
                                <>
                                  {' '}
                                  <p> {dietaryData[dietaryData.length - 1].dietaryPreference}</p>
                                  <small style={{ fontStyle: 'italic' }}>
                                    Latest dietary data of the resident
                                  </small>
                                  <p>
                                    Started Date:{' '}
                                    {formatDate(dietaryData[dietaryData.length - 1].startDate)}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto position-relative"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <div id="stats-holder">
                          {incidentReportList.length > 0 ? (
                            <>
                              <div className="active-allergy">
                                <p>{incidentReportList.length}</p>
                              </div>
                            </>
                          ) : (
                            <div className="nodata warning-ico">
                              <AssignmentLateIcon style={{ fontSize: '30px' }} />
                            </div>
                          )}
                        </div>
                        <CardActionArea onClick={() => changeActiveTab(5)}>
                          <PopupAll title="Dietary">
                            <IncidentReport />
                          </PopupAll>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Incident
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {incidentReportList.length > 0 ? (
                                <>
                                  {' '}
                                  <p>
                                    {' '}
                                    {
                                      incidentReportList[incidentReportList.length - 1]
                                        .incidentDescription
                                    }
                                  </p>
                                  <p>
                                    Location:{' '}
                                    {incidentReportList[incidentReportList.length - 1].location}
                                  </p>
                                  <small style={{ fontStyle: 'italic' }}>
                                    Latest incident data of the resident
                                  </small>
                                  <p>
                                    Date Happened:{' '}
                                    {formatDate(
                                      incidentReportList[incidentReportList.length - 1].date,
                                    )}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto position-relative"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <div id="stats-holder">
                          {vitalData.length > 0 ? (
                            <>
                              <div className="active-allergy">
                                <p>{vitalData.length}</p>
                              </div>
                            </>
                          ) : (
                            <div className="nodata warning-ico">
                              <AssignmentLateIcon style={{ fontSize: '30px' }} />
                            </div>
                          )}
                        </div>
                        <CardActionArea onClick={() => changeActiveTab(6)}>
                          <PopupAll title="Vitals">
                            <Vitals />
                          </PopupAll>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Vitals
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {vitalData.length > 0 ? (
                                <>
                                  {' '}
                                  <p>
                                    Blood Pressure: {vitalData[vitalData.length - 1].systolic}/
                                    {vitalData[vitalData.length - 1].diastolic}
                                  </p>
                                  <p>
                                    Body Temperature: {vitalData[vitalData.length - 1].bodyTemp}
                                  </p>
                                  <p>Pulse Rate: {vitalData[vitalData.length - 1].pulseRate}</p>
                                  <p>
                                    Respiration Rate:{' '}
                                    {vitalData[vitalData.length - 1].respirationRate}
                                  </p>
                                  <small style={{ fontStyle: 'italic' }}>
                                    Latest Vital data of the resident
                                  </small>
                                  <p>
                                    Date Input: {formatDate(vitalData[vitalData.length - 1].date)}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea
                          onClick={() => {
                            // changeActiveTab(9)
                            setIsPopupOpen(true)
                            setPopupChildren('wounds')
                          }}
                        >
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Wounds
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {residentAllData.Wounds != 0 || residentAllData.Wounds != null ? (
                                <>
                                  <div id="wound-img-snapchat">
                                    {residentAllData.Wounds != null &&
                                    residentAllData.Wounds[residentAllData.Wounds.length - 1] !=
                                      null ? (
                                      <>
                                        <img
                                          src={
                                            residentAllData.Wounds[
                                              residentAllData.Wounds.length - 1
                                            ].picture
                                          }
                                          alt="sample"
                                        />
                                        <p>
                                          Date Discovered:{' '}
                                          {formatDate(
                                            residentAllData.Wounds[
                                              residentAllData.Wounds.length - 1
                                            ].dateDiscover,
                                          )}{' '}
                                        </p>
                                      </>
                                    ) : (
                                      'No Available Data'
                                    )}
                                  </div>
                                </>
                              ) : (
                                <p>No woun Data Found</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea
                          onClick={() => {
                            // changeActiveTab(10)
                            setIsPopupOpen(true)
                            setPopupChildren('note')
                          }}
                        >
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Progress Note
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {notesData.length > 0 ? (
                                <>
                                  <div id="stats-holder">
                                    <div className="active-allergy">
                                      <p>{notesData.length}</p>
                                    </div>
                                  </div>
                                </>
                              ) : (
                                <div className="nodata warning-ico">
                                  <AssignmentLateIcon style={{ fontSize: '30px' }} />
                                </div>
                              )}
                              {notesData.length > 0 ? (
                                <>
                                  <p>Description: {notesData[notesData.length - 1].description} </p>
                                  <p>
                                    Review date: {formatDate(notesData[notesData.length - 1].date)}{' '}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto position-relative"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        {' '}
                        {residentAllData.length != 0 ? (
                          <>
                            <div id="pending-holder">
                              <div className="active-allergy">
                                <p>{countPendingAssessment(residentAllData.Assessments).length}</p>
                              </div>
                            </div>
                            <div id="stats-holder">
                              <div className="active-allergy">
                                <p>
                                  {countCompletedAssessment(residentAllData.Assessments).length}
                                </p>
                              </div>
                            </div>
                          </>
                        ) : (
                          <div className="nodata warning-ico">
                            <AssignmentLateIcon style={{ fontSize: '30px' }} />
                          </div>
                        )}
                        <CardActionArea
                          onClick={() => {
                            // changeActiveTab(12)
                            setIsPopupOpen(true)
                            setPopupChildren('assessment')
                          }}
                        >
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Assessment
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {residentAllData.Assessments != null ? (
                                <>
                                  <p>
                                    Assessment Type: {getLatestPending(residentAllData.Assessments)}{' '}
                                  </p>
                                  <p>
                                    Review date:{' '}
                                    {formatDate(getReviewData(residentAllData.Assessments))}{' '}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto position-relative"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        {residentAllData.CarePlans != null ? (
                          <>
                            <div id="pending-holder">
                              <div className="active-allergy">
                                <p>{countPendingCarePlan(residentAllData.CarePlans).length}</p>
                              </div>
                            </div>
                            <div id="stats-holder">
                              <div className="active-allergy">
                                <p>{countingCompleteCareplan(residentAllData.CarePlans).length}</p>
                              </div>
                            </div>
                          </>
                        ) : (
                          <div className="nodata warning-ico">
                            <AssignmentLateIcon style={{ fontSize: '30px' }} />
                          </div>
                        )}
                        <CardActionArea
                          onClick={() => {
                            // changeActiveTab(12)
                            setIsPopupOpen(true)
                            setPopupChildren('careplan')
                          }}
                        >
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Care plan
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {residentAllData.CarePlans != null ? (
                                <>
                                  <p>
                                    Care plan item:{' '}
                                    {getCareplanLatestData(residentAllData.CarePlans)}{' '}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    {/* <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <PopupAll>
                            <DisabilitiesTable />
                          </PopupAll>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Disabilities
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {disabilitiesData.length > 0 ? (
                                <div className="done-ico">
                                  <CheckCircleIcon style={{ fontSize: '25px', color: 'green' }} />
                                </div>
                              ) : (
                                <div className="warning-ico">
                                  <AssignmentLateIcon style={{ fontSize: '30px' }} />
                                </div>
                              )}
                              {disabilitiesData.length > 0 ? (
                                <>
                                  {' '}
                                  <p>
                                    Disabilities:{' '}
                                    {disabilitiesData[disabilitiesData.length - 1].name}
                                  </p>
                                  <p>
                                    Start Date:{' '}
                                    {formatDate(
                                      disabilitiesData[disabilitiesData.length - 1].startDate,
                                    )}
                                  </p>
                                </>
                              ) : (
                                <>
                                  <p>No Available Data</p>
                                  <div className="warning-ico">
                                    <AssignmentLateIcon style={{ fontSize: '30px' }} />
                                  </div>
                                </>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol> */}
                  </CRow>
                </CContainer>
              </TabPanel>
              <TabPanel value={value} index={0}>
                <CContainer>
                  <CRow xs={{ gutterY: 3 }}>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea
                          onClick={() => {
                            setIsPopupOpen(true)
                            setPopupChildren('basicInfo')
                          }}
                        >
                          <CardContent className="position-relative">
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Basic Info
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {residentAllData.BasicInfo != null ? (
                                <div className="done-ico">
                                  <CheckCircleIcon style={{ fontSize: '25px', color: 'green' }} />
                                </div>
                              ) : (
                                <div className="warning-ico">
                                  <AssignmentLateIcon style={{ fontSize: '30px' }} />
                                </div>
                              )}
                              {residentAllData.length != 0 ? (
                                <>
                                  <p>
                                    Place of Birth:
                                    {residentAllData.BasicInfo != null &&
                                    residentAllData.BasicInfo.placeofBirth != null
                                      ? residentAllData.BasicInfo.placeofBirth
                                      : 'N/A'}
                                  </p>
                                  <p>
                                    Preferred Name:
                                    {residentAllData.BasicInfo != null &&
                                    residentAllData.BasicInfo.preferredName != null
                                      ? residentAllData.BasicInfo.preferredName
                                      : 'N/A'}
                                  </p>
                                  <p>
                                    Race:{' '}
                                    {residentAllData.BasicInfo != null &&
                                    residentAllData.BasicInfo.race != null
                                      ? residentAllData.BasicInfo.race
                                      : 'N/A'}
                                  </p>
                                  <p>
                                    Languages:{' '}
                                    {residentAllData.BasicInfo != null &&
                                    residentAllData.BasicInfo.languages != null
                                      ? residentAllData.BasicInfo.languages.join(' , ')
                                      : 'N/A'}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea
                          onClick={() => {
                            setIsPopupOpen(true)
                            setPopupChildren('contact')
                          }}
                        >
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Contacts
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {residentAllData.ContactPeople != null ? (
                                <div className="done-ico">
                                  <CheckCircleIcon style={{ fontSize: '25px', color: 'green' }} />
                                </div>
                              ) : (
                                <div className="warning-ico">
                                  <AssignmentLateIcon style={{ fontSize: '30px' }} />
                                </div>
                              )}

                              {residentAllData.length != 0 ? (
                                <>
                                  <p>
                                    Full Name :{' '}
                                    {residentAllData.ContactPeople.length > 0
                                      ? getFullName(
                                          residentAllData.ContactPeople[
                                            residentAllData.ContactPeople.length - 1
                                          ],
                                        )
                                      : 'N/A'}
                                  </p>
                                  <p>
                                    Contact No:{' '}
                                    {residentAllData.ContactPeople.length > 0 &&
                                    residentAllData.ContactPeople[
                                      residentAllData.ContactPeople.length - 1
                                    ].contact != null
                                      ? residentAllData.ContactPeople[
                                          residentAllData.ContactPeople.length - 1
                                        ].contact
                                      : 'N/A'}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <PopupAll title="Insurance Information">
                            <InsuranceInfo />
                          </PopupAll>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Insurance Information
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {insuranceInfoData.length > 0 ? (
                                <div className="done-ico">
                                  <CheckCircleIcon style={{ fontSize: '25px', color: 'green' }} />
                                </div>
                              ) : (
                                <div className="warning-ico">
                                  <AssignmentLateIcon style={{ fontSize: '30px' }} />
                                </div>
                              )}
                              {insuranceInfoData.length > 0 ? (
                                <>
                                  {' '}
                                  <p>
                                    Insurance Name:{' '}
                                    {insuranceInfoData[insuranceInfoData.length - 1].InsuranceName}
                                  </p>
                                  <p>
                                    Polocy Number:{' '}
                                    {insuranceInfoData[insuranceInfoData.length - 1].PolicyNo}
                                  </p>
                                  <p>
                                    Expiration Data:{' '}
                                    {formatDate(
                                      insuranceInfoData[insuranceInfoData.length - 1]
                                        .ExpirationDate,
                                    )}
                                  </p>
                                </>
                              ) : (
                                <>
                                  <p>No Available Data</p>
                                  <div className="warning-ico">
                                    <AssignmentLateIcon style={{ fontSize: '30px' }} />
                                  </div>
                                </>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <PopupAll title="Health Care Directives">
                            <HealthCareDirectivesTable />
                          </PopupAll>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Health Care Directives
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {HealthCareDirectivesData.length > 0 ? (
                                <div className="done-ico">
                                  <CheckCircleIcon style={{ fontSize: '25px', color: 'green' }} />
                                </div>
                              ) : (
                                <div className="warning-ico">
                                  <AssignmentLateIcon style={{ fontSize: '30px' }} />
                                </div>
                              )}
                              {HealthCareDirectivesData.length > 0 ? (
                                <>
                                  {' '}
                                  <p>
                                    Primary Physician:{' '}
                                    {
                                      HealthCareDirectivesData[HealthCareDirectivesData.length - 1]
                                        .primaryPhysician
                                    }
                                  </p>
                                  <p>
                                    Note:{' '}
                                    {
                                      HealthCareDirectivesData[HealthCareDirectivesData.length - 1]
                                        .notes
                                    }
                                  </p>
                                </>
                              ) : (
                                <>
                                  <p>No Available Data</p>
                                </>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    {/* <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <PopupAll>
                            <RoutinesTable />
                          </PopupAll>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Routines
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {routineDate.length > 0 ? (
                                <div className="done-ico">
                                  <CheckCircleIcon style={{ fontSize: '25px', color: 'green' }} />
                                </div>
                              ) : (
                                <div className="warning-ico">
                                  <AssignmentLateIcon style={{ fontSize: '30px' }} />
                                </div>
                              )}
                              {routineDate.length > 0 ? (
                                <>
                                  {' '}
                                  <p>Name: {routineDate[routineDate.length - 1].name}</p>
                                  <p>Frequency: {routineDate[routineDate.length - 1].frequency}</p>
                                </>
                              ) : (
                                <>
                                  <p>No Available Data</p>
                                  <div className="warning-ico">
                                    <AssignmentLateIcon style={{ fontSize: '30px' }} />
                                  </div>
                                </>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol> */}

                    {/* <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Miscellaneous
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              <p>No Available Data</p>
                              <div className="warning-ico">
                                <AssignmentLateIcon style={{ fontSize: '30px' }} />
                              </div>
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol> */}
                    {/* <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography id="header-text" gutterBottom component="div">
                                Digital Documents
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              <p>No Available Data</p>
                              <div className="warning-ico">
                                <AssignmentLateIcon style={{ fontSize: '30px' }} />
                              </div>
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol> */}
                  </CRow>
                </CContainer>
              </TabPanel>
              <TabPanel value={value} index={2}>
                Reports
              </TabPanel>
              <TabPanel value={value} index={3}>
                <EDocuments />
              </TabPanel>
              <TabPanel value={value} index={4}>
                Contacts
              </TabPanel>
              <TabPanel value={value} index={5}>
                Insurance
              </TabPanel>
              <TabPanel value={value} index={6}>
                Healthcare
              </TabPanel>
              <TabPanel value={value} index={7}>
                Routines
              </TabPanel>
              <TabPanel value={value} index={8}>
                Medical History
              </TabPanel>
              <TabPanel value={value} index={9}>
                Surgeries
              </TabPanel>
              <TabPanel value={value} index={10}>
                Disabilities
              </TabPanel>
              <TabPanel value={value} index={11}>
                Miscellaneous
              </TabPanel>
              <TabPanel value={value} index={12}>
                Digital Documents
              </TabPanel>
            </Box>
          </div>
        </>
      ) : (
        <div id="tenant-profile-loader">
          <ContentLoader
            speed={2}
            viewBox="0 0 476 124"
            backgroundColor={'#3333'}
            foregroundColor={'#999'}
          >
            <rect x="97" y="56" rx="3" ry="3" width="410" height="6" />
            <rect x="97" y="75" rx="3" ry="3" width="380" height="6" />
            <rect x="97" y="90" rx="3" ry="3" width="178" height="6" />
            <rect x="4" y="16" rx="0" ry="0" width="87" height="83" />
            <rect x="97" y="18" rx="3" ry="3" width="380" height="6" />
            <rect x="97" y="37" rx="3" ry="3" width="217" height="6" />
          </ContentLoader>
          <ContentLoader
            speed={2}
            viewBox="0 0 476 124"
            backgroundColor={'#3333'}
            foregroundColor={'#999'}
          >
            <rect x="53" y="112" rx="0" ry="0" width="0" height="5" />
            <rect x="5" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="52" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="99" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="146" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="193" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="240" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="287" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="334" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="381" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="428" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="9" y="52" rx="0" ry="0" width="458" height="8" />
            <rect x="10" y="72" rx="0" ry="0" width="266" height="5" />
            <rect x="10" y="88" rx="0" ry="0" width="357" height="6" />
          </ContentLoader>
        </div>
      )}

      {/* <Dietary /> */}

      <Row>
        {/*<div style={{ display: "none" }}>
              <FaceSheet ref={componentRef} />
            </div> */}

        {/*insert action button tab here*/}

        <Col md="12" className="d-flex justify-content-center">
          <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
          />
        </Col>
      </Row>
    </div>
  )
}

export default TenantProfile
