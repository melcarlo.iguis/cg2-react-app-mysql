import React, { useEffect, useState } from 'react'
import { useContext } from 'react'
import PropTypes from 'prop-types'
import Button from '@mui/material/Button'
import DeleteIcon from '@mui/icons-material/Delete'
import SendIcon from '@mui/icons-material/Send'
import { styled } from '@mui/material/styles'
import noPhoto from '../../../assets/images/nophoto.jpg'
import Dialog from '@mui/material/Dialog'
import { Row, Col, Container, Form } from 'react-bootstrap'
import CheckIcon from '@mui/icons-material/Check'
import Tooltip from '@mui/material/Tooltip'
import RemoveIcon from '@mui/icons-material/Remove'
import ListSubheader from '@mui/material/ListSubheader'
import TextField from '@mui/material/TextField'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import DialogActions from '@mui/material/DialogActions'
import IconButton from '@mui/material/IconButton'
import AddBox from '@material-ui/icons/AddBox'
import Divider from '@mui/material/Divider'
import CloseIcon from '@mui/icons-material/Close'
import Grid from '@mui/material/Grid'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import Select, { SelectChangeEvent } from '@mui/material/Select'
import Typography from '@mui/material/Typography'
// global variable
import AppContext from '../../../AppContext'
import Slide from '@mui/material/Slide'
import AddIcon from '@mui/icons-material/Add'
import Wounds from '../include/clinical/Wounds'
import Progressnote from '../include/clinical/Progressnote'
import Assessments from '../include/clinical/Assessments'
import PerformAssessmentForm from '../include/clinical/PerformAssessmentForm'
import CarePlan from '../include/clinical/CarePlan'
import BasicInfo from '../form/BasicInfo'
import api from '../../../api/api'
import Contact from '../include/profile/Contact'
import { CButton } from '@coreui/react'
import Backdrop from '@mui/material/Backdrop'
import { makeStyles } from '@material-ui/core/styles'
// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

import { useTheme } from '@mui/material/styles'
import Box from '@mui/material/Box'
import OutlinedInput from '@mui/material/OutlinedInput'
import Chip from '@mui/material/Chip'
import { faBullseye } from '@fortawesome/free-solid-svg-icons'

const ITEM_HEIGHT = 48
const ITEM_PADDING_TOP = 8
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
}

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}))

const useStyles = makeStyles(() => ({
  dialog: {
    height: 800,
  },
}))

console.count('Render')

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />
})

const BootstrapDialogTitle = (props) => {
  const { title, children, onClose, ...other } = props

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {title}
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  )
}

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
}

export default function CustomizedDialogs({ children }) {
  const theme = useTheme()
  const [firstName, setFirstName] = useState('')
  const [middleName, setMiddleName] = useState('')
  const [lastName, setLastName] = useState('')
  const [relationship, setRelationship] = useState('')
  const [emailData, setEmailData] = useState([{ email: '' }])
  const [contactData, setContactData] = useState([{ phone: '' }])
  const [addressData, setAddressData] = useState([{ address: '' }])
  const [responsibility, setResponsibility] = useState([])
  const [openResponsibility, setOpenResponsibility] = React.useState(false)

  const [otherInput, setOtherInput] = useState('')

  const [showOtherInputField, setshowOtherInputField] = useState(true)
  const handleCloseResponsibility = () => {
    setOpenResponsibility(false)
  }

  const handleOpen = () => {
    setOpenResponsibility(true)
  }
  const handleChange = (event) => {
    const {
      target: { value },
    } = event

    let index = value.indexOf('Other')
    if (index != -1) {
      setOpenResponsibility(false)
      setshowOtherInputField(false)
    } else {
      console.log(value)
      setResponsibility(
        // On autofill we get a stringified value.
        typeof value === 'string' ? value.split(',') : value,
      )
    }
    // let findIfValueExist = setPersonName([...personName, value.toString()])
  }

  // states for picture
  const [open, setOpen] = React.useState(false)
  const [scroll, setScroll] = React.useState('paper')
  const {
    currentTab,
    setCurrentTab,
    action,
    setAction,
    content,
    setContent,
    dialogClose,
    setDialogClose,
    activeTab,
    setActiveTab,
    setIsPopupOpen,
    isInsurancePopupOpen,
    setIsInsurnacePopupOpen,
    popupChildren,
    setInsuranceInfoData,
    insuranceInfoData,
    setPopupChildren,
    setPopupTitle,
    popupTitle,
    contactPeopleData,
    setContactPeopleData,
  } = useContext(AppContext)
  const [fullWidth, setFullWidth] = React.useState(true)
  const [maxWidth, setMaxWidth] = React.useState('lg')

  const handleFileInputChange = (e) => {
    const file = e.target.files[0]
    previewFile(file)
    setImageFile(file)
  }

  const previewFile = (file) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onloadend = () => {
      setImage(reader.result)
    }
  }

  useEffect(() => {
    setOpen(false)
  }, [])

  useEffect(() => {
    if (isInsurancePopupOpen) {
      setOpen(true)
    }
  }, [isInsurancePopupOpen])

  const handleClickOpen = () => {
    setOpen(true)
    localStorage.setItem('popupIsOpen', true)
  }
  const handleClose = () => {
    setOpen(false)
    setIsInsurnacePopupOpen(false)
    localStorage.setItem('popupIsOpen', false)
  }
  let tenantName = localStorage.getItem('tenantName')
  const classes = useStyles()

  const addOtherInput = (e) => {
    e.preventDefault()
    setResponsibility([...responsibility, otherInput])
    setshowOtherInputField(true)
    setOtherInput('')
  }

  const fetchResidentData = async () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')
    await api
      .get(`/residents/${tenantId}/fetchAllData`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        setContactPeopleData(res.data.ContactPeople)
      })
  }

  const getFullName = (data) => {
    return data.lastName + ',' + ' ' + data.firstName + ' ' + data.middleName
  }

  // const setInitialValue = (data) => {
  //   setAddress(data.ContactPerson.address)
  //   setContactNo(data.ContactPerson.contactNo)
  //   setFullName(getFullName(data.ContactPerson))
  // }

  const addContact = (e) => {
    e.preventDefault()
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')
    let userId = localStorage.getItem('userId')

    const input = {
      firstName: firstName,
      middleName: middleName,
      lastName: lastName,
      relationship: relationship,
      // email: email,
      // contact: contact,
      responsibility: responsibility,
      ResidentId: tenantId,
    }

    // api call for adding new allergy tenant
    api
      .post(`/contact/add`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        emailData.map((data) => {
          let emailInput = {
            email: data.email,
            ContactPersonId: result.data.id,
          }

          api
            .post(`/contactEmail/add`, emailInput, {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
            .then((res) => {
              console.log(res)
            })
        })
        contactData.map((data) => {
          let contactInput = {
            phoneNumber: data.phone,
            ContactPersonId: result.data.id,
          }

          api
            .post(`/contactPhone/add`, contactInput, {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
            .then((res) => {
              console.log(res)
            })
        })
        addressData.map((data) => {
          let addressInput = {
            address: data.address,
            ContactPersonId: result.data.id,
          }

          api
            .post(`/contactAddress/add`, addressInput, {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
            .then((res) => {
              console.log(res)
            })
        })
        fetchResidentData()
        setTimeout(() => {
          handleClose()
        }, 2000)
        toast.success('Added Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
        // code for adding daily log
      })
      .catch((err) => {
        console.log(err)
      })
  }

  // dynamic address input
  const handleAddressInputChange = (index, e) => {
    console.log(index)
    const values = [...addressData]
    values[index].address = e.target.value
    setAddressData(values)
  }

  // dynamic contact input
  const handlePhoneInputChange = (index, e) => {
    console.log(index)
    const values = [...contactData]
    values[index].phone = e.target.value
    setContactData(values)
  }

  // dynamic email input
  const handleEmailInputChange = (index, e) => {
    console.log(index)
    const values = [...emailData]
    values[index].email = e.target.value
    setEmailData(values)
  }

  const handleAdd = (e, type) => {
    e.preventDefault()
    if (type == 'email') {
      if (emailData.length <= 4) {
        setEmailData([...emailData, { email: '' }])
      } else {
        return
      }
    } else if (type == 'phone') {
      if (contactData.length <= 4) {
        setContactData([...contactData, { phone: '' }])
      } else {
        return
      }
    } else if (type == 'address') {
      if (addressData.length <= 4) {
        setAddressData([...addressData, { address: '' }])
      } else {
        return
      }
    }
  }

  const handleRemoveField = (e, index, type) => {
    e.preventDefault()
    if (type == 'email') {
      if (emailData.length > 1) {
        const values = [...emailData]
        values.splice(index, 1)
        setEmailData(values)
      } else {
        return
      }
    } else if (type == 'phone') {
      if (contactData.length > 1) {
        const values = [...contactData]
        values.splice(index, 1)
        setContactData(values)
      } else {
        return
      }
    } else if (type == 'address') {
      if (addressData.length > 1) {
        const values = [...addressData]
        values.splice(index, 1)
        setAddressData(values)
      } else {
        return
      }
    }
  }

  return (
    <div>
      <div id="addInsuranceBtn2" variant="contained" onClick={handleClickOpen}>
        <AddBox />
      </div>
      <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={open}
        style={{ backdropFilter: 'blur(5px)', pointerEvents: 'none' }}
      >
        <BootstrapDialog
          scroll={scroll}
          fullWidth={fullWidth}
          maxWidth={maxWidth}
          onClose={handleClose}
          TransitionComponent={Transition}
          aria-labelledby="customized-dialog-title"
          open={open}
          classes={{ paper: classes.dialog }}
        >
          <DialogTitle>
            <p>Add New Contact</p>
          </DialogTitle>

          <DialogContent className="position-relative py-0 add-contact-holder" dividers>
            <form onSubmit={(e) => addContact(e)} className="mt-3">
              <Container fluid>
                <Grid container spacing={2}>
                  <Grid item md="4" sm="12" className="phone-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'black' },
                        }}
                        id="standard-basic"
                        label="First Name"
                        variant="standard"
                        value={firstName}
                        onChange={(e) => setFirstName(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="12" className="phone-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'black' },
                        }}
                        id="standard-basic"
                        label="Middle Name"
                        variant="standard"
                        value={middleName}
                        onChange={(e) => setMiddleName(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="12" className="phone-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'black' },
                        }}
                        id="standard-basic"
                        label="Last Name"
                        variant="standard"
                        value={lastName}
                        onChange={(e) => setLastName(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="12" className="phone-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'black' },
                        }}
                        id="standard-basic"
                        label="Relationship"
                        variant="standard"
                        value={relationship}
                        onChange={(e) => setRelationship(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="12">
                    <Grid container spacing={2} className="d-flex justify-content-center">
                      <Grid item md={6} xs={12} className="phone-holder">
                        <div className="holder">
                          {emailData.map((inputField, index) => (
                            <>
                              <Row>
                                <Col md={11} xs={10}>
                                  <TextField
                                    fullWidth
                                    type="email"
                                    InputProps={{
                                      style: {
                                        fontSize: '15px',
                                        margin: '10px 0',
                                        color: 'black',
                                      },
                                    }}
                                    id="standard-basic"
                                    label="Email"
                                    variant="standard"
                                    value={inputField.email}
                                    onChange={(event) => handleEmailInputChange(index, event)}
                                    required
                                  />
                                </Col>
                                <Col md={1} xs={2}>
                                  <div
                                    className="remove-btn"
                                    onClick={(e) => handleRemoveField(e, index, 'email')}
                                  >
                                    <CloseIcon />
                                  </div>
                                </Col>
                              </Row>
                            </>
                          ))}
                          <div className="add-btn-holder" onClick={(e) => handleAdd(e, 'email')}>
                            {' '}
                            <Button
                              className="btn"
                              disableRipple
                              disableTouchRipple
                              disableFocusRipple
                              variant="contained"
                              startIcon={<AddIcon style={{ fontSize: '25px' }} />}
                            >
                              Email
                            </Button>
                          </div>
                        </div>
                      </Grid>
                      <Grid item md={6} xs={12} className="phone-holder ">
                        <div className="holder">
                          {contactData.map((inputField, index) => (
                            <>
                              <Row>
                                <Col md={11} xs={10}>
                                  <TextField
                                    fullWidth
                                    InputProps={{
                                      style: {
                                        fontSize: '15px',
                                        margin: '10px 0',
                                        color: 'black',
                                      },
                                    }}
                                    id="standard-basic"
                                    label="Phone"
                                    variant="standard"
                                    value={inputField.phone}
                                    onChange={(event) => handlePhoneInputChange(index, event)}
                                    required
                                  />
                                </Col>
                                <Col md={1} xs={2}>
                                  <div
                                    className="remove-btn"
                                    onClick={(e) => handleRemoveField(e, index, 'phone')}
                                  >
                                    <CloseIcon />
                                  </div>
                                </Col>
                                {/* <Col md={1}>
                                <div onClick={(e) => handleAdd(e, 'phone')}>
                                  <AddIcon style={{ margin: '.5rem' }} />
                                </div>
                              </Col> */}
                              </Row>
                            </>
                          ))}
                          <div className="add-btn-holder" onClick={(e) => handleAdd(e, 'phone')}>
                            {' '}
                            <Button
                              className="btn"
                              disableRipple
                              disableTouchRipple
                              disableFocusRipple
                              variant="contained"
                              startIcon={<AddIcon style={{ fontSize: '25px' }} />}
                            >
                              Phone
                            </Button>
                          </div>
                        </div>
                      </Grid>
                      <Grid item md={6} xs={12} className="phone-holder ">
                        <div className="holder">
                          {addressData.map((inputField, index) => (
                            <>
                              <Row>
                                <Col md={11} xs={10}>
                                  <TextField
                                    fullWidth
                                    InputProps={{
                                      style: {
                                        fontSize: '15px',
                                        margin: '10px 0',
                                        color: 'black',
                                      },
                                    }}
                                    id="standard-basic"
                                    label="Address"
                                    variant="standard"
                                    value={inputField.address}
                                    onChange={(event) => handleAddressInputChange(index, event)}
                                    required
                                  />
                                </Col>
                                <Col md={1} xs={2}>
                                  <div
                                    className="remove-btn"
                                    onClick={(e) => handleRemoveField(e, index, 'address')}
                                  >
                                    <CloseIcon />
                                  </div>
                                </Col>
                              </Row>
                            </>
                          ))}
                          <div className="add-btn-holder" onClick={(e) => handleAdd(e, 'address')}>
                            {' '}
                            <Button
                              className="btn"
                              disableRipple
                              disableTouchRipple
                              disableFocusRipple
                              variant="contained"
                              startIcon={<AddIcon style={{ fontSize: '25px' }} />}
                            >
                              Address
                            </Button>
                          </div>
                        </div>
                      </Grid>
                      <Grid item md={6} xs={12} className="phone-holder ">
                        <div className="holder">
                          <FormControl variant="standard" sx={{ minWidth: '100%' }}>
                            <InputLabel variant="standard" id="demo-multiple-chip-label">
                              Responsibility
                            </InputLabel>
                            <Select
                              variant="standard"
                              labelId="demo-multiple-chip-label"
                              id="demo-multiple-chip"
                              multiple
                              open={openResponsibility}
                              onClose={handleCloseResponsibility}
                              onOpen={handleOpen}
                              value={responsibility}
                              onChange={handleChange}
                              // input={<OutlinedInput id="select-multiple-chip" label="Chip" />}
                              renderValue={(selected) => (
                                <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
                                  {selected.map((value) => (
                                    <Chip color="primary" key={value} label={value} />
                                  ))}
                                </Box>
                              )}
                              MenuProps={MenuProps}
                            >
                              <ListSubheader>POA</ListSubheader>
                              <MenuItem value="POA for Health">Health</MenuItem>
                              <MenuItem value="POA General">General</MenuItem>
                              <MenuItem value="POA for Finance">for Finance</MenuItem>
                              <ListSubheader>Conservatorship of:</ListSubheader>
                              <MenuItem value="Conservatorship of Person">Person</MenuItem>
                              <MenuItem value="Conservatorship of Estate">Estate</MenuItem>
                              <MenuItem value="Conservatorship of LPS">LPS</MenuItem>
                              <MenuItem value="Other">Other</MenuItem>

                              {/* {names.map((name) => (
                      <MenuItem key={name} value={name} style={getStyles(name, personName, theme)}>
                        {name}
                      </MenuItem>
                    ))} */}
                            </Select>
                          </FormControl>

                          <Grid container hidden={showOtherInputField}>
                            <div className="d-flex">
                              <Grid item md={11}>
                                <TextField
                                  InputProps={{
                                    style: {
                                      fontSize: '15px',
                                      margin: '10px 0',
                                      color: 'black',
                                      width: '410px',
                                    },
                                  }}
                                  id="standard-basic"
                                  label="Other"
                                  value={otherInput}
                                  onChange={(e) => setOtherInput(e.target.value)}
                                  variant="standard"
                                />
                              </Grid>
                              <Grid item md={1}>
                                <div onClick={(e) => addOtherInput(e)}>
                                  <AddIcon style={{ margin: '.5rem' }} />
                                </div>
                              </Grid>
                            </div>
                          </Grid>
                        </div>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>

                <Tooltip title="Save" placement="bottom">
                  <div id="submit-btn5">
                    <label htmlFor="submit-btn">
                      <CheckIcon />
                    </label>
                  </div>
                </Tooltip>
                <CButton type="submit" id="submit-btn" hidden>
                  Submit
                </CButton>
              </Container>
            </form>
          </DialogContent>
          <DialogActions>
            <IconButton
              aria-label="close"
              onClick={handleClose}
              sx={{
                position: 'absolute',
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500],
              }}
            >
              <CloseIcon />
            </IconButton>
          </DialogActions>
        </BootstrapDialog>
      </Backdrop>
      {/* <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      /> */}
    </div>
  )
}
