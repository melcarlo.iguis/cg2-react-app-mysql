import React, { useEffect, useState } from 'react'
import { useContext } from 'react'
import PropTypes from 'prop-types'
import Button from '@mui/material/Button'
import { styled } from '@mui/material/styles'
import Tooltip from '@mui/material/Tooltip'
import UploadFileIcon from '@mui/icons-material/UploadFile'
import Grid from '@mui/material/Grid'
import noPhoto from '../../../assets/images/nophoto.jpg'
import Dialog from '@mui/material/Dialog'
import { Row, Col, Container, Form } from 'react-bootstrap'
import CheckIcon from '@mui/icons-material/Check'
import TextField from '@mui/material/TextField'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import DialogActions from '@mui/material/DialogActions'
import IconButton from '@mui/material/IconButton'
import AddBox from '@material-ui/icons/AddBox'
import CloseIcon from '@mui/icons-material/Close'
import Typography from '@mui/material/Typography'
// global variable
import AppContext from '../../../AppContext'
import Slide from '@mui/material/Slide'
import AddIcon from '@mui/icons-material/Add'
import Wounds from '../include/clinical/Wounds'
import Progressnote from '../include/clinical/Progressnote'
import Assessments from '../include/clinical/Assessments'
import PerformAssessmentForm from '../include/clinical/PerformAssessmentForm'
import CarePlan from '../include/clinical/CarePlan'
import BasicInfo from '../form/BasicInfo'
import api from '../../../api/api'
import Contact from '../include/profile/Contact'
import { CButton } from '@coreui/react'
import Backdrop from '@mui/material/Backdrop'
import { makeStyles } from '@material-ui/core/styles'
// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}))

const useStyles = makeStyles(() => ({
  dialog: {
    height: 800,
  },
}))

console.count('Render')

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />
})

const BootstrapDialogTitle = (props) => {
  const { title, children, onClose, ...other } = props

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {title}
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  )
}

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
}

export default function CustomizedDialogs({ children }) {
  let newDate = new Date()
  let day = newDate.getDate()
  let month = newDate.getMonth() + 1
  let year = newDate.getFullYear()

  let newMonth = month < 10 ? '0' + month : month
  let newDay = day < 10 ? '0' + day : day

  let fullDate = year + '-' + newMonth + '-' + newDay
  // states for form
  const [codeStatus, setCodeStatus] = useState('')
  const [advanceCareOrder, setAdvanceCareOrder] = useState('')
  const [notes, setNotes] = useState('')
  //   const [personActingforResident, setPersonActingforResident] = useState('')
  const [primaryPhysician, setPrimaryPhysician] = useState('')
  const [primaryPhysicianTelNo, setPrimaryPhysicianTelNo] = useState('')
  const [primaryPhysicianAddress, setPrimaryPhysicianAddress] = useState('')
  const [MortuaryInfo, setMortuaryInfo] = useState('')

  // states for picture
  const [imageFile, setImageFile] = useState('')
  const [image, setImage] = useState('')
  const [open, setOpen] = React.useState(false)
  const [scroll, setScroll] = React.useState('paper')
  const {
    currentTab,
    setCurrentTab,
    action,
    setAction,
    content,
    setContent,
    dialogClose,
    setDialogClose,
    activeTab,
    setActiveTab,
    setIsPopupOpen,
    isInsurancePopupOpen,
    setIsInsurnacePopupOpen,
    popupChildren,
    setInsuranceInfoData,
    insuranceInfoData,
    setPopupChildren,
    setPopupTitle,
    HealthCareDirectivesData,
    setHealthCareDirectivesData,
    popupTitle,
  } = useContext(AppContext)
  const [fullWidth, setFullWidth] = React.useState(true)
  const [maxWidth, setMaxWidth] = React.useState('lg')
  const [fileName, setFileName] = useState('')

  const handleFileInputChange = (e) => {
    const file = e.target.files[0]
    previewFile(file)
    setImageFile(file)
    setFileName(file.name)
  }

  const previewFile = (file) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onloadend = () => {
      setImage(reader.result)
    }
  }

  useEffect(() => {
    setOpen(false)
  }, [])

  useEffect(() => {
    if (isInsurancePopupOpen) {
      setOpen(true)
    }
  }, [isInsurancePopupOpen])

  const handleClickOpen = () => {
    setOpen(true)
    localStorage.setItem('popupIsOpen', true)
  }
  const handleClose = () => {
    setOpen(false)
    setIsInsurnacePopupOpen(false)
    localStorage.setItem('popupIsOpen', false)
  }
  let tenantName = localStorage.getItem('tenantName')
  const classes = useStyles()

  const addHealthCareDirectivesData = (e) => {
    e.preventDefault()
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')
    let userId = localStorage.getItem('userId')

    const formData = new FormData()

    formData.append('image', imageFile)
    formData.append('codeStatus', codeStatus)
    formData.append('advanceCareOrder', advanceCareOrder)
    formData.append('notes', notes)
    // formData.append('personActingforResident', personActingforResident)
    formData.append('primaryPhysician', primaryPhysician)
    formData.append('primaryPhysicianTelNo', primaryPhysicianTelNo)
    formData.append('primaryPhysicianAddress', primaryPhysicianAddress)
    formData.append('MortuaryInfo', MortuaryInfo)
    formData.append('ResidentId', tenantId)
    formData.append('creator', name)

    // api call for adding new allergy tenant
    api
      .post(`/healthcareDirectives/add`, formData, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        // setInsuranceInfoData([...insuranceInfoData, result.data])
        setHealthCareDirectivesData([...HealthCareDirectivesData, result.data])

        toast.success('Added Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
        // code for adding daily log
      })
      .catch((err) => {
        console.log(err)
      })
  }

  return (
    <div>
      <div id="addInsuranceBtn" variant="contained" onClick={handleClickOpen}>
        <AddBox />
      </div>
      <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={open}
        style={{ backdropFilter: 'blur(5px)', pointerEvents: 'none' }}
      >
        <BootstrapDialog
          scroll={scroll}
          fullWidth={fullWidth}
          maxWidth={maxWidth}
          onClose={handleClose}
          TransitionComponent={Transition}
          aria-labelledby="customized-dialog-title"
          open={open}
          classes={{ paper: classes.dialog }}
        >
          <DialogTitle>
            <p>Add New Health Care Directives</p>
          </DialogTitle>

          <DialogContent className="position-relative py-0" dividers>
            <form onSubmit={(e) => addHealthCareDirectivesData(e)}>
              <Container fluid>
                <Grid container spacing={2}>
                  <Grid item md="12" sm="12" className="mx-auto text-center">
                    <Grid container>
                      <Grid item md="6" sm="6" className="mx-auto text-center mt-4 main-holder">
                        <div className="file-upload-holder">
                          <Tooltip title="Upload File" placement="right">
                            <label id="input-label" htmlFor="input-file">
                              <UploadFileIcon />
                            </label>
                          </Tooltip>
                          <Form.Control
                            required
                            hidden
                            id="input-file"
                            type="file"
                            // value={fileInputState}
                            onChange={handleFileInputChange}
                          />
                          <label className="d-block">{fileName ? fileName : ''}</label>
                          <label>Attachment File</label>
                        </div>
                      </Grid>
                    </Grid>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'black' },
                        }}
                        id="standard-basic"
                        label="Code Status"
                        variant="standard"
                        value={codeStatus}
                        onChange={(e) => setCodeStatus(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'black' },
                        }}
                        id="standard-basic"
                        label="Advance Care Order"
                        variant="standard"
                        value={advanceCareOrder}
                        onChange={(e) => setAdvanceCareOrder(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'black' },
                        }}
                        id="standard-basic"
                        label="Notes"
                        variant="standard"
                        value={notes}
                        onChange={(e) => setNotes(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  {/* <Col md="4" sm="6">
                    <TextField
                      fullWidth
                      InputProps={{ style: { fontSize: '15px', margin: '10px 0', color: 'black' } }}
                      id="standard-basic"
                      label="Person Acting for Resident"
                      variant="standard"
                      value={personActingforResident}
                      onChange={(e) => setPersonActingforResident(e.target.value)}
                      required
                    />
                  </Col> */}
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'black' },
                        }}
                        id="standard-basic"
                        label="Primary Physician"
                        variant="standard"
                        value={primaryPhysician}
                        onChange={(e) => setPrimaryPhysician(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'black' },
                        }}
                        id="standard-basic"
                        label="Primary Physician Tel No"
                        variant="standard"
                        value={primaryPhysicianTelNo}
                        onChange={(e) => setPrimaryPhysicianTelNo(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'black' },
                        }}
                        id="standard-basic"
                        label="Primary Physician Address"
                        variant="standard"
                        value={primaryPhysicianAddress}
                        onChange={(e) => setPrimaryPhysicianAddress(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'black' },
                        }}
                        id="standard-basic"
                        label="Mortuary Information"
                        variant="standard"
                        value={MortuaryInfo}
                        onChange={(e) => setMortuaryInfo(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                </Grid>
                <Tooltip title="Save" placement="bottom">
                  <div id="submit-btn5">
                    <label htmlFor="submit-btn">
                      <CheckIcon />
                    </label>
                  </div>
                </Tooltip>
                <CButton type="submit" id="submit-btn" hidden>
                  Submit
                </CButton>
              </Container>
            </form>
          </DialogContent>
          <DialogActions>
            <IconButton
              aria-label="close"
              onClick={handleClose}
              sx={{
                position: 'absolute',
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500],
              }}
            >
              <CloseIcon />
            </IconButton>
          </DialogActions>
        </BootstrapDialog>
      </Backdrop>
      {/* <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      /> */}
    </div>
  )
}
