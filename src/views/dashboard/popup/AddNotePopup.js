import React, { useEffect } from 'react'
import { useContext, useState } from 'react'
import PropTypes from 'prop-types'
import Button from '@mui/material/Button'
import { styled } from '@mui/material/styles'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import DialogActions from '@mui/material/DialogActions'
import IconButton from '@mui/material/IconButton'
import CloseIcon from '@mui/icons-material/Close'
import Typography from '@mui/material/Typography'
// global variable
import AppContext from '../../../AppContext'
import Slide from '@mui/material/Slide'
import AddIcon from '@mui/icons-material/Add'
import Wounds from '../include/clinical/Wounds'
import Progressnote from '../include/clinical/Progressnote'
import Assessments from '../include/clinical/Assessments'
import PerformAssessmentForm from '../include/clinical/PerformAssessmentForm'
import CarePlan from '../include/clinical/CarePlan'
import BasicInfo from '../form/BasicInfo'
import Contact from '../include/profile/Contact'
import NoteAddIcon from '@mui/icons-material/NoteAdd'
import Backdrop from '@mui/material/Backdrop'
import { makeStyles } from '@material-ui/core/styles'
import Tooltip from '@mui/material/Tooltip'
import { CButton } from '@coreui/react'
import TextField from '@mui/material/TextField'
import api from '../../../api/api'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}))

const useStyles = makeStyles(() => ({
  dialog: {
    height: 800,
  },
}))

console.count('Render')

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />
})

const BootstrapDialogTitle = (props) => {
  const { title, children, onClose, ...other } = props

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {title}
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  )
}

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
}

export default function AddNotePopup({ children }) {
  let newDate = new Date()
  let day = newDate.getDate()
  let month = newDate.getMonth() + 1
  let year = newDate.getFullYear()

  let newMonth = month < 10 ? '0' + month : month
  let newDay = day < 10 ? '0' + day : day

  let fullDate = year + '-' + newMonth + '-' + newDay

  console.log(fullDate)
  // states
  const [note, setNote] = useState('')
  const [date, setDate] = useState(fullDate)
  const [open, setOpen] = React.useState(false)
  const [scroll, setScroll] = React.useState('paper')
  const {
    currentTab,
    setCurrentTab,
    action,
    setAction,
    content,
    setContent,
    dialogClose,
    setDialogClose,
    activeTab,
    notesData,
    setNotesData,
    setActiveTab,
    popupChildren,
    setPopupChildren,
    setPopupTitle,
    popupTitle,
  } = useContext(AppContext)
  const [fullWidth, setFullWidth] = React.useState(true)
  const [maxWidth, setMaxWidth] = React.useState('lg')

  useEffect(() => {
    setOpen(false)
  }, [])

  const handleClickOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }

  let tenantName = localStorage.getItem('tenantName')
  const classes = useStyles()

  const addNotes = (e) => {
    e.preventDefault()

    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    const input = {
      date: date,
      description: note,
      ResidentId: tenantId,
    }

    // api call for adding new allergy tenant
    api
      .post(`/note/add`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        setNotesData([...notesData, result.data])
        // setInsuranceInfoData([...insuranceInfoData, result.data])
        // code for adding shiftlog summary
        // const shiftSummaryLogInput = {
        //   activity: `${name} added ${allergy} as a new allergy data to ${tenantName}`,
        //   residentName: tenantName,
        //   userName: name,
        //   ResidentId: tenantId,
        //   UserId: userId,
        // }
        // console.log(shiftSummaryLogInput)
        // api
        //   .post(`/shiftsummarylog/add`, shiftSummaryLogInput, {
        //     headers: {
        //       Authorization: `Bearer ${token}`,
        //     },
        //   })
        //   .then((result) => {
        //     // setDailylogData([...dailylogData, result.data])
        //     console.log(result)
        //   })
        //   .catch((err) => {
        //     console.error(err)
        //   })
        toast.success('Added Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
        // code for adding daily log
      })
      .catch((err) => {
        console.log(err)
      })
  }

  return (
    <div>
      <Tooltip
        title={<p style={{ fontSize: '15px', padding: '.2rem' }}>Add Progress Note</p>}
        placement="left"
        arrow
      >
        <NoteAddIcon onClick={handleClickOpen} />
      </Tooltip>
      {/* <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={open}
        style={{ backdropFilter: 'blur(5px)', pointerEvents: 'none' }}
      > */}
      <BootstrapDialog
        scroll={scroll}
        fullWidth={fullWidth}
        maxWidth={maxWidth}
        onClose={handleClose}
        TransitionComponent={Transition}
        aria-labelledby="customized-dialog-title"
        open={open}
        classes={{ paper: classes.dialog }}
      >
        <DialogTitle>Add Progress Note</DialogTitle>

        <DialogContent className="position-relative py-0" dividers>
          <form onSubmit={(e) => addNotes(e)}>
            <div id="addnote-holder" className="mb-4">
              <TextField
                required
                className="input"
                label="Notes"
                multiline
                rows={4}
                value={note}
                onChange={(e) => setNote(e.target.value)}
              />
            </div>
            <div>
              <input
                type="date"
                id="note-date"
                value={date}
                min={fullDate}
                onChange={(e) => setDate(e.target.value)}
              ></input>
            </div>
            <CButton type="submit" id="submit-btn5">
              Submit
            </CButton>
          </form>
        </DialogContent>

        <DialogActions>
          <IconButton
            aria-label="close"
            onClick={handleClose}
            sx={{
              position: 'absolute',
              right: 8,
              top: 8,
              color: (theme) => theme.palette.grey[500],
            }}
          >
            <CloseIcon />
          </IconButton>
        </DialogActions>
      </BootstrapDialog>
      {/* </Backdrop> */}
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  )
}
