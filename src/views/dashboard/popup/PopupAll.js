import React, { useEffect } from 'react'
import { useContext } from 'react'
import PropTypes from 'prop-types'
import Button from '@mui/material/Button'
import { styled } from '@mui/material/styles'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import DialogActions from '@mui/material/DialogActions'
import IconButton from '@mui/material/IconButton'
import CloseIcon from '@mui/icons-material/Close'
import Typography from '@mui/material/Typography'
import Backdrop from '@mui/material/Backdrop'
// global variable
import AppContext from '../../../AppContext'
import Slide from '@mui/material/Slide'
import AddIcon from '@mui/icons-material/Add'

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}))

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />
})

const BootstrapDialogTitle = (props) => {
  const { title, children, onClose, ...other } = props

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {title}
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  )
}

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
}

export default function CustomizedDialogs({ children, title }) {
  const [open, setOpen] = React.useState(false)
  const [scroll, setScroll] = React.useState('paper')
  const {
    currentTab,
    setCurrentTab,
    action,
    setAction,
    content,
    setContent,
    dialogClose,
    setDialogClose,
    activeTab,
    setActiveTab,
    isPopupOpen,
    setIsPopupOpen,
  } = useContext(AppContext)
  const [fullWidth, setFullWidth] = React.useState(true)
  const [maxWidth, setMaxWidth] = React.useState('xl')

  useEffect(() => {
    setOpen(false)
  }, [])

  // useEffect(() => {
  //   if (isPopupOpen) {
  //     setOpen(true)
  //   }
  // }, [isPopupOpen])

  const handleClickOpen = () => {
    setOpen(true)
    localStorage.setItem('popupIsOpen', true)
  }
  const handleClose = () => {
    setOpen(false)
    localStorage.setItem('popupIsOpen', false)
  }

  return (
    <div>
      <div id="popup-btn" variant="contained" onClick={handleClickOpen}>
        {/* <CallIcon /> */}
      </div>
      <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={open}
        style={{ backdropFilter: 'blur(5px)', pointerEvents: 'none' }}
      >
        <BootstrapDialog
          scroll={scroll}
          fullWidth={fullWidth}
          maxWidth={maxWidth}
          onClose={handleClose}
          TransitionComponent={Transition}
          aria-labelledby="customized-dialog-title"
          open={open}
        >
          <DialogContent className="position-relative" dividers>
            {children}
          </DialogContent>
          <DialogActions></DialogActions>
        </BootstrapDialog>
      </Backdrop>
    </div>
  )
}
