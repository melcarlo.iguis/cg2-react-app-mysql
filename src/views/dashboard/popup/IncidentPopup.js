import React, { useEffect, useState } from 'react'
import { useContext } from 'react'
import PropTypes from 'prop-types'
import Button from '@mui/material/Button'
import { styled } from '@mui/material/styles'
import noPhoto from '../../../assets/images/nophoto.jpg'
import Tooltip from '@mui/material/Tooltip'
import CheckIcon from '@mui/icons-material/Check'
import Dialog from '@mui/material/Dialog'
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'
import { DatePicker } from '@mui/x-date-pickers/DatePicker'
import Grid from '@mui/material/Grid'
import { Row, Col, Container, Form } from 'react-bootstrap'
import TextField from '@mui/material/TextField'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import DialogActions from '@mui/material/DialogActions'
import IconButton from '@mui/material/IconButton'
import AddBox from '@material-ui/icons/AddBox'
import CloseIcon from '@mui/icons-material/Close'
import Typography from '@mui/material/Typography'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import Select, { SelectChangeEvent } from '@mui/material/Select'
// global variable
import AppContext from '../../../AppContext'
import Slide from '@mui/material/Slide'
import AddIcon from '@mui/icons-material/Add'
import Wounds from '../include/clinical/Wounds'
import Progressnote from '../include/clinical/Progressnote'
import Assessments from '../include/clinical/Assessments'
import PerformAssessmentForm from '../include/clinical/PerformAssessmentForm'
import CarePlan from '../include/clinical/CarePlan'
import BasicInfo from '../form/BasicInfo'
import api from '../../../api/api'
import Contact from '../include/profile/Contact'
import { CButton } from '@coreui/react'
import Backdrop from '@mui/material/Backdrop'
import { makeStyles } from '@material-ui/core/styles'
// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}))

const useStyles = makeStyles(() => ({
  dialog: {
    height: 800,
  },
}))

console.count('Render')

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />
})

const BootstrapDialogTitle = (props) => {
  const { title, children, onClose, ...other } = props

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {title}
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  )
}

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
}

export default function CustomizedDialogs({ children }) {
  let newDate = new Date()
  let day = newDate.getDate()
  let month = newDate.getMonth() + 1
  let year = newDate.getFullYear()

  let newMonth = month < 10 ? '0' + month : month
  let newDay = day < 10 ? '0' + day : day

  let fullDate = year + '-' + newMonth + '-' + newDay
  // states for form
  const [date, setDate] = useState(fullDate)
  const [tenantStatement, setTenantStatement] = useState('')
  const [incidentClass, setIncidentClass] = useState('')
  const [location, setLocation] = useState('')
  const [incidentDescription, setIncidentDescription] = useState('')
  const [witness, setWitness] = useState('')
  const [firstAidGiven, setFirstAidGiven] = useState('')
  const [firstAidNote, setFirstAidNote] = useState('')
  const [isVitalTaken, setIsVitalTaken] = useState('')
  const [questionPainLvl, setQuestionPainLvl] = useState('')
  const [requestPhysician, setRequestPhysician] = useState('')

  // states for picture
  const [imageFile, setImageFile] = useState('')
  const [image, setImage] = useState('')
  const [open, setOpen] = React.useState(false)
  const [scroll, setScroll] = React.useState('paper')
  const {
    currentTab,
    setCurrentTab,
    action,
    setAction,
    content,
    setContent,
    dialogClose,
    setDialogClose,
    activeTab,
    setActiveTab,
    setIsPopupOpen,
    isInsurancePopupOpen,
    setIsInsurnacePopupOpen,
    incidentReportList,
    setIncidentReportList,
    popupChildren,
    setInsuranceInfoData,
    insuranceInfoData,
    setPopupChildren,
    setPopupTitle,
    popupTitle,
  } = useContext(AppContext)
  const [fullWidth, setFullWidth] = React.useState(true)
  const [maxWidth, setMaxWidth] = React.useState('lg')

  const handleFileInputChange = (e) => {
    const file = e.target.files[0]
    previewFile(file)
    setImageFile(file)
  }

  const previewFile = (file) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onloadend = () => {
      setImage(reader.result)
    }
  }

  useEffect(() => {
    setOpen(false)
  }, [])

  useEffect(() => {
    if (isInsurancePopupOpen) {
      setOpen(true)
    }
  }, [isInsurancePopupOpen])

  const handleClickOpen = () => {
    setOpen(true)
    localStorage.setItem('popupIsOpen', true)
  }
  const handleClose = () => {
    setOpen(false)
    setIsInsurnacePopupOpen(false)
    localStorage.setItem('popupIsOpen', false)
  }
  let tenantName = localStorage.getItem('tenantName')
  const classes = useStyles()

  const addIncidentReport = (e) => {
    e.preventDefault()
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')
    let userId = localStorage.getItem('userId')

    const input = {
      date: date,
      tenantStatement: tenantStatement,
      incidentClass: incidentClass,
      location: location,
      incidentDescription: incidentDescription,
      witness: witness,
      firstAidGiven: firstAidGiven,
      firstAidNote: firstAidNote,
      isVitalTaken: isVitalTaken,
      questionPainLvl: questionPainLvl,
      requestPhysician: requestPhysician,
      ResidentId: tenantId,
    }

    // api call for adding new allergy tenant
    api
      .post(`/incident/add`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        setIncidentReportList([...incidentReportList, result.data])

        toast.success('Added Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
        // code for adding daily log
      })
      .catch((err) => {
        console.log(err)
      })
  }

  return (
    <div>
      <div id="addInsuranceBtn3" variant="contained" onClick={handleClickOpen}>
        <AddBox />
      </div>
      <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={open}
        style={{ backdropFilter: 'blur(5px)', pointerEvents: 'none' }}
      >
        <BootstrapDialog
          scroll={scroll}
          fullWidth={fullWidth}
          maxWidth={maxWidth}
          onClose={handleClose}
          TransitionComponent={Transition}
          aria-labelledby="customized-dialog-title"
          open={open}
          classes={{ paper: classes.dialog }}
        >
          <DialogTitle>
            <p>Add New Incident Report</p>
          </DialogTitle>

          <DialogContent className="position-relative py-0" dividers>
            <form onSubmit={(e) => addIncidentReport(e)} className="mt-3">
              <Container fluid>
                <Grid container spacing={2}>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <DatePicker
                          value={date}
                          fullWidth
                          label="Date"
                          onChange={(newValue) => {
                            setDate(newValue.$d)
                          }}
                          renderInput={(params) => <TextField variant="standard" {...params} />}
                        />
                      </LocalizationProvider>
                    </div>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'blue' },
                        }}
                        id="standard-basic"
                        label="Tenant Statement"
                        variant="standard"
                        value={tenantStatement}
                        onChange={(e) => setTenantStatement(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'blue' },
                        }}
                        id="standard-basic"
                        label="Incident Class"
                        variant="standard"
                        value={incidentClass}
                        onChange={(e) => setIncidentClass(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'blue' },
                        }}
                        id="standard-basic"
                        label="Location"
                        variant="standard"
                        value={location}
                        onChange={(e) => setLocation(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'blue' },
                        }}
                        id="standard-basic"
                        label="Incident Description"
                        variant="standard"
                        value={incidentDescription}
                        onChange={(e) => setIncidentDescription(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'blue' },
                        }}
                        id="standard-basic"
                        label="Incident Description"
                        variant="standard"
                        value={incidentDescription}
                        onChange={(e) => setIncidentDescription(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'blue' },
                        }}
                        id="standard-basic"
                        label="Witness"
                        variant="standard"
                        value={witness}
                        onChange={(e) => setWitness(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'blue' },
                        }}
                        id="standard-basic"
                        label="First Aid"
                        variant="standard"
                        value={firstAidGiven}
                        onChange={(e) => setFirstAidGiven(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'blue' },
                        }}
                        id="standard-basic"
                        label="First Aid Note"
                        variant="standard"
                        value={firstAidNote}
                        onChange={(e) => setFirstAidNote(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'blue' },
                        }}
                        id="standard-basic"
                        label="Does Vital Taken"
                        variant="standard"
                        value={isVitalTaken}
                        onChange={(e) => setIsVitalTaken(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'blue' },
                        }}
                        id="standard-basic"
                        label="Pain Level"
                        variant="standard"
                        value={questionPainLvl}
                        onChange={(e) => setQuestionPainLvl(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'blue' },
                        }}
                        id="standard-basic"
                        label="Pain Level"
                        variant="standard"
                        value={questionPainLvl}
                        onChange={(e) => setQuestionPainLvl(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'blue' },
                        }}
                        id="standard-basic"
                        label="Request Physician"
                        variant="standard"
                        value={requestPhysician}
                        onChange={(e) => setRequestPhysician(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                </Grid>
                <Tooltip title="Save" placement="bottom">
                  <div id="submit-btn5">
                    <label htmlFor="submit-btn">
                      <CheckIcon />
                    </label>
                  </div>
                </Tooltip>
                <CButton type="submit" id="submit-btn" hidden>
                  Submit
                </CButton>
              </Container>
            </form>
          </DialogContent>
          <DialogActions>
            <IconButton
              aria-label="close"
              onClick={handleClose}
              sx={{
                position: 'absolute',
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500],
              }}
            >
              <CloseIcon />
            </IconButton>
          </DialogActions>
        </BootstrapDialog>
      </Backdrop>
      {/* <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      /> */}
    </div>
  )
}
