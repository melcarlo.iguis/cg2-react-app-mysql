import React, { useEffect, useState } from 'react'
import { useContext } from 'react'
import PropTypes from 'prop-types'
import Button from '@mui/material/Button'
import Tooltip from '@mui/material/Tooltip'
import { styled } from '@mui/material/styles'
import CheckIcon from '@mui/icons-material/Check'
import noPhoto from '../../../assets/images/nophoto.jpg'
import UploadFileIcon from '@mui/icons-material/UploadFile'
import Dialog from '@mui/material/Dialog'
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate'
import { Row, Col, Container, Form } from 'react-bootstrap'
import TextField from '@mui/material/TextField'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import DialogActions from '@mui/material/DialogActions'
import IconButton from '@mui/material/IconButton'
import AddBox from '@material-ui/icons/AddBox'
import CloseIcon from '@mui/icons-material/Close'
import Typography from '@mui/material/Typography'
// global variable
import AppContext from '../../../AppContext'
import Slide from '@mui/material/Slide'
import AddIcon from '@mui/icons-material/Add'
import Wounds from '../include/clinical/Wounds'
import Progressnote from '../include/clinical/Progressnote'
import Assessments from '../include/clinical/Assessments'
import PerformAssessmentForm from '../include/clinical/PerformAssessmentForm'
import CarePlan from '../include/clinical/CarePlan'
import BasicInfo from '../form/BasicInfo'
import api from '../../../api/api'
import Grid from '@mui/material/Grid'
import Contact from '../include/profile/Contact'
import { CButton } from '@coreui/react'
import Backdrop from '@mui/material/Backdrop'
import { makeStyles } from '@material-ui/core/styles'
// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}))

const useStyles = makeStyles(() => ({
  dialog: {
    height: 800,
  },
}))

console.count('Render')

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />
})

const BootstrapDialogTitle = (props) => {
  const { title, children, onClose, ...other } = props

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {title}
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  )
}

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
}

export default function CustomizedDialogs({ children }) {
  let newDate = new Date()
  let day = newDate.getDate()
  let month = newDate.getMonth() + 1
  let year = newDate.getFullYear()

  let newMonth = month < 10 ? '0' + month : month
  let newDay = day < 10 ? '0' + day : day

  let fullDate = year + '-' + newMonth + '-' + newDay
  // states for form
  const [InsuranceName, setInsuranceName] = useState('')
  const [PolicyNo, setPolicyNo] = useState('')
  const [GroupNo, setGroupNo] = useState('')
  const [Telephone, setTelephone] = useState('')
  const [ExpirationDate, setExpirationDate] = useState('')

  // states for picture
  const [imageFile, setImageFile] = useState('')
  const [image, setImage] = useState('')
  const [open, setOpen] = React.useState(false)
  const [scroll, setScroll] = React.useState('paper')
  const {
    currentTab,
    setCurrentTab,
    action,
    setAction,
    content,
    setContent,
    dialogClose,
    setDialogClose,
    activeTab,
    setActiveTab,
    setIsPopupOpen,
    isInsurancePopupOpen,
    setIsInsurnacePopupOpen,
    popupChildren,
    setInsuranceInfoData,
    insuranceInfoData,
    setPopupChildren,
    setPopupTitle,
    popupTitle,
  } = useContext(AppContext)
  const [fullWidth, setFullWidth] = React.useState(true)
  const [maxWidth, setMaxWidth] = React.useState('lg')
  const [fileName, setFileName] = useState('')

  const handleFileInputChange = (e) => {
    const file = e.target.files[0]
    previewFile(file)
    setImageFile(file)
    setFileName(file.name)
  }

  const previewFile = (file) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onloadend = () => {
      setImage(reader.result)
    }
  }

  useEffect(() => {
    setOpen(false)
  }, [])

  useEffect(() => {
    if (isInsurancePopupOpen) {
      setOpen(true)
    }
  }, [isInsurancePopupOpen])

  const handleClickOpen = () => {
    setOpen(true)
    localStorage.setItem('popupIsOpen', true)
  }
  const handleClose = () => {
    setOpen(false)
    setIsInsurnacePopupOpen(false)
    localStorage.setItem('popupIsOpen', false)
  }
  let tenantName = localStorage.getItem('tenantName')
  const classes = useStyles()

  const addInsuranceData = (e) => {
    e.preventDefault()
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')
    let userId = localStorage.getItem('userId')

    const formData = new FormData()

    formData.append('image', imageFile)
    formData.append('InsuranceName', InsuranceName)
    formData.append('PolicyNo', PolicyNo)
    formData.append('GroupNo', GroupNo)
    formData.append('Telephone', Telephone)
    formData.append('ExpirationDate', ExpirationDate)
    formData.append('ResidentId', tenantId)
    formData.append('creator', name)

    // api call for adding new allergy tenant
    api
      .post(`/insuranceinfo/add`, formData, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        setInsuranceInfoData([...insuranceInfoData, result.data])

        toast.success('Added Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
        // code for adding daily log
      })
      .catch((err) => {
        console.log(err)
      })
  }

  return (
    <div>
      <div id="addInsuranceBtn" variant="contained" onClick={handleClickOpen}>
        <AddBox />
      </div>
      <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={open}
        style={{ backdropFilter: 'blur(5px)', pointerEvents: 'none' }}
      >
        <BootstrapDialog
          scroll={scroll}
          fullWidth={fullWidth}
          maxWidth={maxWidth}
          onClose={handleClose}
          TransitionComponent={Transition}
          aria-labelledby="customized-dialog-title"
          open={open}
          classes={{ paper: classes.dialog }}
        >
          <DialogTitle>
            <p>Add New Insurance</p>
          </DialogTitle>

          <DialogContent className="position-relative py-0" dividers>
            <form onSubmit={(e) => addInsuranceData(e)}>
              <Container fluid>
                <Grid container spacing={2}>
                  <Grid item md="12" sm="12" className="mx-auto text-center">
                    <Grid container>
                      <Grid item md="6" sm="6" className="mx-auto text-center mt-4 main-holder">
                        <div className="file-upload-holder">
                          <Tooltip title="Upload File" placement="right">
                            <label id="input-label" htmlFor="input-file">
                              <UploadFileIcon />
                            </label>
                          </Tooltip>
                          <Form.Control
                            required
                            hidden
                            id="input-file"
                            type="file"
                            // value={fileInputState}
                            onChange={handleFileInputChange}
                          />
                          <label className="d-block">{fileName ? fileName : ''}</label>
                          <label>Attachment File</label>
                        </div>
                      </Grid>
                    </Grid>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'black' },
                        }}
                        id="standard-basic"
                        label="Insurance Name"
                        variant="standard"
                        value={InsuranceName}
                        onChange={(e) => setInsuranceName(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'black' },
                        }}
                        id="standard-basic"
                        label="Policy Number"
                        variant="standard"
                        value={PolicyNo}
                        onChange={(e) => setPolicyNo(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'black' },
                        }}
                        id="standard-basic"
                        label="Group Number"
                        variant="standard"
                        value={GroupNo}
                        onChange={(e) => setGroupNo(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="6" className="main-holder">
                    <div className="holder">
                      <TextField
                        fullWidth
                        InputProps={{
                          style: { fontSize: '15px', margin: '10px 0', color: 'black' },
                        }}
                        id="standard-basic"
                        label="Telephone Number"
                        variant="standard"
                        value={Telephone}
                        onChange={(e) => setTelephone(e.target.value)}
                        required
                      />
                    </div>
                  </Grid>
                  <Grid item md="4" sm="6" className="d-flex main-holder">
                    <div className="holder">
                      <Grid container spacing={2}>
                        <Grid item md="4">
                          <label>Expiration Date:</label>
                        </Grid>
                        <Grid item md="8">
                          <input
                            type="date"
                            id="start"
                            value={ExpirationDate}
                            min={fullDate}
                            style={{ width: '100%', height: '45px' }}
                            onChange={(e) => setExpirationDate(e.target.value)}
                            required
                          ></input>
                        </Grid>
                      </Grid>
                    </div>
                  </Grid>
                </Grid>
                <Tooltip title="Save" placement="bottom">
                  <div id="submit-btn5">
                    <label htmlFor="submit-btn">
                      <CheckIcon />
                    </label>
                  </div>
                </Tooltip>
                <CButton type="submit" id="submit-btn" hidden>
                  Submit
                </CButton>
              </Container>
            </form>
          </DialogContent>
          <DialogActions>
            <IconButton
              aria-label="close"
              onClick={handleClose}
              sx={{
                position: 'absolute',
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500],
              }}
            >
              <CloseIcon />
            </IconButton>
          </DialogActions>
        </BootstrapDialog>
      </Backdrop>
      {/* <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      /> */}
    </div>
  )
}
