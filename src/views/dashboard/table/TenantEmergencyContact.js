import React, { useState, useEffect, useContext } from 'react'
import { Row, Col, Form, Button, Table } from 'react-bootstrap'
import AddIcon from '@mui/icons-material/Add'
import VolunteerActivismIcon from '@mui/icons-material/VolunteerActivism'
import NoteAltIcon from '@mui/icons-material/NoteAlt'
import InsertChartIcon from '@mui/icons-material/InsertChart'
import DriveFileMoveIcon from '@mui/icons-material/DriveFileMove'
import ThumbUpAltIcon from '@mui/icons-material/ThumbUpAlt'
import LocalPharmacyIcon from '@mui/icons-material/LocalPharmacy'
import PersonSearchIcon from '@mui/icons-material/PersonSearch'
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn'
import ContentPasteSearchIcon from '@mui/icons-material/ContentPasteSearch'
import FindInPageIcon from '@mui/icons-material/FindInPage'

// global variable
import AppContext from '../../../AppContext'

// axios api
import api from '../../../api/api'

function TenantEmergencyContact(data) {
  const [age, setAge] = useState('')
  const {
    allergyTypeList,
    setAllergyTypeList,
    historyList,
    setHistoryList,
    allergiesList,
    setAllergiesList,
    currentTab,
    setCurrentTab,
  } = useContext(AppContext)

  let fullName = data.data.firstName + ' ' + data.data.middleName + ' ' + data.data.lastName

  useEffect(() => {
    // setCurrentTab('provide service')
    localStorage.setItem('tenantId', data.data._id)
    localStorage.setItem('tenantName', fullName)
  }, [])

  const cutBirthday = (string) => {
    let date = new Date(string)
    const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
    const day = date.getUTCDate()
    const year = date.getUTCFullYear()

    let fullDate = month + ' ' + day + ', ' + year
    return fullDate
  }

  const getTheAge = (birthday) => {
    let date = new Date(birthday)
    let currentYear = new Date().getFullYear()
    let year = date.getUTCFullYear()

    const age = currentYear - year

    return age
  }

  return (
    <div>
      <Row>
        <Col md="12" sm="12" className="mx-auto mb-3 colItem mt-3">
          <Row>
            <Col md="2">
              <img id="tenantPic" className="ml-3" src={data.data.picture} alt=""></img>
            </Col>
            <Col md="8" className="d-flex">
              <table id="studInfoTable">
                <tr>
                  <th className="pr-5">Name:</th>
                  <td className="pr-5">{fullName}</td>
                </tr>
                <tr>
                  <th className="pr-5">Age:</th>
                  <td className="pr-5">{getTheAge(data.data.birthday)}</td>
                </tr>
              </table>
            </Col>
          </Row>
        </Col>
        <Col md="12">
          <div id="hr-line" className="mb-3 mt-3"></div>
        </Col>

        <Col md="6" className="mx-auto">
          <h4>Contact Informations</h4>
          <table id="studInfoTable">
            <tr>
              <th className="pr-5">Person To Contact:</th>
              <td className="pr-5">
                {data.data.contactDetail[0].firstName} {data.data.contactDetail[0].middleName}{' '}
                {data.data.contactDetail[0].lastName}
              </td>
            </tr>
            <tr>
              <th className="pr-5">Contact Number:</th>
              <td className="pr-5">{data.data.contactDetail[0].contactNo}</td>
            </tr>
            <tr>
              <th className="pr-5">Address:</th>
              <td className="pr-5">{data.data.contactDetail[0].address}</td>
            </tr>
          </table>
        </Col>
        <Col md="6" className="mx-auto">
          <h4>Emergency Instructions</h4>
          <table id="studInfoTable">
            <tr>
              <th className="pr-5">Insert Data here:</th>
              <td className="pr-5">Insert Data here</td>
            </tr>
            <tr>
              <th className="pr-5">Insert Data here:</th>
              <td className="pr-5">Insert Data Here</td>
            </tr>
            <tr>
              <th className="pr-5">Insert Data here:</th>
              <td className="pr-5">Insert Data Here</td>
            </tr>
          </table>
        </Col>
      </Row>
    </div>
  )
}

export default TenantEmergencyContact
