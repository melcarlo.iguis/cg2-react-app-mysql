import React, { useContext, useEffect, useState } from 'react'
import { Row, Col, Table } from 'react-bootstrap'
import AppContext from '../../../AppContext'
import InputLabel from '@mui/material/InputLabel'
import FormControl from '@mui/material/FormControl'
import NativeSelect from '@mui/material/NativeSelect'
import { Grid, Box, CardActionArea } from '@mui/material'
import { DataGrid } from '@mui/x-data-grid'
// axios api
import api from '../../../api/api'

import Popup from '../popup/Popup'
import AddNewAllergyForm from '../form/AddNewAllergyForm'
import Swal from 'sweetalert2'
import EditPopup from '../popup/EditPopup'
import EditBehaviorForm from '../form/EditBehaviorForm'

// Icons
import SearchIcon from '@mui/icons-material/Search'
import InputBase from '@material-ui/core/InputBase'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'
import AttachmentIcon from '@mui/icons-material/Attachment'
import EditIcon from '@mui/icons-material/Edit'

function BehaviorTable() {
  const { behaviorData, setBehaviorData, setDiagnosisData, diagnosisData } = useContext(AppContext)

  const [isFetchDone, setIsFetchDone] = useState(false)
  const columns = [
    {
      field: 'behavior',
      headerName: 'Behavior',
      flex: 2.5,
      minWidth: 250,
    },
    {
      field: 'startDate',
      headerName: 'Start Date',
      minWidth: 150,
      sortable: false,
      flex: 0.5,
      valueGetter: (params) => {
        return formatDate(params.row.startDate)
      },
    },

    {
      field: 'location',
      headerName: 'Location',
      flex: 1.5,
      minWidth: 250,
    },
    {
      field: 'alterable',
      headerName: 'Alterable',
      minWidth: 150,
      flex: 0.5,
    },
    {
      field: 'riskTo',
      headerName: 'Risk To',
      minWidth: 180,
      sortable: false,
    },
    {
      field: 'triggerBy',
      headerName: 'Trigger By',
      flex: 1,
      minWidth: 200,
    },
    {
      field: 'review',
      headerName: 'Review',
      sortable: false,
      flex: 1,
      minWidth: 250,
    },
    {
      field: 'action',
      headerName: 'Action',
      sortable: false,
      flex: 1,
      minWidth: 80,
      renderCell: (params) => (
        <div className="d-flex">
          <EditPopup>
            {' '}
            <EditBehaviorForm data={params.row} />
          </EditPopup>
          <div
            id="delete-btn"
            className="btn d-flex"
            onClick={(e) => deleteBehavior(e, params.row.id, params.row.allergy)}
          >
            <DeleteForeverIcon />
          </div>
        </div>
      ),
    },
  ]

  const formatDate = (string) => {
    if (string === undefined) {
      return ''
    } else {
      let date = new Date(string)
      const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
      const day = date.getUTCDate()
      const year = date.getUTCFullYear()

      let fullDate = month + ' ' + day + ', ' + year
      return fullDate
    }
  }

  const deleteBehavior = (e, id, allergy) => {
    e.preventDefault()
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')
    console.log(id)
    console.log(tenantId)
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        api
          .put(`/behavior/${id}/delete/`)
          .then((result) => {
            console.log(result)
            const newArr = [...behaviorData]
            newArr.forEach((item, index) => {
              if (item.id === id) {
                newArr.splice(index, 1)
              }
            })
            setBehaviorData(newArr)
            Swal.fire('Deleted!', 'The Data has been deleted.', 'success')
            // adding history to database
            const input2 = {
              title: `Deleted ${tenantName}'s allergy data on ${allergy}`,
              tenantName: tenantName,
              tenantId: tenantId,
              userName: name,
            }
            api
              .post(`/history/create/`, input2, {
                headers: {
                  Authorization: `Bearer ${token}`,
                },
              })
              .then((result) => {})
              .catch((err) => {
                console.error(err)
              })
          })
          .catch((err) => {
            Swal.fire({
              title: 'fail to delete',
              icon: 'error',
            })
          })
      }
    })
  }

  let tenantId = localStorage.getItem('tenantId')
  // code for fetching allergy
  useEffect(() => {
    fetchBehaviorData()
  }, [tenantId])

  const fetchBehaviorData = () => {
    let token = localStorage.getItem('token')
    api
      .get(`/behavior/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        const newFilter = res.data.filter((val) => {
          return val.isActive === true
        })
        setBehaviorData(newFilter)
        setIsFetchDone(true)
      })
  }

  return (
    <div className="mt-3 mb-3" id="datagrid-container">
      <Col id="datagrid2" md="12" className="mx-auto mb-3 mt-5">
        {behaviorData.length > 0 && isFetchDone == true ? (
          <DataGrid
            getRowId={(row) => row.id}
            rows={behaviorData}
            columns={columns}
            pageSize={5}
            rowsPerPageOptions={[5]}
          />
        ) : (
          <p>No Data Available</p>
        )}
      </Col>
    </div>
  )
}

export default BehaviorTable
