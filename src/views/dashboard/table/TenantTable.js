import React, { useContext, useEffect, useState } from 'react'
import { Row, Col, Table } from 'react-bootstrap'
import AppContext from '../../../AppContext'
// component
import TenantEmergencyContactPopup from '../popup/TenantEmergencyContactPopup'
import TenantEmergencyContact from './TenantEmergencyContact'

// MUI
import InputBase from '@material-ui/core/InputBase'

import SearchIcon from '@mui/icons-material/Search'
// axios api
import api from '../../../api/api'

import TextField from '@mui/material/TextField'
import Autocomplete from '@mui/material/Autocomplete'

function TenantTable() {
  // state for search word
  const [wordEntered, setWordEntered] = useState('')

  const { isEmergencyOpen, tenantList, setTenantList, sideBarShowing, setSidebarShowing } =
    useContext(AppContext)

  const [chars, setChars] = useState([])

  useEffect(() => {
    let newState = tenantList.map((e) => e.firstName + ' ' + e.lastName) // map your state here
    setChars(newState) // and then update the state
  }, [])

  useEffect(() => {}, [])

  console.log(chars)

  useEffect(() => {
    // To refetch the tenant's list after closing the emergency window
    if (isEmergencyOpen === false) {
      let token = localStorage.getItem('token')
      api
        .get('/tenants/fetch', {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          setTenantList(res.data)
        })
    }
  }, [isEmergencyOpen])

  const cutBirthday = (string) => {
    let date = new Date(string)
    const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
    const day = date.getUTCDate()
    const year = date.getUTCFullYear()

    let fullDate = month + ' ' + day + ', ' + year
    return fullDate
  }

  const getTheAge = (birthday) => {
    let date = new Date(birthday)
    let currentYear = new Date().getFullYear()
    let year = date.getUTCFullYear()

    const age = currentYear - year

    return age
  }

  // function for search input
  const handleFilter = async (event) => {
    const searchWord = event.target.value
    setWordEntered(searchWord)

    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    await api
      .get('/tenants/fetch', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setTenantList(res.data)

        const newFilter = res.data.filter((value) => {
          let fullName = value.firstName + ' ' + value.middleName + ' ' + value.lastName

          return fullName.toLowerCase().includes(searchWord.toLowerCase())
        })
        setTenantList(newFilter)
      })
  }

  return (
    <div>
      {/*<Autocomplete
		      disablePortal
		      id="combo-box-demo"
		      options={chars}
		      sx={{ width: 300 }}
		      renderInput={(params) => <TextField {...params} label="Resident Name" />}
		    />*/}
      <Row className="d-flex justify-content-center">
        <Col md="12">
          <div id="search-bar-container">
            <InputBase
              placeholder="Search…"
              value={wordEntered}
              onChange={handleFilter}
              inputProps={{ 'aria-label': 'search' }}
              endAdornment={<SearchIcon style={{ fontSize: 25 }} className="pr-3" />}
              id="searchBar"
            />
          </div>
        </Col>
        <Col md="10">
          <Table hover className="table" responsive>
            <thead>
              <tr>
                <th> </th>
                <th>Name</th>
                <th>Age</th>
                <th>Status</th>
                <th>Birthday</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {tenantList.map((val, key) => {
                return (
                  <tr key={key}>
                    <td>
                      <img id="tenantPic" src={val.picture} alt=""></img>
                    </td>
                    <td>
                      {val.firstName} {val.middleName} {val.lastName}
                    </td>
                    <td>{getTheAge(val.birthday)}</td>
                    <td>{val.status}</td>
                    <td>{cutBirthday(val.birthday)}</td>
                    <td>
                      <TenantEmergencyContactPopup>
                        <TenantEmergencyContact data={val} />
                      </TenantEmergencyContactPopup>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </Table>
        </Col>
      </Row>
    </div>
  )
}

export default TenantTable
