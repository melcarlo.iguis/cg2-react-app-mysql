import React, { useState, useContext, forwardRef, useRef, useEffect } from 'react'
import MaterialTable from 'material-table'
import AppContext from '../../../AppContext'
import AddBox from '@material-ui/icons/AddBox'
import ArrowDownward from '@material-ui/icons/ArrowDownward'
import Check from '@material-ui/icons/Check'
import ChevronLeft from '@material-ui/icons/ChevronLeft'
import ChevronRight from '@material-ui/icons/ChevronRight'
import Clear from '@material-ui/icons/Clear'
import DeleteOutline from '@material-ui/icons/DeleteOutline'
import Edit from '@material-ui/icons/Edit'
import FilterList from '@material-ui/icons/FilterList'
import FirstPage from '@material-ui/icons/FirstPage'
import LastPage from '@material-ui/icons/LastPage'
import Remove from '@material-ui/icons/Remove'
import SaveAlt from '@material-ui/icons/SaveAlt'
import Search from '@material-ui/icons/Search'
import ViewColumn from '@material-ui/icons/ViewColumn'
import EmojiEmotionsIcon from '@mui/icons-material/EmojiEmotions'
// axios api
import api from '../../../api/api'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function EDocuments() {
  const {
    allergiesList,
    setAllergiesList,
    currentTab,
    setCurrentTab,
    activeTab,
    setActiveTab,
    dailylogData,
    setDailylogData,
    eDocumentsData,
    setEDocumentsData,
  } = useContext(AppContext)
  const [data, setData] = useState([])

  const formatDate = (string) => {
    if (string === undefined || string === '' || string === null) {
      return string
    } else {
      let date = new Date(string)
      const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
      const day = date.getUTCDate() + 1
      const year = date.getUTCFullYear()

      let fullDate = month + ' ' + day + ', ' + year
      return fullDate
    }
  }

  const [imageFile, setImageFile] = useState('')
  const [imageName, setImageName] = useState('')

  const handleFileInputChange = (e) => {
    console.log(e)
    const file = e.target.files[0]
    setImageName(file.name)
    setImageFile(file)
  }

  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  }

  const categoryLookup = {
    Clinical: 'Clinical',
    Legal: 'Legal',
    Miscellaneous: 'Miscellaneous',
    Photo: 'Photo',
  }

  const columns = [
    { title: 'Name', field: 'name' },
    {
      title: 'Category',
      field: 'category',
      lookup: categoryLookup,
    },
    {
      title: 'Files',
      field: 'files',
      render: (rowData) => (
        <a href={rowData.files} target="_blank">
          View Files
        </a>
      ),
      editComponent: () => {
        return (
          <>
            <input
              type="file"
              id="img"
              onChange={handleFileInputChange}
              style={{ display: 'none' }}
            />
            <label className="btn" for="img">
              {imageName == '' ? 'Upload Image' : imageName}
            </label>
          </>
        )
      },
    },
  ]

  const addAllergy = (data) => {
    console.log(data)
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')
    let userId = localStorage.getItem('userId')

    console.log(imageFile)
    const formData = new FormData()

    formData.append('image', imageFile)
    formData.append('name', data.name)
    formData.append('category', data.category)
    formData.append('ResidentId', tenantId)

    console.log(formData)

    // api call for adding new allergy tenant
    api
      .post(`/edocuments/add`, formData, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        setEDocumentsData([...eDocumentsData, result.data])
        // code for adding shiftlog summary
        // const shiftSummaryLogInput = {
        //   activity: `${name} added ${allergy} as a new allergy data to ${tenantName}`,
        //   residentName: tenantName,
        //   userName: name,
        //   ResidentId: tenantId,
        //   UserId: userId,
        // }
        // console.log(shiftSummaryLogInput)
        // api
        //   .post(`/shiftsummarylog/add`, shiftSummaryLogInput, {
        //     headers: {
        //       Authorization: `Bearer ${token}`,
        //     },
        //   })
        //   .then((result) => {
        //     // setDailylogData([...dailylogData, result.data])
        //     console.log(result)
        //   })
        //   .catch((err) => {
        //     console.error(err)
        //   })
        toast.success('Added Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
        // code for adding daily log
        const dailyLogInput = {
          title: `${name} added ${data.allergy} as a new allergy data to ${tenantName}`,
          residentName: tenantName,
          userName: name,
          ResidentId: tenantId,
        }
        api
          .post(`/dailylog/add`, dailyLogInput, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((result) => {
            setDailylogData([...dailylogData, result.data])
            console.log(result)
          })
          .catch((err) => {
            console.error(err)
          })
      })
      .catch((err) => {
        console.log(err)
      })
  }

  const deleteAllergy = (id) => {
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    console.log(id)

    api
      .put(`/edocuments/${id}/delete`)
      .then((result) => {
        console.log(result)

        const newArr = [...eDocumentsData]
        newArr.forEach((item, index) => {
          if (item.id === id) {
            newArr.splice(index, 1)
          }
        })

        setEDocumentsData(newArr)

        // adding history to database
        // const input2 = {
        //   title: `Deleted ${tenantName}'s allergy data on ${allergy}`,
        //   tenantName: tenantName,
        //   tenantId: tenantId,
        //   userName: name,
        // }
        // api
        //   .post(`/history/create/`, input2, {
        //     headers: {
        //       Authorization: `Bearer ${token}`,
        //     },
        //   })
        //   .then((result) => {})
        //   .catch((err) => {
        //     console.error(err)
        //   })
        toast.success('Deleted Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
      })
      .catch((err) => {
        Swal.fire({
          title: 'fail to delete',
          icon: 'error',
        })
      })
  }

  const updateAllergy = (data, id) => {
    console.log(data)
    console.log(id)
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    const input = {
      name: data.name,
      category: data.category,
    }

    // api call for editing allergy data
    api
      .put(`/allergy/${id}/update`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        // updating the tenant's allergy data state
        eDocumentsData.forEach((item) => {
          if (item.id === id) {
            item.name = data.name
            item.category = data.category
          }
        })

        setEDocumentsData([...eDocumentsData])

        // adding history to database
        // const input2 = {
        //   title: `Edited ${tenantName}'s allergy data on ${allergy}`,
        //   tenantName: tenantName,
        //   tenantId: tenantId,
        //   userName: name,
        // }
        // api
        //   .post(`/history/create/`, input2, {
        //     headers: {
        //       Authorization: `Bearer ${token}`,
        //     },
        //   })
        //   .then((result) => {})
        //   .catch((err) => {
        //     console.error(err)
        //   })
        toast.success('Updated Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
      })
      .catch((err) => {
        console.log(err)
      })
  }

  // useEffect(() => {
  //   setActiveTab(parseInt(activeTabVal))
  // }, [])

  return (
    <div style={{ maxWidth: '100%' }}>
      <MaterialTable
        icons={tableIcons}
        title="eDocuments"
        columns={columns}
        data={eDocumentsData}
        editable={{
          onRowAdd: (newRow) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                addAllergy(newRow)
                setEDocumentsData([...eDocumentsData, newRow])
                resolve()
              }, 2000)
            }),
          onRowDelete: (selectedRow) =>
            new Promise((resolve, reject) => {
              console.log(selectedRow)
              setTimeout(() => {
                deleteAllergy(selectedRow.id)
                resolve()
              }, 2000)
            }),
          onRowUpdate: (updatedRow, oldRow) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                updateAllergy(updatedRow, oldRow.id)
                resolve()
              }, 2000)
            }),
        }}
        options={{
          search: false,
          actionsColumnIndex: -1,
          addRowPosition: 'first',
        }}
      />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  )
}

export default EDocuments
