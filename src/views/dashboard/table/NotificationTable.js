import React, { useContext, useEffect, useState } from 'react'
import { Row, Col, Table } from 'react-bootstrap'
import AppContext from '../../../AppContext'
import InputLabel from '@mui/material/InputLabel'
import FormControl from '@mui/material/FormControl'
import NativeSelect from '@mui/material/NativeSelect'
import AssignmentTurnedInIcon from '@mui/icons-material/AssignmentTurnedIn'
import { Grid, Box, CardActionArea } from '@mui/material'
import { DataGrid } from '@mui/x-data-grid'
// axios api
import api from '../../../api/api'

import Popup from '../popup/Popup'
import AddNewAllergyForm from '../form/AddNewAllergyForm'
import Swal from 'sweetalert2'
import EditPopup from '../popup/EditPopup'
import EditBehaviorForm from '../form/EditBehaviorForm'

// Icons
import SearchIcon from '@mui/icons-material/Search'
import InputBase from '@material-ui/core/InputBase'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'
import AttachmentIcon from '@mui/icons-material/Attachment'
import EditIcon from '@mui/icons-material/Edit'

// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function NotificationTable() {
  const {
    behaviorData,
    setBehaviorData,
    setDiagnosisData,
    diagnosisData,
    notificationData,
    setNotificationData,
    notifTableData,
    setNotifTableData,
  } = useContext(AppContext)

  const [isFetchDone, setIsFetchDone] = useState(false)

  // function for marking the notification as done
  const markAsDone = () => {
    const input = {
      priorityLevel: notificationData[notificationData.length - 1].priorityLevel - 1,
      isCompleted: true,
      dateAccomplish: new Date().toLocaleString(),
    }

    // api call for adding new tenant
    let id = localStorage.getItem('notificationId')
    let token = localStorage.getItem('token')
    api
      .put(`/notification/${id}/update`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        // updating the data on the state
        notificationData.forEach((item) => {
          if (item.id == id) {
            item.priorityLevel = notificationData[notificationData.length - 1].priorityLevel - 1
            item.isCompleted = true
            item.dateAccomplish = new Date().toLocaleString()
          }
        })

        setNotificationData([...notificationData])
        console.log(notificationData)

        toast.success('Updated Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
      })
      .catch((err) => {
        console.log(err)
      })
  }

  //   function for fetching notification data
  const fetchSpecificNotifData = async () => {
    console.log('hehe')
    let id = localStorage.getItem('notificationId')
    let token = localStorage.getItem('token')
    await api
      .get(`/notification/${id}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res)
        setNotifTableData(res.data)
        setIsFetchDone(true)
      })
      .catch((err) => {
        console.error(err)
      })
  }

  useEffect(() => {
    fetchSpecificNotifData()
  }, [])

  console.log(notifTableData)

  const columns = [
    // {
    //   field: 'date',
    //   headerName: 'Date Created',
    //   minWidth: 150,
    //   sortable: false,
    //   flex: 0.5,
    //   valueGetter: (params) => {
    //     return formatDate(params.row.date)
    //   },
    // },
    {
      field: 'title',
      headerName: 'Title',
      flex: 2.5,
      minWidth: 250,
    },
    {
      field: 'tenantName',
      headerName: 'Tenant Involve',
      minWidth: 150,
      flex: 1.5,
    },

    {
      field: 'action',
      headerName: 'Action',
      sortable: false,
      flex: 1,
      minWidth: 80,
      renderCell: (params) => (
        <div id={params.row.isCompleted ? 'disable' : ''}>
          <div
            id="done-btn"
            onClick={(e) => markAsDone(e)}
            className="btn d-flex"
            style={{ padding: '0px' }}
          >
            Done
            {/* <AssignmentTurnedInIcon /> mark as done */}
          </div>
        </div>
      ),
    },
  ]

  const formatDate = (string) => {
    if (string === undefined) {
      return ''
    } else {
      let date = new Date(string)
      const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
      const day = date.getUTCDate()
      const year = date.getUTCFullYear()

      let fullDate = month + ' ' + day + ', ' + year
      return fullDate
    }
  }

  let tenantId = localStorage.getItem('tenantId')
  // code for fetching allergy
  useEffect(() => {
    fetchBehaviorData()
  }, [tenantId])

  const fetchBehaviorData = () => {
    let token = localStorage.getItem('token')
    api
      .get(`/behavior/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        const newFilter = res.data.filter((val) => {
          return val.isActive === true
        })
        setBehaviorData(newFilter)
        setIsFetchDone(true)
      })
  }

  return (
    <div className="mt-3 mb-3" id="datagrid-container-single-item">
      <Col id="datagrid2" md="12" className="mx-auto mb-3 mt-5">
        {notifTableData.length > 0 && isFetchDone == true ? (
          <DataGrid
            getRowId={(row) => row.id}
            rows={notifTableData}
            columns={columns}
            pageSize={5}
            rowsPerPageOptions={[5]}
          />
        ) : (
          <p>No Data Available</p>
        )}
      </Col>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  )
}

export default NotificationTable
