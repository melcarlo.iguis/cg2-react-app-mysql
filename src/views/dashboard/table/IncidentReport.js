import React, { useState, useContext, forwardRef, useRef, useEffect } from 'react'
import MaterialTable from 'material-table'
import AppContext from '../../../AppContext'
import AddBox from '@material-ui/icons/AddBox'
import ArrowDownward from '@material-ui/icons/ArrowDownward'
import Check from '@material-ui/icons/Check'
import ChevronLeft from '@material-ui/icons/ChevronLeft'
import ChevronRight from '@material-ui/icons/ChevronRight'
import Clear from '@material-ui/icons/Clear'
import DeleteOutline from '@material-ui/icons/DeleteOutline'
import Edit from '@material-ui/icons/Edit'
import FilterList from '@material-ui/icons/FilterList'
import FirstPage from '@material-ui/icons/FirstPage'
import LastPage from '@material-ui/icons/LastPage'
import Remove from '@material-ui/icons/Remove'
import SaveAlt from '@material-ui/icons/SaveAlt'
import Search from '@material-ui/icons/Search'
import ViewColumn from '@material-ui/icons/ViewColumn'
import EmojiEmotionsIcon from '@mui/icons-material/EmojiEmotions'
// axios api
import api from '../../../api/api'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import IncidentPopup from '../popup/IncidentPopup'

function IncidentReport() {
  const {
    allergiesList,
    setAllergiesList,
    insuranceInfoData,
    setInsuranceInfoData,
    currentTab,
    setCurrentTab,
    activeTab,
    setActiveTab,
    dailylogData,
    setDailylogData,
    incidentReportList,
    setIncidentReportList,
    isInsurancePopupOpen,
    setIsInsurnacePopupOpen,
  } = useContext(AppContext)
  const [data, setData] = useState([])

  const formatDate = (string) => {
    if (string === undefined || string === '' || string === null) {
      return string
    } else {
      let date = new Date(string)
      const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
      const day = date.getUTCDate() + 1
      const year = date.getUTCFullYear()

      let fullDate = month + ' ' + day + ', ' + year
      return fullDate
    }
  }

  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  }

  const typeLookup = {
    1: 'Insurance type 1',
    2: 'Insurance type 2',
    3: 'Insurance type 3',
  }

  const TitleImg = (rowData) => {
    return (
      <div>
        <p>gegege</p>
        <input type="file" />
      </div>
    )
  }

  const columns = [
    {
      title: 'Incident Description',
      field: 'incidentDescription',
      cellStyle: {
        minWidth: 200,
      },
    },
    {
      title: 'Location',
      field: 'location',
      // lookup: typeLookup,
    },
    {
      title: 'Incident Class',
      field: 'incidentClass',
      cellStyle: {
        minWidth: 200,
      },
    },
    {
      title: 'Tenant Statement',
      field: 'tenantStatement',
      cellStyle: {
        minWidth: 200,
      },
    },
    {
      title: 'Witness',
      field: 'witness',
    },
    {
      title: 'First Aid Given',
      field: 'firstAidGiven',
      cellStyle: {
        minWidth: 200,
      },
    },
    {
      title: 'First Aid Note',
      field: 'firstAidNote',
      cellStyle: {
        minWidth: 200,
      },
    },
    {
      title: 'Vital Taken',
      field: 'isVitalTaken',
      cellStyle: {
        minWidth: 200,
      },
    },
    {
      title: 'Pain Level',
      field: 'questionPainLvl',
      cellStyle: {
        minWidth: 200,
      },
    },
    {
      title: 'Request a Physician',
      field: 'requestPhysician',
      cellStyle: {
        minWidth: 200,
      },
    },

    {
      title: 'Date',
      field: 'date',
      type: 'date',
      dateSetting: { locale: 'ko-KR' },
      render: (rowData) => formatDate(rowData.ExpirationDate),
    },
    { title: 'Created by', field: 'creator' },
    {
      title: 'Created Date',
      field: 'createdAt',
      type: 'datetime',

      // dateSetting: { locale: 'ko-KR' },
      // render: (rowData) => formatDate(rowData.createdAt),
    },
  ]

  const addData = (data) => {
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')
    let userId = localStorage.getItem('userId')

    const input = {
      name: data.name,
      type: data.type,
      coverageAmount: data.coverageAmount,
      ResidentId: tenantId,
    }

    // api call for adding new allergy tenant
    api
      .post(`/insuranceinfo/add`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        setInsuranceInfoData([...insuranceInfoData, result.data])
        // code for adding shiftlog summary
        // const shiftSummaryLogInput = {
        //   activity: `${name} added ${allergy} as a new allergy data to ${tenantName}`,
        //   residentName: tenantName,
        //   userName: name,
        //   ResidentId: tenantId,
        //   UserId: userId,
        // }
        // console.log(shiftSummaryLogInput)
        // api
        //   .post(`/shiftsummarylog/add`, shiftSummaryLogInput, {
        //     headers: {
        //       Authorization: `Bearer ${token}`,
        //     },
        //   })
        //   .then((result) => {
        //     // setDailylogData([...dailylogData, result.data])
        //     console.log(result)
        //   })
        //   .catch((err) => {
        //     console.error(err)
        //   })
        toast.success('Added Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
        // code for adding daily log
        const dailyLogInput = {
          title: `${name} added ${data.allergy} as a new allergy data to ${tenantName}`,
          residentName: tenantName,
          userName: name,
          ResidentId: tenantId,
        }
        api
          .post(`/dailylog/add`, dailyLogInput, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((result) => {
            setDailylogData([...dailylogData, result.data])
            console.log(result)
          })
          .catch((err) => {
            console.error(err)
          })
      })
      .catch((err) => {
        console.log(err)
      })
  }

  const deleteData = (id) => {
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    console.log(id)

    api
      .put(`/insuranceinfo/${id}/delete`)
      .then((result) => {
        console.log(result)

        const newArr = [...insuranceInfoData]
        newArr.forEach((item, index) => {
          if (item.id === id) {
            newArr.splice(index, 1)
          }
        })

        setInsuranceInfoData(newArr)

        // adding history to database
        // const input2 = {
        //   title: `Deleted ${tenantName}'s allergy data on ${allergy}`,
        //   tenantName: tenantName,
        //   tenantId: tenantId,
        //   userName: name,
        // }
        // api
        //   .post(`/history/create/`, input2, {
        //     headers: {
        //       Authorization: `Bearer ${token}`,
        //     },
        //   })
        //   .then((result) => {})
        //   .catch((err) => {
        //     console.error(err)
        //   })
        toast.success('Deleted Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
      })
      .catch((err) => {
        Swal.fire({
          title: 'fail to delete',
          icon: 'error',
        })
      })
  }

  const updateData = (data, id) => {
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    const input = {
      name: data.name,
      type: data.type,
      coverageAmount: data.coverageAmount,
    }

    // api call for editing allergy data
    api
      .put(`/insuranceinfo/${id}/update`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        // updating the tenant's allergy data state
        insuranceInfoData.forEach((item) => {
          if (item.id === id) {
            item.name = data.name
            item.type = data.type
            item.coverageAmount = data.coverageAmount
          }
        })

        setInsuranceInfoData([...insuranceInfoData])

        // adding history to database
        // const input2 = {
        //   title: `Edited ${tenantName}'s allergy data on ${allergy}`,
        //   tenantName: tenantName,
        //   tenantId: tenantId,
        //   userName: name,
        // }
        // api
        //   .post(`/history/create/`, input2, {
        //     headers: {
        //       Authorization: `Bearer ${token}`,
        //     },
        //   })
        //   .then((result) => {})
        //   .catch((err) => {
        //     console.error(err)
        //   })
        toast.success('Updated Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
      })
      .catch((err) => {
        console.log(err)
      })
  }

  // useEffect(() => {
  //   setActiveTab(parseInt(activeTabVal))
  // }, [])

  const toggleForm = () => {
    console.log('hello')
  }

  return (
    <div style={{ maxWidth: '100%' }}>
      <IncidentPopup />
      <MaterialTable
        icons={tableIcons}
        title="Incident Report"
        style={{ zIndex: 9999999, fontSize: '12px' }}
        columns={columns}
        data={incidentReportList}
        // actions={[
        //   {
        //     icon: AddBox,
        //     tooltip: 'Add new users',
        //     isFreeAction: true,
        //     onClick: (e) => setIsInsurnacePopupOpen(true),
        //   },
        // ]}
        editable={{
          // onRowAdd: (newRow) =>
          //   new Promise((resolve, reject) => {
          //     setTimeout(() => {
          //       setInsuranceInfoData([...insuranceInfoData, newRow])
          //       addData(newRow)
          //       resolve()
          //     }, 2000)
          //   }),
          onRowDelete: (selectedRow) =>
            new Promise((resolve, reject) => {
              console.log(selectedRow)
              setTimeout(() => {
                deleteData(selectedRow.id)
                resolve()
              }, 2000)
            }),
          onRowUpdate: (updatedRow, oldRow) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                updateData(updatedRow, oldRow.id)
                resolve()
              }, 2000)
            }),
        }}
        options={{
          rowStyle: (rowData) => {
            if (rowData.tableData.id % 2 == 0) {
              return { backgroundColor: '#f4f8ff' }
            }

            return {
              backgroundColor: '#deeaff',
            }
          },
          search: false,
          actionsColumnIndex: -1,
          addRowPosition: 'first',
          headerStyle: {
            zIndex: 9999999,
            fontSize: '13px',
            fontWeight: '600',
          },
        }}
        onRowClick={(event, rowData) => {
          console.log(rowData)
        }}
      />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  )
}

export default IncidentReport
