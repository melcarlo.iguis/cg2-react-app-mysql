import React, { useState, useContext, forwardRef, useRef, useEffect } from 'react'
import MaterialTable from 'material-table'
import AppContext from '../../../AppContext'
import AddBox from '@material-ui/icons/AddBox'
import ArrowDownward from '@material-ui/icons/ArrowDownward'
import Check from '@material-ui/icons/Check'
import ChevronLeft from '@material-ui/icons/ChevronLeft'
import ChevronRight from '@material-ui/icons/ChevronRight'
import Clear from '@material-ui/icons/Clear'
import DeleteOutline from '@material-ui/icons/DeleteOutline'
import Edit from '@material-ui/icons/Edit'
import FilterList from '@material-ui/icons/FilterList'
import FirstPage from '@material-ui/icons/FirstPage'
import LastPage from '@material-ui/icons/LastPage'
import Remove from '@material-ui/icons/Remove'
import SaveAlt from '@material-ui/icons/SaveAlt'
import Search from '@material-ui/icons/Search'
import ViewColumn from '@material-ui/icons/ViewColumn'
// axios api
import api from '../../../api/api'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function RoomPreferenceLookup() {
  const {
    allergiesList,
    setAllergiesList,
    currentTab,
    setCurrentTab,
    activeTab,
    setActiveTab,
    dailylogData,
    setDailylogData,
  } = useContext(AppContext)
  const [data, setData] = useState([])
  const [isFetchDone, setIsFetchDone] = useState(false)

  useEffect(() => {
    fetchData()
  }, [])

  const fetchData = () => {
    let token = localStorage.getItem('token')

    api
      .get('/roomPrefLookup/fetch', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        setData(result.data)
        setIsFetchDone(true)
      })
  }

  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  }

  const allergyTypeSelection = { 1: 'Food', 2: 'Environment' }

  const columns = [
    {
      title: 'Label',
      field: 'label',
      cellStyle: {
        minWidth: 100,
        fontSize: 12,
      },
      headerStyle: {
        minWidth: 100,
      },
    },
    {
      title: 'Order',
      field: 'order',
      cellStyle: {
        minWidth: 100,
        fontSize: 12,
      },
      headerStyle: {
        minWidth: 100,
      },
      //   lookup: allergyTypeSelection,
    },
  ]

  const addData = (data) => {
    console.log(data)
    let token = localStorage.getItem('token')

    const input = {
      label: data.label,
      order: data.order,
    }

    // api call for adding new allergy tenant
    api
      .post(`/roomPrefLookup/add`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        toast.success('Added Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
      })
      .catch((err) => {
        console.log(err)
      })
  }

  const deleteData = (id) => {
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    console.log(id)

    api
      .put(`/roomPrefLookup/${id}/delete`)
      .then((result) => {
        console.log(result)

        const newArr = [...allergiesList]
        newArr.forEach((item, index) => {
          if (item.id === id) {
            newArr.splice(index, 1)
          }
        })

        setData(newArr)
        toast.success('Deleted Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
      })
      .catch((err) => {
        Swal.fire({
          title: 'fail to delete',
          icon: 'error',
        })
      })
  }

  const updateData = (inputData, id) => {
    console.log(data)
    console.log(id)
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    const input = {
      label: inputData.label,
      order: inputData.order,
    }

    // api call for editing allergy data
    api
      .put(`/roomPrefLookup/${id}/update`, input, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        console.log(result)
        // updating the tenant's allergy data state
        data.forEach((item) => {
          if (item.id === id) {
            ;(item.label = inputData.label), (item.order = inputData.order)
          }
        })

        setData([...data])
        toast.success('Updated Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
      })
      .catch((err) => {
        console.log(err)
      })
  }

  return (
    <div style={{ maxWidth: '100%' }}>
      {data.length > 0 && isFetchDone ? (
        <MaterialTable
          icons={tableIcons}
          title="Room Preference"
          columns={columns}
          data={data}
          editable={{
            onRowAdd: (newRow) =>
              new Promise((resolve, reject) => {
                setTimeout(() => {
                  setData([...data, newRow])
                  addData(newRow)
                  resolve()
                }, 2000)
              }),
            onRowDelete: (selectedRow) =>
              new Promise((resolve, reject) => {
                console.log(selectedRow)
                setTimeout(() => {
                  deleteData(selectedRow.id)
                  resolve()
                }, 2000)
              }),
            onRowUpdate: (updatedRow, oldRow) =>
              new Promise((resolve, reject) => {
                setTimeout(() => {
                  updateData(updatedRow, oldRow.id)
                  resolve()
                }, 2000)
              }),
          }}
          options={{
            search: false,
            actionsColumnIndex: -1,
            addRowPosition: 'first',
            // headerStyle: {
            //   zIndex: ,
            // },
          }}
        />
      ) : (
        <p>No Data Found</p>
      )}

      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  )
}

export default RoomPreferenceLookup
