import React from 'react'
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js'
import { Pie } from 'react-chartjs-2'
import ChartDataLabels from 'chartjs-plugin-datalabels'

ChartJS.register(ArcElement, Tooltip, Legend, ChartDataLabels)
function PieChart() {
  let sum = 35
  const footer = () => {
    return `Total : ${sum}`
  }
  let delayed
  const options = {
    maintainAspectRatio: false,
    plugins: {
      tooltip: {
        callbacks: {
          footer: footer,
        },
      },
      legend: {
        display: true,
        position: 'bottom',
      },
      datalabels: {
        backgroundColor: function (context) {
          return context.dataset.backgroundColor
        },
        color: 'white',
        font: {
          weight: 'bold',
        },
        padding: 6,
        // formatter: function (value, context) {
        //   return (value / sum) * 100 + '%'
        // },
      },
    },
    animation: {
      onComplete: () => {
        delayed = true
      },
      delay: (context) => {
        let delay = 0
        if (context.type === 'data' && context.mode === 'default' && !delayed) {
          delay = context.dataIndex * 300 + context.datasetIndex * 100
        }
        return delay
      },
    },
  }
  const data = {
    labels: ['sample data', 'sample data 2', 'sample data 3'],
    datasets: [
      {
        data: [5, 10, 20],
        backgroundColor: ['rgb(110,155,217)', '#64BD41', 'rgba(219, 89, 110, 0.8)'],
        hoverOffset: 4,
      },
    ],
  }

  return <Pie options={options} data={data} />
}

export default PieChart
