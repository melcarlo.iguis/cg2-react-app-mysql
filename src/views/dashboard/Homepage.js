import React from 'react'
import TenantList from './TenantList'
import UserManagement from './UserManagement'

function Homepage() {
  let userType = localStorage.getItem('userType')
  //check the user type if admin or not
  return userType == 'admin' ? <UserManagement /> : <TenantList />
}

export default Homepage
